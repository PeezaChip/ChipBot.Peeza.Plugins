﻿using BetPlugin.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using Discord;
using Microsoft.Extensions.Logging;
using PointsPlugin.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BetPlugin.Services
{
    [Chipject]
    public class BetService : ChipService<BetService>
    {
        private readonly PointsService pointsService;

        private Bet ActiveBet { get; set; }

        public BetService(ILogger<BetService> log, PointsService pointsService) : base(log)
            => this.pointsService = pointsService;

        public Bet CreateBet(IUser author, string name, string[] options)
        {
            if (ActiveBet != null) throw new Exception("Previous bet is still active.");
            if (options.Length < 2) throw new Exception("Atleast two options should be available to create a bet");
            ActiveBet = new Bet()
            {
                Author = author,
                Name = name,
                Options = new List<BetOption>(options.Select(o => new BetOption()
                {
                    Name = o,
                    Bets = new Dictionary<ulong, SingleBet>()
                }))
            };

            return ActiveBet;
        }

        public Bet CloseBet(IUser author)
        {
            if (ActiveBet == null) throw new Exception("No bet to close.");
            if (author.Id != ActiveBet.Author.Id) throw new Exception("Bet isn't yours to close.");
            if (ActiveBet.Status == BetStatus.Open) ActiveBet.Status = BetStatus.BettingClosed;

            return ActiveBet;
        }

        public Bet CloseBet(IUser author, string optionKey)
        {
            var selectedOption = ActiveBet.Options.SingleOrDefault(o => o.Name == optionKey)
                ?? throw new Exception("No such option exists.");

            CloseBet(author);
            ActiveBet.Status = BetStatus.BetClosed;

            var winnings = ActiveBet.Options.Where(o => o != selectedOption).Sum(o => o.Sum());

            var sum = selectedOption.Sum();
            foreach (var bet in selectedOption.Bets)
            {
                var won = bet.Value.Amount;
                won += (long)(winnings * bet.Value.Coefficient(sum));
                pointsService.ChangePoints(bet.Key, won, "Bet payout");
            }

            var closedBet = ActiveBet;
            ActiveBet = null;
            return closedBet;
        }

        public Bet CancelBet(IUser author)
        {
            if (ActiveBet == null) throw new Exception("No bet to cancel.");
            if (author.Id != ActiveBet.Author.Id) throw new Exception("Bet isn't yours to cancel.");

            ActiveBet.Status = BetStatus.BetCanceled;
            foreach (var bet in ActiveBet.Options.SelectMany(b => b.Bets))
                pointsService.ChangePoints(bet.Key, bet.Value.Amount, "Bet refund");

            var closedBet = ActiveBet;
            ActiveBet = null;
            return closedBet;
        }

        public Bet PlaceBet(IUser user, long amount, string optionKey)
        {
            if (ActiveBet == null) throw new Exception("No active bet.");
            if (ActiveBet.Status != BetStatus.Open) throw new Exception("Betting is closed.");
            if (user.Id == ActiveBet.Author.Id) throw new Exception("You can't place on your own bet.");
            if (amount < 100) throw new Exception("100 is a minimum bet.");
            if (!pointsService.HasEnough(user, amount)) throw new Exception("You don't have enough points");

            var selectedOption = ActiveBet.Options.SingleOrDefault(o => o.Name == optionKey)
                ?? throw new Exception("No such option exists.");

            var otherOptions = ActiveBet.Options.Where(o => o != selectedOption);
            if (otherOptions.Any(o => o.Bets.ContainsKey(user.Id))) throw new Exception("You can't place on more than one option.");

            if (!selectedOption.Bets.ContainsKey(user.Id))
                selectedOption.Bets.Add(user.Id, new SingleBet() { User = user });

            selectedOption.Bets[user.Id].Amount += amount;
            pointsService.ChangePoints(user, -amount, "Bet placed");

            return ActiveBet;
        }

        public IEnumerable<BetOption> GetBetOptions()
            => ActiveBet?.Options ?? new List<BetOption>();

        public IEnumerable<BetOption> GetBetOptions(IUser user)
        {
            var options = GetBetOptions();
            var placed = options.FirstOrDefault(o => o.Bets.ContainsKey(user.Id));

            return placed != null
                ? new List<BetOption>() { placed }
                : options;
        }

        public Bet ForceCancel()
            => ActiveBet != null ? CancelBet(ActiveBet.Author) : null;
    }
}
