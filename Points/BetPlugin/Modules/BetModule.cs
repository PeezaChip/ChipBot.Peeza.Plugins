﻿using BetPlugin.Services;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BetPlugin.Modules
{
    [Group("bet", "bet commands")]
    public class BetModule : ChipInteractionModuleBase
    {
        public BetService BetService { get; set; }

        [SlashCommand("create", "create a bet")]
        public async Task Create([Summary("name", "name of the bet")] string name, [Summary("options", "betting options divided with space")] string input)
        {
            try
            {
                var options = input.Split(' ');
                var bet = BetService.CreateBet(Context.User, name, options);
                bet.Message = await ReplyAsync(embed: bet.ToEmbed());
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("hold", "holds a bet")]
        public async Task Close()
        {
            try
            {
                var bet = BetService.CloseBet(Context.User);
                await bet?.Message.ModifyAsync(msg => msg.Embed = bet.ToEmbed());
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("close", "closes betting")]
        public async Task Close([Summary("key", "won option")][Autocomplete(typeof(BetOptionCloseAutoCompleter))] string key)
        {
            try
            {
                var bet = BetService.CloseBet(Context.User, key);
                await bet?.Message.ModifyAsync(msg => msg.Embed = bet.ToEmbed(key));
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("cancel", "cancels bet")]
        public async Task Cancel()
        {
            try
            {
                var bet = BetService.CancelBet(Context.User);
                await bet?.Message.ModifyAsync(msg => msg.Embed = bet.ToEmbed());
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("place", "places a bet")]
        public async Task Place([Summary("key", "bet option")][Autocomplete(typeof(BetOptionPlaceAutoCompleter))] string key, [Summary("amount", "how much to bet")] long amount)
        {
            try
            {
                var bet = BetService.PlaceBet(Context.User, amount, key);
                await bet?.Message.ModifyAsync(msg => msg.Embed = bet.ToEmbed());
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("force-cancel", "force cancels a bet")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task ForceCancel()
        {
            try
            {
                var bet = BetService.ForceCancel();
                await bet?.Message.ModifyAsync(msg => msg.Embed = bet.ToEmbed());
                await ReplySuccessAsync(true);
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        private class BetOptionPlaceAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var betService = services.GetRequiredService<BetService>();
                var options = betService.GetBetOptions(context.User);

                return Task.FromResult(AutocompletionResult.FromSuccess(options.Select(o => new AutocompleteResult(o.Name, o.Name))));
            }
        }

        private class BetOptionCloseAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var betService = services.GetRequiredService<BetService>();
                var options = betService.GetBetOptions();

                return Task.FromResult(AutocompletionResult.FromSuccess(options.Select(o => new AutocompleteResult(o.Name, o.Name))));
            }
        }
    }
}
