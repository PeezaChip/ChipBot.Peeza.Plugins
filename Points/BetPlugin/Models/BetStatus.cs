﻿namespace BetPlugin.Models
{
    public enum BetStatus
    {
        Open,
        BettingClosed,
        BetClosed,
        BetCanceled
    }
}
