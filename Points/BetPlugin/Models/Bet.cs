﻿using Discord;
using System.Collections.Generic;
using System.Linq;

namespace BetPlugin.Models
{
    public class Bet
    {
        public IUser Author { get; set; }
        public string Name { get; set; }
        public List<BetOption> Options { get; set; }
        public IUserMessage Message { get; set; }

        public BetStatus Status { get; set; }

        public Embed ToEmbed(string optionKey = null)
        {
            var eb = new EmbedBuilder()
                .WithAuthor(Author).WithCurrentTimestamp()
                .WithDescription($"Once you place a bet you can only increase how much you bet. You can't cancel your bet. You can't place on more than one option.\nTo place a bet use `/bet place <option_id> <amount>`.")
                .WithColor(Status switch
                {
                    BetStatus.Open => Color.Green,
                    BetStatus.BettingClosed => Color.Orange,
                    _ => Color.Red
                })
                .WithTitle(Name);

            var sum = Options.Sum(o => o.Sum());

            for (var i = 0; i < Options.Count; i++)
            {
                var option = Options[i];
                eb.AddField($"{i + 1}. {option.Name} - `{option.Coefficient(sum):0.00}`{(optionKey == option.Name ? " 🥇" : "")}", option.Bets.Any() ? string.Join("\n", option.Bets.Select(b => $"{b.Value.User.Mention} - `{b.Value.Amount}`")) : "No bets placed", true);
            }

            eb.WithFooter(Status switch
            {
                BetStatus.Open => "Bet is opened",
                BetStatus.BettingClosed => "Betting closed",
                BetStatus.BetClosed => "Bet is closed",
                BetStatus.BetCanceled => "Bet is cancelled",
                _ => "?"
            });

            return eb.Build();
        }
    }
}
