﻿using System.Collections.Generic;
using System.Linq;

namespace BetPlugin.Models
{
    public class BetOption
    {
        public string Name { get; set; }
        public Dictionary<ulong, SingleBet> Bets { get; set; }

        public long Sum() => Bets.Sum(b => b.Value.Amount);
        public double Coefficient(long sum)
        {
            var bets = Sum();
            return sum == 0 || bets == 0 ? 1 : 1 - Bets.Values.Sum(b => b.Amount) / (double)sum;
        }
    }
}
