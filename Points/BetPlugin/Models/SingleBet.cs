﻿using Discord;

namespace BetPlugin.Models
{
    public class SingleBet
    {
        public IUser User { get; set; }
        public long Amount { get; set; }

        public double Coefficient(long sum) => Amount / sum;
    }
}
