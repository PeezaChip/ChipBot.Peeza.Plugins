﻿namespace RafflePlugin.Shared
{
    public class RaffleStatistics
    {
        public long TotalWon { get; set; }
        public long BiggestWin { get; set; }

        public long TotalLost { get; set; }
        public long BiggestLose { get; set; }

        public long TotalPtsWon { get; set; }
        public long TotalPtsLost { get; set; }
    }
}
