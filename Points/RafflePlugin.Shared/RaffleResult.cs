﻿namespace RafflePlugin.Shared
{
    public class RaffleResult
    {
        public int Rolled { get; set; }
        public long CurrentPoints { get; set; }
        public long PointsWon { get; set; }
        public string ResultMessage { get; set; }
    }
}
