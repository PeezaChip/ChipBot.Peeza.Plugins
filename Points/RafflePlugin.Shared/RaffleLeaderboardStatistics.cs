﻿namespace RafflePlugin.Shared
{
    public class RaffleLeaderboardleStatistics
    {
        public long BiggestWin { get; set; }
        public ulong BiggestWinUserId { get; set; }
        public long BiggestLose { get; set; }
        public ulong BiggestLoseUserId { get; set; }

        public long TotalPtsWon { get; set; }
        public ulong TotalPtsWonUserId { get; set; }
        public long TotalPtsLost { get; set; }
        public ulong TotalPtsLostUserId { get; set; }
    }
}
