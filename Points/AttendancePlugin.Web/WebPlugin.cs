﻿using ChipBot.Core.Web.Abstract;

namespace AttendancePlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "AttendanceWebPlugin";
    }
}
