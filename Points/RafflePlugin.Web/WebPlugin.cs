﻿using ChipBot.Core.Web.Abstract;

namespace RafflePlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "RaffleWebPlugin";
    }
}
