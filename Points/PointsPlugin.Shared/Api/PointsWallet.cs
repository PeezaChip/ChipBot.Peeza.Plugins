﻿namespace PointsPlugin.Shared.Api
{
    public class PointsWallet
    {
        public long Balance { get; set; }
        public string Currency { get; set; }
        public List<PointsTransaction> Transactions { get; set; }
    }

    public class PointsTransaction
    {
        public string Reason { get; set; }
        public DateTimeOffset DateTime { get; set; }

        public long Points { get; set; }
    }
}
