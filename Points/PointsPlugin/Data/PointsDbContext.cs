﻿using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using PointsPlugin.Data.Models;

namespace PointsPlugin.Data
{
    [ChipDbContext("PointsPlugin")]
    public class PointsDbContext : DbContext
    {
        public DbSet<PointsModel> Points { get; set; }
        public DbSet<PointsTransactionModel> PointsTransactions { get; set; }
        public DbSet<PointsGiftModel> PointsGifts { get; set; }

        public PointsDbContext(DbContextOptions options) : base(options) { }
    }
}
