﻿using ChipBot.Core.Data;

namespace PointsPlugin.Data
{
    public class PointsDbContextFactory : ChipBotDbContextFactory<PointsDbContext> { }
}
