﻿using System.ComponentModel.DataAnnotations;

namespace PointsPlugin.Data.Models
{
    public class PointsGiftModel
    {
        [Key]
        public ulong UserId { get; set; }

        [Required]
        public long PointsGifted { get; set; }
    }
}
