﻿using Microsoft.EntityFrameworkCore;

namespace PointsPlugin.Data.Models
{
    [Index(nameof(Reason))]
    [PrimaryKey(nameof(DateTime), nameof(ExecutorId))]
    public class PointsTransactionModel
    {
        public string Reason { get; set; }
        public DateTimeOffset DateTime { get; set; } = DateTimeOffset.UtcNow;

        public long Points { get; set; }

        public ulong ExecutorId { get; set; }
    }
}
