﻿using System.ComponentModel.DataAnnotations;

namespace PointsPlugin.Data.Models
{
    public class PointsModel
    {
        [Key]
        public ulong UserId { get; set; }

        [Required]
        public long Points { get; set; }
    }
}
