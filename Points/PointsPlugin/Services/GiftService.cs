﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PointsPlugin.Data;
using PointsPlugin.Data.Models;

namespace PointsPlugin.Services
{
    [Notify(NoEarlierThan = 0)]
    [Chipject]
    public class GiftService : ChipService<GiftService>, INotifyService
    {
        private readonly PointsService pointsService;
        private readonly ChipBotDbContextFactory<PointsDbContext> dbContextFactory;
        private readonly SettingsService<PointsConfig> pointsConfig;

        public GiftService(ILogger<GiftService> log, PointsService pointsService, ChipBotDbContextFactory<PointsDbContext> dbContextFactory, SettingsService<PointsConfig> pointsConfig) : base(log)
        {
            this.pointsService = pointsService;
            this.dbContextFactory = dbContextFactory;
            this.pointsConfig = pointsConfig;
        }

        public void Gift(IUser giver, IUser given, long pts)
        {
            if (giver.Id == given.Id) throw new Exception("You can't gift points to yourself.");
            if (pts <= 0) throw new Exception("Points amount should be higher than 0.");

            using var dbContext = dbContextFactory.CreateDbContext();

            var givenData = GetModel(dbContext.PointsGifts, given);
            var giverData = GetModel(dbContext.PointsGifts, giver);

            CheckLimit(givenData, given, pts);
            CheckLimit(giverData, giver, pts);

            givenData.PointsGifted += pts;
            giverData.PointsGifted += pts;

            pointsService.ChangePoints(giver, -pts, $"Gift to {given.Id} ({given.Username}#{given.DiscriminatorValue})");
            pointsService.ChangePoints(given, pts, $"Gift from {giver.Id} ({giver.Username}#{giver.DiscriminatorValue})");

            dbContext.SaveChanges();
        }

        private static PointsGiftModel GetModel(DbSet<PointsGiftModel> set, IUser user)
        {
            var model = set.Find(user.Id);
            if (model == null)
            {
                model = new PointsGiftModel() { UserId = user.Id, PointsGifted = 0 };
                set.Add(model);
            }
            return model;
        }

        private void CheckLimit(PointsGiftModel model, IUser user, long pts)
        {
            var limit = model?.PointsGifted ?? 0;
            if (pts + limit > pointsConfig.Settings.GiftLimit) throw new Exception($"{user.Mention} can't gift nor receive more than {pointsConfig.Settings.GiftLimit - limit}pts today.");
        }

        public async Task Notify()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            await dbContext.PointsGifts.ExecuteDeleteAsync();
        }
    }
}