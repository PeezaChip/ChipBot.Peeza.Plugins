﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using PointsPlugin.Data;
using PointsPlugin.Data.Models;

namespace PointsPlugin.Services
{
    [Chipject]
    public class PointsService : ChipService<PointsService>
    {
        private readonly DiscordSocketClient discord;
        private readonly ChipBotDbContextFactory<PointsDbContext> dbContextFactory;
        private readonly SettingsService<GlobalConfig> globalConfig;
        private readonly SettingsService<PointsConfig> pointsConfig;

        private long PointsPerMin => pointsConfig.Settings.PointsPerMinute;
        private long PointsMultiplier => pointsConfig.Settings.PointsMultiplier;
        public string CurrencyName => pointsConfig.Settings.CurrencyName;

        public PointsService(DiscordSocketClient discord, ChipBotDbContextFactory<PointsDbContext> dbContextFactory,
            SettingsService<GlobalConfig> globalConfig, SettingsService<PointsConfig> pointsConfig, ILogger<PointsService> logger)
            : base(logger)
        {
            this.discord = discord;
            this.dbContextFactory = dbContextFactory;
            this.globalConfig = globalConfig;
            this.pointsConfig = pointsConfig;

            _ = RunTask();
        }

        private async Task RunTask()
        {
            while (true)
            {
                try
                {
                    await IncreasePoints();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Couldn't increase points");
                }
                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        }

        private async Task IncreasePoints()
        {
            if (discord.ConnectionState != ConnectionState.Connected) return;

            logger.LogTrace("Increasing points");

            var guild = discord.GetGuild(globalConfig.Settings.MainGuildId);
            var pointsToGive = PointsPerMin * PointsMultiplier;

            await guild.DownloadUsersAsync();

            using var dbContext = dbContextFactory.CreateDbContext();

            var guildUsers = guild.Users.ToList();

            var data = dbContext.Points.ToDictionary(d => d.UserId, d => d);
            var keysToRemove = data.Keys.Except(guildUsers.Select(x => x.Id).ToList()).ToList();

            foreach (var key in keysToRemove)
            {
                dbContext.Points.Remove(data[key]);
                dbContext.PointsTransactions.Add(new() { ExecutorId = key, Points = 0, Reason = "Removing user for not being in guild" });
            }

            foreach (IGuildUser usr in guild.Users.ToList())
            {
                if (usr.VoiceChannel is not null)
                {
                    if (data.TryGetValue(usr.Id, out var value))
                    {
                        value.Points += pointsToGive;
                        dbContext.Points.Update(value);
                    }
                    else
                    {
                        value = new() { UserId = usr.Id, Points = pointsToGive };
                        dbContext.Points.Add(value);
                    }
                    dbContext.PointsTransactions.Add(new() { ExecutorId = value.UserId, Points = pointsToGive, Reason = "Adding points for being inside voice channel" });
                }
            }

            dbContext.SaveChanges();
            logger.LogTrace("Increased points");
        }

        public long GetPoints(IUser user)
            => GetPoints(user.Id);
        public long GetPoints(ulong id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.Points.Find(id)?.Points ?? 0;
        }

        public long ChangePoints(ulong id, long amount, string reason)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.Points.Find(id);
            if (data != null)
            {
                data.Points += amount;
                dbContext.Update(data);
            }
            else
            {
                data = new() { UserId = id, Points = amount };
                dbContext.Add(data);
            }
            dbContext.PointsTransactions.Add(new() { ExecutorId = id, Points = amount, Reason = reason });
            dbContext.SaveChanges();

            logger.LogTrace($"Changed {id} on {amount}. Current value {data.Points}");

            return data.Points;
        }
        public long ChangePoints(IUser user, long amount, string reason)
            => ChangePoints(user.Id, amount, reason);

        public long AwardPoints(ulong id, long amount, string reason)
            => ChangePoints(id, amount * PointsMultiplier, reason);
        public long AwardPoints(IUser user, long amount, string reason)
            => ChangePoints(user.Id, amount * PointsMultiplier, reason);

        public bool HasEnough(ulong id, long amount)
            => GetPoints(id) >= amount;
        public bool HasEnough(IUser user, long amount)
            => HasEnough(user.Id, amount);

        public string GetFormatted(long points)
            => $"{points}{CurrencyName}";
        public string GetUserPointsFormatted(ulong id)
            => GetFormatted(GetPoints(id));
        public string GetUserPointsFormatted(IUser user)
            => GetFormatted(GetPoints(user.Id));

        public IEnumerable<PointsModel> GetUsers(int n)
            => GetUsers().Take(n);
        public IOrderedEnumerable<PointsModel> GetUsers()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.Points.Where(u => u.UserId != discord.CurrentUser.Id).ToList().OrderByDescending(u => u.Points);
        }

        public IEnumerable<PointsTransactionModel> GetTransactionHistory(IUser user)
            => GetTransactionHistory(user.Id);
        public IEnumerable<PointsTransactionModel> GetTransactionHistory(ulong id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.PointsTransactions
                .Where(t => t.ExecutorId == id)
                .OrderByDescending(t => t.DateTime)
                .Take(50).ToList();
        }

        public PointsConfig GetConfig()
            => pointsConfig.Settings;

        public void SaveConfig(PointsConfig config)
            => pointsConfig.SaveConfiguration(config);
    }
}
