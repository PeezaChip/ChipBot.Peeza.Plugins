﻿using Newtonsoft.Json;

namespace PointsPlugin
{
    public class PointsConfig
    {
        [JsonProperty("pointsPerMinute")]
        [JsonRequired]
        public long PointsPerMinute { get; set; } = 1;

        [JsonProperty("pointsMultiplier")]
        [JsonRequired]
        public long PointsMultiplier { get; set; } = 1;

        [JsonProperty("giftLimit")]
        public long GiftLimit { get; set; } = 10000;

        [JsonProperty("currencyName")]
        [JsonRequired]
        public string CurrencyName { get; set; } = "$";
    }
}
