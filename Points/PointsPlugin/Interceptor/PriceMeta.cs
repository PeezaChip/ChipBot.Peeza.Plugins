﻿namespace PointsPlugin.Interceptor
{
    public class PriceMeta
    {
        public long Price { get; set; } = 0;
        public bool RefundOnError { get; set; } = false;
    }
}
