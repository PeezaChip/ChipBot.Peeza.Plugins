﻿using ChipBot.Core.Interceptors;

namespace PointsPlugin.Interceptor
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PriceAttribute : Attribute, IInterceptorAttribute<PriceMeta>
    {
        private PriceMeta Meta { get; set; } = new PriceMeta();

        public long Price { get => Meta.Price; private set => Meta.Price = value; }
        public bool RefundOnError { get => Meta.RefundOnError; set => Meta.RefundOnError = value; }

        public PriceAttribute(long price)
            => Price = price;

        public PriceMeta ToMeta()
            => Meta;
    }
}
