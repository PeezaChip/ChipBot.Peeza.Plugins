﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Implementations;
using ChipBot.Core.Interceptors;
using PointsPlugin.Services;

namespace PointsPlugin.Interceptor
{
    public class PriceInterceptor : AttributeInterceptor<PriceAttribute, PriceMeta>
    {
        private readonly PointsService pointsService;

        public PriceInterceptor(PointsService pointsService)
            => this.pointsService = pointsService;

        public override Task<InterceptResult> Intercept(IChipContext context, string commandKey)
        {
            var meta = GetMeta(context);
            if (meta == null || meta.Price <= 0) return Task.FromResult(new InterceptResult());

            if (pointsService.HasEnough(context.User, meta.Price))
            {
                pointsService.ChangePoints(context.User, -meta.Price, $"Command {commandKey} price");
                if (!meta.RefundOnError) RemoveMeta(context);
                return Task.FromResult(new InterceptResult());
            }
            else
            {
                RemoveMeta(context);
                return Task.FromResult(new InterceptResult("You don't have enough points."));
            }
        }

        public override Task OnCommandError(IChipContext context, string commandKey, ChipExecutionResult result)
        {
            var meta = GetMeta(context);
            pointsService.ChangePoints(context.User, meta.Price, $"Command {commandKey} price refund");
            return Task.CompletedTask;
        }
    }
}
