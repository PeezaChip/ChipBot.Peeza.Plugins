﻿using ChipBot.Core.Abstract;

namespace PointsPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "PointsPlugin";
    }
}
