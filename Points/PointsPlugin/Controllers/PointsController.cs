﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PointsPlugin.Services;
using PointsPlugin.Shared.Api;

namespace PointsPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "points", Version = "v1", Title = "ChipBot Points V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PointsController : ControllerBase
    {
        private readonly PointsService pointsService;
        private readonly SettingsService<PointsConfig> config;

        public PointsController(PointsService pointsService, SettingsService<PointsConfig> config)
        {
            this.pointsService = pointsService;
            this.config = config;
        }

        [HttpGet("currency")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ResponseCache(CacheProfileName = CachingConstants.HourPublicCache)]
        public IActionResult GetCurrency()
            => Ok(config.Settings.CurrencyName);

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<ulong, long>))]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        public IActionResult Get()
            => Ok(pointsService.GetUsers().ToDictionary(model => model.UserId, model => model.Points));

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        public IActionResult Get(ulong id)
            => Ok(pointsService.GetPoints(id));

        [HttpGet("my")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        public IActionResult GetMy()
            => Get(HttpContext.GetCurrentDiscordUserId());

        [HttpGet("wallet")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PointsWallet))]
        public IActionResult Transactions()
        {
            var userId = HttpContext.GetCurrentDiscordUserId();
            var history = pointsService.GetTransactionHistory(userId);

            return Ok(new PointsWallet()
            {
                Balance = pointsService.GetPoints(userId),
                Transactions = history.Select(t => new PointsTransaction()
                {
                    DateTime = t.DateTime,
                    Points = t.Points,
                    Reason = t.Reason
                }).ToList()
            });
        }

        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = RoleDefaults.Owner)]
        public void Post(ulong id, [FromBody] long value)
            => pointsService.ChangePoints(id, value, "Admin Api Change");
    }
}
