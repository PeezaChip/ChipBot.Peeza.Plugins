﻿using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using PointsPlugin.Services;

namespace PointsPlugin.Modules
{
    [Group("points-admin", "point related commands")]
    [RequireUserPermission(GuildPermission.Administrator)]
    [ChipInteractionPrecondition(typeof(RequireOwners))]
    public class PointsAdminModule : ChipInteractionModuleBase
    {
        public PointsService Points { get; set; }

        [SlashCommand("add", "Changes points to someone")]
        public async Task Add([Summary("user", "User to give points to")] IUser user, [Summary("points", "Amount of points to give")] long points)
        {
            Points.ChangePoints(user, points, "Admin Change");
            await ReplySuccessAsync(true);
        }

        [SlashCommand("wipe", "Takes all points away from someone")]
        public async Task Wipe([Summary("user", "User to take points from")] IUser user)
        {
            var p = Points.GetPoints(user);
            if (p > 0)
                Points.ChangePoints(user, -p, "Admin Wipe");
            await ReplySuccessAsync(true);
        }

        [SlashCommand("get-settings", "Displays current point settings")]
        public async Task Settings()
        {
            var cfg = Points.GetConfig();
            await ReplyWithConfig(cfg);
        }

        [SlashCommand("set-settings", "Sets current point settings")]
        public async Task Settings(long perMin, long mult, string currency)
        {
            var cfg = new PointsConfig() { PointsPerMinute = perMin, PointsMultiplier = mult, CurrencyName = currency };
            Points.SaveConfig(cfg);
            await ReplyWithConfig(cfg);
        }

        private async Task ReplyWithConfig(PointsConfig cfg)
            => await RespondAsync($"Points per minute `{cfg.PointsPerMinute}`\nPoints multiplier `{cfg.PointsMultiplier}`\nCurrency name `{cfg.CurrencyName}`", ephemeral: true);
    }
}
