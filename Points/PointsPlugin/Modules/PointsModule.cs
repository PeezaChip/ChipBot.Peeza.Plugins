﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using PointsPlugin.Attributes;
using PointsPlugin.Services;

namespace PointsPlugin.Modules
{
    [Group("points", "points related commands")]
    public class PointsModule : ChipInteractionModuleBase
    {
        public PointsService Points { get; set; }
        public GiftService Gifts { get; set; }

        [SlashCommand("balance", "Displays your current amount of points")]
        public async Task Default()
            => await RespondAsync($"{Context.User.Mention} has {Points.GetUserPointsFormatted(Context.User)}.");

        [SlashCommand("gift", "Gifts points to someone else")]
        public async Task Gift([Summary("user", "User to gift points to")] IUser user, [Summary("points", "Amount of points to gift")][HasEnoughPoints] long points)
        {
            try
            {
                Gifts.Gift(Context.User, user, points);
                await ReplySuccessAsync();
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("leaderboard", "Displays leaderboard")]
        public async Task Leaderboard()
        {
            var ordered = Points.GetUsers().Take(10);
            var i = 0;
            var result = new List<string>();

            for (var e = ordered.GetEnumerator(); e.MoveNext(); i++)
            {
                var row = $"{i + 1}. <@{e.Current.UserId}> - {e.Current.Points}{Points.CurrencyName}\n";
                if (i % 10 == 0) result.Add("");
                result[^1] = result.Last() + row;
            }

            await RespondAsync(string.Join("\n", result));
        }
    }
}
