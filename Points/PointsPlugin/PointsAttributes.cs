﻿using Discord.Commands;
using PointsPlugin.Services;

namespace PointsPlugin.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public class HasEnoughPointsAttribute : ParameterPreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            var points = (PointsService)services.GetService(typeof(PointsService));

            return points.HasEnough(context.User, (long)(value))
                ? Task.FromResult(PreconditionResult.FromSuccess())
                : Task.FromResult(PreconditionResult.FromError("You don't have enough points"));
        }
    }
}
