﻿using ChipBot.Core.Web.Abstract;

namespace PointsPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "PointsWebPlugin";
    }
}
