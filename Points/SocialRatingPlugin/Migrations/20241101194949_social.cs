﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SocialRatingPlugin.Migrations
{
    /// <inheritdoc />
    public partial class social : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SocialRating",
                columns: table => new
                {
                    UserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    SocialRating = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialRating", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "SocialRatingHistory",
                columns: table => new
                {
                    DateTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    TargetId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    Reason = table.Column<string>(type: "text", nullable: true),
                    SocialRatingChange = table.Column<double>(type: "double precision", nullable: false),
                    StrikerId = table.Column<decimal>(type: "numeric(20,0)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialRatingHistory", x => new { x.DateTime, x.TargetId });
                });

            migrationBuilder.CreateIndex(
                name: "IX_SocialRatingHistory_Reason",
                table: "SocialRatingHistory",
                column: "Reason");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SocialRating");

            migrationBuilder.DropTable(
                name: "SocialRatingHistory");
        }
    }
}
