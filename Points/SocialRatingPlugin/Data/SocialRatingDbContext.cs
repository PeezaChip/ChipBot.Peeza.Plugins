﻿using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using SocialRatingPlugin.Data.Models;

namespace SocialRatingPlugin.Data
{
    [ChipDbContext("SocialRatingPlugin")]
    public class SocialRatingDbContext : DbContext
    {
        public DbSet<SocialRatingModel> SocialRating { get; set; }
        public DbSet<SocialRatingChangeModel> SocialRatingHistory { get; set; }

        public SocialRatingDbContext(DbContextOptions options) : base(options) { }
    }
}
