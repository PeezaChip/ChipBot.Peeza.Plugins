﻿using ChipBot.Core.Data;

namespace SocialRatingPlugin.Data
{
    public class SocialRatingDbContextFactory : ChipBotDbContextFactory<SocialRatingDbContext> { }
}
