﻿using System.ComponentModel.DataAnnotations;

namespace SocialRatingPlugin.Data.Models
{
    public class SocialRatingModel
    {
        [Key]
        public ulong UserId { get; set; }

        [Required]
        public double SocialRating { get; set; }
    }
}
