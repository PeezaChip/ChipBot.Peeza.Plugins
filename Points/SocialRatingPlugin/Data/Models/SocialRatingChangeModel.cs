﻿using Microsoft.EntityFrameworkCore;

namespace SocialRatingPlugin.Data.Models
{
    [Index(nameof(Reason))]
    [PrimaryKey(nameof(DateTime), nameof(TargetId))]
    public class SocialRatingChangeModel
    {
        public string Reason { get; set; }
        public DateTimeOffset DateTime { get; set; } = DateTimeOffset.UtcNow;

        public double SocialRatingChange { get; set; }

        public ulong TargetId { get; set; }
        public ulong StrikerId { get; set; }
    }
}
