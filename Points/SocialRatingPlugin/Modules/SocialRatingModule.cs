﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using SocialRatingPlugin.Services;

namespace SocialRatingPlugin.Modules
{
    [Group("social", "social rating related commands")]
    public class SocialRatingModule : ChipInteractionModuleBase
    {
        public SocialRatingService SocialRatingService { get; set; }

        [SlashCommand("check", "Displays user social rating")]
        public async Task Check([Summary("user", "User to see rating of")] IUser user)
            => await RespondAsync($"{user.Mention} social rating\n{SocialRatingService.FormatSR(SocialRatingService.GetSR(user))}");

        [SlashCommand("report", "Report someone")]
        public async Task Report([Summary("user", "User to report")] IUser user, [Summary("reason", "Report reason")] string reason)
        {
            try
            {
                SocialRatingService.Report(user, Context.User, reason);
                await ReplySuccessAsync();
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }

        [SlashCommand("commend", "Commend someone")]
        public async Task Commend([Summary("user", "User to report")] IUser user, [Summary("reason", "Commend reason")] string reason)
        {
            try
            {
                SocialRatingService.Commend(user, Context.User, reason);
                await ReplySuccessAsync();
            }
            catch (Exception e)
            {
                await ReplyError(e.Message);
            }
        }
    }
}
