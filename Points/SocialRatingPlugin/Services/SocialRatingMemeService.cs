﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;

namespace SocialRatingPlugin.Services
{
    [Chipject]
    public class SocialRatingMemeService : BackgroundService<SocialRatingMemeService>
    {
        private readonly DiscordSocketClient discord;

        public override bool IsWorking => isWorking;
        private bool isWorking = false;

        public SocialRatingMemeService(DiscordSocketClient discord, ILogger<SocialRatingMemeService> logger) : base(logger)
        {
            this.discord = discord;
        }

        public override Task StartAsync()
        {
            if (isWorking) return Task.CompletedTask;
            isWorking = true;
            discord.ReactionAdded += OnReactionAdded;
            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            if (!isWorking) return Task.CompletedTask;
            isWorking = false;
            discord.ReactionAdded -= OnReactionAdded;
            return Task.CompletedTask;
        }

        private Task OnReactionAdded(Discord.Cacheable<Discord.IUserMessage, ulong> message, Discord.Cacheable<Discord.IMessageChannel, ulong> channel, SocketReaction reaction)
        {
            // if reaction equals reaction from config list
            // socialratingservice.plusrep

            return Task.CompletedTask;
        }
    }
}
