﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using SocialRatingPlugin.Data;
using SocialRatingPlugin.Data.Models;

namespace SocialRatingPlugin.Services
{
    [Chipject]
    public class SocialRatingService : ChipService<SocialRatingService>
    {
        private readonly DiscordSocketClient discord;
        private readonly ChipBotDbContextFactory<SocialRatingDbContext> dbContextFactory;
        private readonly SettingsService<GlobalConfig> globalConfig;

        public SocialRatingService(DiscordSocketClient discord, ChipBotDbContextFactory<SocialRatingDbContext> dbContextFactory,
            SettingsService<GlobalConfig> globalConfig, ILogger<SocialRatingService> logger)
            : base(logger)
        {
            this.discord = discord;
            this.dbContextFactory = dbContextFactory;
            this.globalConfig = globalConfig;
        }

        public double GetSR(IUser user)
            => GetSR(user.Id);
        public double GetSR(ulong id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(id);
            return data != null
                ? data.SocialRating
                : InitSR(id).SocialRating;
        }

        private SocialRatingModel InitSR(ulong id)
        {
            var isElite = IsElite(id);

            var data = new SocialRatingModel() { UserId = id, SocialRating = isElite ? 50 : 20 };
            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.SocialRating.Add(data);
            dbContext.SocialRatingHistory.Add(new SocialRatingChangeModel() { Reason = "Initial rating", SocialRatingChange = data.SocialRating, TargetId = id });
            dbContext.SaveChanges();
            return data;
        }

        public void MinusRep(IUser target, IUser initiator, string reason)
        {
            if (target.Id == initiator.Id) throw new Exception("You cannot plus rep yourself");

            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(initiator.Id);

            var isElite = IsElite(target.Id);
            var change = (isElite ? -0.25 : -0.5) * data.SocialRating / 100d;
            ChangeSR(target, initiator, reason, change);
        }

        public void PlusRep(IUser target, IUser initiator, string reason)
        {
            if (target.Id == initiator.Id) throw new Exception("You cannot plus rep yourself");

            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(initiator.Id);

            var isElite = IsElite(target.Id);
            var change = (isElite ? 0.5 : 0.25) * data.SocialRating / 100d;
            ChangeSR(target, initiator, reason, change);
        }

        public void Report(IUser target, IUser initiator, string reason)
        {
            if (target.Id == initiator.Id) throw new Exception("You cannot report yourself");
            CheckCooldown(target, initiator);

            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(initiator.Id);
            if (data.SocialRating <= 0) throw new Exception("Your social rating is too low to report other users.");

            var isElite = IsElite(target.Id);
            var change = (isElite ? -5 : -10) * data.SocialRating / 100d;
            ChangeSR(target, initiator, reason, change);
        }

        public void Commend(IUser target, IUser initiator, string reason)
        {
            if (target.Id == initiator.Id) throw new Exception("You cannot commend yourself");
            CheckCooldown(target, initiator);

            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(initiator.Id);

            var isElite = IsElite(target.Id);
            var change = (isElite ? 10 : 5) * data.SocialRating / 100d;
            ChangeSR(target, initiator, reason, change);
        }

        public static string FormatSR(double rating)
        {
            int color;
            if (rating >= 90)
            {
                color = 36; // Cyan
            }
            else if (rating >= 50)
            {
                color = 34; // Blue
            }
            else if (rating >= 30)
            {
                color = 32; // Green
            }
            else if (rating >= 0)
            {
                color = 37; // White
            }
            else if (rating >= -10)
            {
                color = 33; // Yellow
            }
            else
            {
                color = 31; // Red
            }

            return color == 37
                ? $"```\n{rating:0.00}\n```"
                : $"```ansi\n\u001b[1;{color}m{rating:0.00}\n```";
        }

        private void CheckCooldown(IUser target, IUser initiator)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRatingHistory.FirstOrDefault(h => h.StrikerId == initiator.Id && h.TargetId == target.Id);

            if (data == null) return;
            if (DateTimeOffset.UtcNow - data.DateTime < TimeSpan.FromHours(24)) throw new Exception("You've recently commended or reported this person.");
        }

        private void ChangeSR(IUser target, IUser initiator, string reason, double change)
        {
            if (target.Id == initiator.Id) throw new Exception("You cannot report or commend yourself");

            using var dbContext = dbContextFactory.CreateDbContext();
            var data = dbContext.SocialRating.Find(target.Id);
            data ??= InitSR(target.Id);
            var isElite = IsElite(target.Id);

            data.SocialRating += change;

            if (data.SocialRating < -100) data.SocialRating = -100;
            else if (data.SocialRating > 100) data.SocialRating = 100;

            dbContext.SocialRatingHistory.Add(new SocialRatingChangeModel() { Reason = reason, SocialRatingChange = change, TargetId = target.Id, StrikerId = initiator.Id });
            dbContext.SocialRating.Update(data);
            dbContext.SaveChanges();
        }

        private bool IsElite(ulong id)
        {
            return discord
                .GetGuild(globalConfig.Settings.MainGuildId)
                .GetUser(id)?.Roles
                .Any(r => globalConfig.Settings.EliteRoleIds.Contains(r.Id)) ?? false;
        }
    }
}
