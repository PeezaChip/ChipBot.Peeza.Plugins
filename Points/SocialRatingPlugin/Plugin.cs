﻿using ChipBot.Core.Abstract;

namespace SocialRatingPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "SocialRatingPlugin";
    }
}
