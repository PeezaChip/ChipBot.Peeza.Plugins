﻿using AttendancePlugin.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace AttendancePlugin.Data.Models
{
    [PrimaryKey(nameof(UserId), nameof(Event), nameof(TimeStamp))]
    public class AttendanceEvent
    {
        public ulong UserId { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        [Required]
        public AttendanceEventEnum Event { get; set; }

        public ulong? Subject { get; set; }
    }
}
