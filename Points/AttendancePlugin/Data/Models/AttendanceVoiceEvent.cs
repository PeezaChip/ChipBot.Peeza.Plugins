﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendancePlugin.Data.Models
{
    [PrimaryKey(nameof(UserId), nameof(StartTime))]
    public class AttendanceVoiceEvent
    {
        public ulong UserId { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset EndTime { get; set; }

        public ulong VoiceChannelId { get; set; }

        [NotMapped]
        public TimeSpan Duration => EndTime - StartTime;
        public TimeSpan DurationDeafened { get; set; }
        public TimeSpan DurationStreamed { get; set; }
    }
}
