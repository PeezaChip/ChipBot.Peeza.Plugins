﻿using System.ComponentModel.DataAnnotations;

namespace AttendancePlugin.Data.Models
{
    public class DayReport
    {
        [Key]
        public DateTimeOffset Date { get; set; }

        public List<ulong> FullPresence { get; set; } = [];
        public List<ulong> LowActivity { get; set; } = [];
        public List<ulong> MessageOnly { get; set; } = [];
    }
}
