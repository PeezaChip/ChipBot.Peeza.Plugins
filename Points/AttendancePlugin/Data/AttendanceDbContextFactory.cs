﻿using ChipBot.Core.Data;

namespace AttendancePlugin.Data
{
    public class AttendanceDbContextFactory : ChipBotDbContextFactory<AttendanceDbContext> { }
}
