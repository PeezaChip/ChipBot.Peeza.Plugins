﻿using AttendancePlugin.Data.Models;
using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;

namespace AttendancePlugin.Data
{
    [ChipDbContext("AttendancePlugin")]
    public class AttendanceDbContext : DbContext
    {
        public DbSet<AttendanceEvent> AttendanceEvents { get; set; }
        public DbSet<AttendanceVoiceEvent> AttendanceVoiceEvents { get; set; }
        public DbSet<DayReport> AttendanceReports { get; set; }

        public AttendanceDbContext(DbContextOptions options) : base(options) { }
    }
}
