﻿using AttendancePlugin.Service;
using AttendancePlugin.Shared.Api.Models;
using ChipBot.Core.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AttendancePlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "attendance", Version = "v1", Title = "ChipBot Attendance V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class AttendanceController(AttendanceDataService attendanceData) : ControllerBase
    {
        private readonly AttendanceDataService attendanceData = attendanceData;

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserAttendanceApi))]
        public IActionResult Get(ulong id, DateTimeOffset? date = null)
            => Ok(attendanceData.GetDataFor(id, date ?? DateTimeOffset.UtcNow));

        [HttpGet("report")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DayReportApi))]
        public IActionResult GetReport(DateTimeOffset? date = null)
            => Ok(attendanceData.GetReportFor(date ?? DateTimeOffset.UtcNow));

        [HttpGet("reportbulk")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<DateTimeOffset, DayReportApi>))]
        public IActionResult GetReportBulk(DateTimeOffset? date = null, int lastDays = 7)
        {
            if (lastDays > 31) lastDays = 31;
            if (lastDays < 0) lastDays = 0;

            return Ok(attendanceData.GetReportFor(date ?? DateTimeOffset.UtcNow, lastDays));
        }
    }
}
