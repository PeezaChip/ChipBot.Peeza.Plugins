﻿namespace AttendancePlugin
{
    public class AttendanceConfig
    {
        public int PresenceHoursNeeded { get; set; } = 4;
        public double StreamMultiplier { get; set; } = 1;
        public double DeafMultiplier { get; set; } = -1;
    }
}
