﻿using AttendancePlugin.Data;
using AttendancePlugin.Data.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;

namespace AttendancePlugin.Service
{
    [Chipject]
    [Notify(NoEarlierThan = 1)]
    public class AttendanceCalculationService(ChipBotDbContextFactory<AttendanceDbContext> dbContextFactory, SettingsService<AttendanceConfig> config, ILogger<AttendanceCalculationService> log) : ChipService<AttendanceCalculationService>(log), INotifyService
    {
        private readonly AttendanceDbContext dbContext = dbContextFactory.CreateDbContext();
        private readonly SettingsService<AttendanceConfig> config = config;

        public Task Notify()
        {
            var dayBefore = DateTimeOffset.UtcNow.AddDays(-1).Date;

            logger.LogInformation($"Calculating attendance for day {dayBefore}");

            if (dbContext.AttendanceReports.FirstOrDefault(r => r.Date.Date == dayBefore) != null)
            {
                logger.LogWarning($"Attendance was already calculated for day {dayBefore}");
            }

            var events = dbContext.AttendanceEvents.Where(ev => ev.TimeStamp.Date == dayBefore).ToList();
            var voiceEvents = dbContext.AttendanceVoiceEvents.Where(ev => ev.StartTime.Date == dayBefore || ev.EndTime.Date == dayBefore).ToList();

            var userIds = events.Select(ev => ev.UserId).Concat(voiceEvents.Select(ev => ev.UserId)).Distinct();

            var report = new DayReport()
            {
                Date = DateTime.SpecifyKind(dayBefore, DateTimeKind.Utc),
            };

            foreach (var userId in userIds)
            {
                var timeTotal = new TimeSpan();

                foreach (var ev in voiceEvents.Where(ev => ev.UserId == userId))
                {
                    var globalMultiplier = 1.0;

                    var realStartTime = ev.StartTime < dayBefore ? dayBefore : ev.StartTime;
                    var realEndTime = ev.EndTime > dayBefore.AddHours(23).AddMinutes(59).AddSeconds(59) ? dayBefore.AddHours(23).AddMinutes(59).AddSeconds(59) : ev.EndTime;
                    var realDuration = realEndTime - realStartTime;

                    globalMultiplier = realDuration / ev.Duration;

                    timeTotal += ev.Duration * globalMultiplier;
                    timeTotal += ev.DurationDeafened * config.Settings.DeafMultiplier * globalMultiplier;
                    timeTotal += ev.DurationStreamed * config.Settings.StreamMultiplier * globalMultiplier;
                }

                if (timeTotal.TotalHours > config.Settings.PresenceHoursNeeded)
                {
                    report.FullPresence.Add(userId);
                }
                else if (timeTotal.TotalHours <= 0)
                {
                    if (events.Where(ev => ev.UserId == userId).Any())
                    {
                        report.MessageOnly.Add(userId);
                    }
                }
                else
                {
                    report.LowActivity.Add(userId);
                }
            }

            dbContext.AttendanceReports.Add(report);
            dbContext.SaveChanges();
            return Task.CompletedTask;
        }
    }
}
