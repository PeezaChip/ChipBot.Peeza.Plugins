﻿using AttendancePlugin.Data;
using AttendancePlugin.Data.Models;
using AttendancePlugin.Extensions;
using AttendancePlugin.Shared.Api.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using Microsoft.Extensions.Logging;

namespace AttendancePlugin.Service
{
    [Chipject]
    public class AttendanceDataService(ILogger<AttendanceDataService> log, ChipBotDbContextFactory<AttendanceDbContext> dbContextFactory) : ChipService<AttendanceDataService>(log)
    {
        private readonly ChipBotDbContextFactory<AttendanceDbContext> dbContextFactory = dbContextFactory;

        public UserAttendanceApi GetDataFor(ulong userId, DateTimeOffset date)
        {
            var day = date.Date;

            var context = dbContextFactory.CreateDbContext();
            var events = context.AttendanceEvents.Where(ev => ev.UserId == userId && ev.TimeStamp.Date == day);
            var voiceEvents = context.AttendanceVoiceEvents.Where(ev => ev.UserId == userId && (ev.StartTime.Date == day || ev.EndTime.Date == day));

            return new UserAttendanceApi()
            {
                UserId = userId,
                Events = [.. events.Select(ev => ev.ToApi())],
                VoiceEvents = [.. voiceEvents.Select(ev => ev.ToApi())],
            };
        }

        public DayReport GetReportFor(DateTimeOffset date)
        {
            var day = date.Date;

            var context = dbContextFactory.CreateDbContext();
            return context.AttendanceReports.FirstOrDefault(r => r.Date.Date == day);
        }

        public Dictionary<DateTimeOffset, DayReport> GetReportFor(DateTimeOffset date, int lastDays = 7)
        {
            var day = date.Date;

            var result = new Dictionary<DateTimeOffset, DayReport>();

            var context = dbContextFactory.CreateDbContext();

            for (var i = 0; i < lastDays; i++)
            {
                var key = day.AddDays(-i);
                var report = context.AttendanceReports.FirstOrDefault(r => r.Date.Date == key);
                result.Add(key, report);
            }

            return result;
        }
    }
}
