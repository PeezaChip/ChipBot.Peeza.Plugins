﻿using AttendancePlugin.Data;
using AttendancePlugin.Data.Models;
using AttendancePlugin.Models;
using AttendancePlugin.Shared.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;

namespace AttendancePlugin.Service
{
    [Chipject]
    public class AttendanceService(DiscordSocketClient discordSocketClient, SettingsService<GlobalConfig> globalConfig, ChipBotDbContextFactory<AttendanceDbContext> attendanceDbContextFactory, ILogger<AttendanceService> log) : BackgroundService<AttendanceService>(log)
    {
        private readonly DiscordSocketClient discordSocketClient = discordSocketClient;
        private readonly ChipBotDbContextFactory<AttendanceDbContext> attendanceDbContextFactory = attendanceDbContextFactory;
        private readonly SettingsService<GlobalConfig> globalConfig = globalConfig;

        private readonly Dictionary<ulong, VoiceSession> sessions = [];

        private bool _isWorking = false;
        public override bool IsWorking => _isWorking;

        public override Task StartAsync()
        {
            if (_isWorking) return Task.CompletedTask;
            _isWorking = true;
            discordSocketClient.UserVoiceStateUpdated += DiscordSocketClient_UserVoiceStateUpdated;
            discordSocketClient.MessageReceived += DiscordSocketClient_MessageReceived;
            discordSocketClient.ReactionAdded += DiscordSocketClient_ReactionAdded;
            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            if (!_isWorking) return Task.CompletedTask;
            _isWorking = false;
            discordSocketClient.UserVoiceStateUpdated -= DiscordSocketClient_UserVoiceStateUpdated;
            discordSocketClient.MessageReceived -= DiscordSocketClient_MessageReceived;
            discordSocketClient.ReactionAdded -= DiscordSocketClient_ReactionAdded;
            return Task.CompletedTask;
        }

        private async Task DiscordSocketClient_ReactionAdded(Cacheable<IUserMessage, ulong> message, Cacheable<IMessageChannel, ulong> channel, SocketReaction reaction)
        {
            if ((await channel.GetOrDownloadAsync()) is not IGuildChannel gc) return;
            if (globalConfig.Settings.MainGuildId != gc.Guild.Id) return;

            PutEvent(reaction.UserId, AttendanceEventEnum.Reaction, message.Id);
        }

        private Task DiscordSocketClient_MessageReceived(SocketMessage message)
        {
            if (message.Channel is not IGuildChannel gc) return Task.CompletedTask;
            if (globalConfig.Settings.MainGuildId != gc.Guild.Id) return Task.CompletedTask;

            PutEvent(message.Author.Id, AttendanceEventEnum.Message, message.Id);
            return Task.CompletedTask;
        }

        private Task DiscordSocketClient_UserVoiceStateUpdated(SocketUser user, SocketVoiceState oldState, SocketVoiceState newState)
        {
            if (oldState.VoiceChannel == null && newState.VoiceChannel == null) return Task.CompletedTask;

            if (newState.VoiceChannel == null || oldState.VoiceChannel != newState.VoiceChannel)
            {
                EndSession(user.Id);
            }

            if (newState.VoiceChannel == null) return Task.CompletedTask;
            StartSession(user.Id, newState);

            return Task.CompletedTask;
        }

        private void StartSession(ulong userId, SocketVoiceState state)
        {
            if (globalConfig.Settings.MainGuildId != state.VoiceChannel.Guild.Id) return;

            if (!sessions.TryGetValue(userId, out var session))
            {
                session = new VoiceSession() { VoiceChannelId = state.VoiceChannel.Id };
                sessions.Add(userId, session);
            }

            if (state.IsStreaming && session.StartStream == null)
            {
                session.StartStream = DateTimeOffset.UtcNow;
            }
            else if (!state.IsStreaming && session.StartStream != null)
            {
                session.TimeStreamed += DateTimeOffset.UtcNow - session.StartStream.Value;
                session.StartStream = null;
            }

            if (state.IsSelfDeafened && session.StartDeaf == null)
            {
                session.StartDeaf = DateTimeOffset.UtcNow;
            }
            else if (!state.IsSelfDeafened && session.StartDeaf != null)
            {
                session.TimeDeafened += DateTimeOffset.UtcNow - session.StartDeaf.Value;
                session.StartDeaf = null;
            }
        }

        private void EndSession(ulong userId)
        {
            if (!sessions.TryGetValue(userId, out var session)) return;
            sessions.Remove(userId);

            var dbContext = attendanceDbContextFactory.CreateDbContext();

            var sessionData = new AttendanceVoiceEvent()
            {
                UserId = userId,
                StartTime = session.StartVoice,
                EndTime = DateTimeOffset.UtcNow,
                DurationDeafened = session.TimeDeafened,
                DurationStreamed = session.TimeStreamed,
                VoiceChannelId = session.VoiceChannelId,
            };

            if (session.StartStream != null)
            {
                sessionData.DurationStreamed += sessionData.EndTime - session.StartStream.Value;
                if (sessionData.DurationStreamed > sessionData.Duration)
                {
                    sessionData.DurationStreamed = sessionData.Duration;
                }
            }

            if (session.StartDeaf != null)
            {
                sessionData.DurationDeafened += sessionData.EndTime - session.StartDeaf.Value;
                if (sessionData.DurationDeafened > sessionData.Duration)
                {
                    sessionData.DurationDeafened = sessionData.Duration;
                }
            }

            dbContext.AttendanceVoiceEvents.Add(sessionData);

            dbContext.SaveChanges();
        }

        private void PutEvent(ulong userId, AttendanceEventEnum ev, ulong? subject = null)
        {
            var dbContext = attendanceDbContextFactory.CreateDbContext();

            dbContext.AttendanceEvents.Add(new AttendanceEvent()
            {
                UserId = userId,
                TimeStamp = DateTimeOffset.UtcNow,
                Event = ev,
                Subject = subject,
            });

            dbContext.SaveChanges();
        }
    }
}
