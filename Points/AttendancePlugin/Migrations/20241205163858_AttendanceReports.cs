﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AttendancePlugin.Migrations
{
    /// <inheritdoc />
    public partial class AttendanceReports : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "VoiceChannelId",
                table: "AttendanceVoiceEvents",
                type: "numeric(20,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20)");

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "AttendanceVoiceEvents",
                type: "numeric(20,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Subject",
                table: "AttendanceEvents",
                type: "numeric(20,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "numeric(20)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "AttendanceEvents",
                type: "numeric(20,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20)");

            migrationBuilder.CreateTable(
                name: "AttendanceReports",
                columns: table => new
                {
                    Date = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    FullPresence = table.Column<List<decimal>>(type: "numeric(20,0)[]", nullable: true),
                    LowActivity = table.Column<List<decimal>>(type: "numeric(20,0)[]", nullable: true),
                    MessageOnly = table.Column<List<decimal>>(type: "numeric(20,0)[]", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceReports", x => x.Date);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttendanceReports");

            migrationBuilder.AlterColumn<decimal>(
                name: "VoiceChannelId",
                table: "AttendanceVoiceEvents",
                type: "numeric(20)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "AttendanceVoiceEvents",
                type: "numeric(20)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Subject",
                table: "AttendanceEvents",
                type: "numeric(20)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "AttendanceEvents",
                type: "numeric(20)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,0)");
        }
    }
}
