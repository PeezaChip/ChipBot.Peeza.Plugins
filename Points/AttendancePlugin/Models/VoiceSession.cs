﻿namespace AttendancePlugin.Models
{
    public class VoiceSession
    {
        public DateTimeOffset StartVoice { get; } = DateTimeOffset.UtcNow;

        public ulong VoiceChannelId { get; set; }

        public DateTimeOffset? StartDeaf {  get; set; }
        public DateTimeOffset? StartStream {  get; set; }

        public TimeSpan TimeDeafened { get; set; }
        public TimeSpan TimeStreamed { get; set; }
    }
}
