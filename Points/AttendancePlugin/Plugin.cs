﻿using ChipBot.Core.Abstract;

namespace AttendancePlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "AttendancePlugin";
    }
}
