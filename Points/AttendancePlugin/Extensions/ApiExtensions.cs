﻿using AttendancePlugin.Data.Models;
using AttendancePlugin.Shared.Api.Models;

namespace AttendancePlugin.Extensions
{
    public static class ApiExtensions
    {
        public static AttendanceEventApi ToApi(this AttendanceEvent ev)
        {
            return new AttendanceEventApi()
            {
                Event = ev.Event,
                Subject = ev.Subject,
                TimeStamp = ev.TimeStamp,
            };
        }

        public static AttendanceVoiceEventApi ToApi(this AttendanceVoiceEvent ev)
        {
            return new AttendanceVoiceEventApi()
            {
                EndTime = ev.EndTime,
                StartTime = ev.StartTime,
                Duration = ev.Duration,
                DurationDeafened = ev.DurationDeafened,
                DurationStreamed = ev.DurationStreamed,
                VoiceChannelId = ev.VoiceChannelId,
            };
        }

        public static DayReportApi ToApi(this DayReport report)
        {
            return new DayReportApi()
            {
                Date = report.Date,
                FullPresence = report.FullPresence,
                LowActivity = report.LowActivity,
                MessageOnly = report.MessageOnly,
            };
        }
    }
}
