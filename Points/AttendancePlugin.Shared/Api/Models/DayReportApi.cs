﻿namespace AttendancePlugin.Shared.Api.Models
{
    public class DayReportApi
    {
        public DateTimeOffset Date { get; init; }

        public List<ulong> FullPresence { get; init; }
        public List<ulong> LowActivity { get; init; }
        public List<ulong> MessageOnly { get; init; }
    }
}
