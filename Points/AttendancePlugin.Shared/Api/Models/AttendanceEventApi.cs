﻿using AttendancePlugin.Shared.Models;

namespace AttendancePlugin.Shared.Api.Models
{
    public class AttendanceEventApi
    {
        public DateTimeOffset TimeStamp { get; init; }
        public AttendanceEventEnum Event { get; init; }

        public ulong? Subject { get; init; }
    }
}
