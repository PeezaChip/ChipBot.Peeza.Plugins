﻿namespace AttendancePlugin.Shared.Api.Models
{
    public class AttendanceVoiceEventApi
    {
        public DateTimeOffset StartTime { get; init; }
        public DateTimeOffset EndTime { get; init; }

        public ulong VoiceChannelId { get; init; }

        public TimeSpan Duration { get; init; }
        public TimeSpan DurationDeafened { get; init; }
        public TimeSpan DurationStreamed { get; init; }
    }
}
