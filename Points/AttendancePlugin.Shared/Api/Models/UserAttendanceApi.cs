﻿namespace AttendancePlugin.Shared.Api.Models
{
    public class UserAttendanceApi
    {
        public List<AttendanceEventApi> Events { get; set; }
        public List<AttendanceVoiceEventApi> VoiceEvents { get; set; }

        public ulong UserId { get; init; }

        public void Merge(UserAttendanceApi other)
        {
            Events.AddRange(other.Events);
            VoiceEvents.AddRange(other.VoiceEvents);

            Events = [.. Events.DistinctBy(ev => ev.TimeStamp).OrderByDescending(ev => ev.TimeStamp)];
            VoiceEvents = [.. VoiceEvents.DistinctBy(ev => ev.StartTime).OrderByDescending(ev => ev.StartTime)];
        }
    }
}
