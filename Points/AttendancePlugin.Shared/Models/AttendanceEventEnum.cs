﻿namespace AttendancePlugin.Shared.Models
{
    public enum AttendanceEventEnum
    {
        Message,
        Reaction,
    }
}
