﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PointsPlugin.Services;
using RafflePlugin.Services;
using RafflePlugin.Shared;

namespace RafflePlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "points", Version = "v1", Title = "ChipBot Points V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class RaffleController : ControllerBase
    {
        private readonly RaffleService raffleService;
        private readonly PointsService pointsService;

        public RaffleController(RaffleService raffleService, PointsService pointsService)
        {
            this.raffleService = raffleService;
            this.pointsService = pointsService;
        }

        [HttpPost("roll")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RaffleResult))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        public IActionResult PostRoll(long? points)
        {
            var userPoints = pointsService.GetPoints(HttpContext.GetCurrentDiscordUserId());
            return points.HasValue
                ? userPoints < points
                    ? BadRequest("Not enough points")
                    : Ok(DoRaffle(points.Value, false))
                : Ok(DoRaffle(userPoints, true));
        }

        [HttpGet("statistics")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RaffleStatistics))]
        public IActionResult GetStatistics()
            => Ok(raffleService.GetUserData(HttpContext.GetCurrentDiscordUserId()));

        [HttpGet("leaderboard")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RaffleLeaderboardleStatistics))]
        public IActionResult GetLeaderboard()
            => Ok(raffleService.GetLeaderboard());

        private RaffleResult DoRaffle(long points, bool manMode)
        {
            var result = raffleService.DoRaffle(HttpContext.GetCurrentDiscordUserId(), points, manMode);
            return result;
        }
    }
}
