﻿using ChipBot.Core.Implementations;
using Discord.Interactions;
using PointsPlugin.Attributes;
using PointsPlugin.Services;
using RafflePlugin.Services;

namespace RafflePlugin.Modules
{
    [Group("raffle", "raffle related commands")]
    public class RaffleModule : ChipInteractionModuleBase
    {
        public PointsService Points { get; set; }
        public RaffleService Raffle { get; set; }

        public string Currency => Points.CurrencyName;

        [SlashCommand("roll", "You need atleast 75 on dice to win. Also there is 2% chance to DOUBLE your bet")]
        public async Task Default([Summary("amount", "Points to raffle")][MinValue(10)][HasEnoughPoints] long points)
            => await DoRaffleAndReply(points, false);

        [SlashCommand("roll-all", "Be a man. You need atleast 75 on dice to win. Also there is 2% chance to TRIPLE your bet")]
        public async Task All()
        {
            var points = Points.GetPoints(Context.User);
            if (points <= 0)
            {
                await ReplyFailAsync();
                return;
            }

            await DoRaffleAndReply(points, true);
        }

        private async Task DoRaffleAndReply(long points, bool manMode)
        {
            var res = Raffle.DoRaffle(Context.User, points, manMode);
            await RespondAsync(string.Format(res.ResultMessage, Context.User.Mention, res.Rolled, res.PointsWon, Currency, res.CurrentPoints));
        }

        [SlashCommand("statistics", "Displays raffle statistics")]
        public async Task Statistics()
        {
            var data = Raffle.GetLeaderboard();
            var userData = Raffle.GetUserData(Context.User.Id);

            var reply = "";
            if (data.BiggestWinUserId > 0)
            {
                reply += $"Biggest winner: <@{data.BiggestWinUserId}> - `{data.BiggestWin}`{Currency}\n";
                reply += $"Biggest total winner: <@{data.TotalPtsWonUserId}> - `{data.TotalPtsWon}`{Currency}\n";
                
            }
            if (data.BiggestLoseUserId > 0)
            {
                reply += $"Biggest loser: <@{data.BiggestLoseUserId}> - `{data.BiggestLose}`{Currency}\n";
                reply += $"Biggest total loser: <@{data.TotalPtsLostUserId}> - `{data.TotalPtsLost}`{Currency}\n";
            }

            if (userData != null)
            {
                reply +=
                    $"\n{Context.User.Mention} stats:\n" +
                    $"You've won {userData.TotalPtsWon}{Currency} over {userData.TotalWon} wins.\n" +
                    $"You've lost {userData.TotalPtsLost}{Currency} over {userData.TotalLost} loses.";
            }
            await RespondAsync(reply);
        }
    }
}
