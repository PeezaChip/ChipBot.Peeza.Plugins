﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using Discord;
using Microsoft.Extensions.Logging;
using PointsPlugin.Services;
using RafflePlugin.Data;
using RafflePlugin.Data.Models;
using RafflePlugin.Shared;

namespace RafflePlugin.Services
{
    [Chipject]
    public class RaffleService : ChipService<RaffleService>
    {
        private readonly Random random;
        private readonly PointsService pointsService;

        private readonly ChipBotDbContextFactory<RaffleDbContext> raffleDbContextFactory;
        private readonly ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory;

        public RaffleService(ILogger<RaffleService> log, Random random, PointsService pointsService,
            ChipBotDbContextFactory<RaffleDbContext> raffleDbContextFactory, ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory)
            : base(log)
        {
            this.random = random;
            this.pointsService = pointsService;
            this.raffleDbContextFactory = raffleDbContextFactory;
            this.chipBotDbContextFactory = chipBotDbContextFactory;

            SetDefaultMessages();
        }

        public RaffleResult DoRaffle(IUser user, long points, bool manMode)
            => DoRaffle(user.Id, points, manMode);
        public RaffleResult DoRaffle(ulong userId, long points, bool manMode)
        {
            if (points < 10)
            {
                throw new Exception("You can't raffle less than 10 points");
            }

            var currentPoints = pointsService.GetPoints(userId);
            if (points >= currentPoints)
            {
                throw new Exception("You don't have that many points to raffle");
            }

            var rolled = random.Next(1, 101);
            var res = new RaffleResult() { Rolled = rolled };

            if (rolled >= 99)
            {
                points *= manMode ? 3 : 2;
                res.PointsWon = points;
                res.ResultMessage = manMode ? GetMessage(BigWinFormatKey) : GetMessage(WinFormatKey);
                res.CurrentPoints = pointsService.AwardPoints(userId, points, "Raffle reward");
                LogOperation(userId, points, true);
            }
            else if (rolled >= 75)
            {
                res.PointsWon = points;
                res.ResultMessage = GetMessage(WinFormatKey);
                res.CurrentPoints = pointsService.AwardPoints(userId, points, "Raffle reward");
                LogOperation(userId, points, true);
            }
            else
            {
                res.PointsWon = points;
                res.ResultMessage = manMode ? GetMessage(BigLoseFormatKey) : GetMessage(LoseFormatKey);
                res.CurrentPoints = pointsService.ChangePoints(userId, -points, "Raffle lost");
                LogOperation(userId, points, false);
            }

            return res;
        }

        private void SetDefaultMessages()
        {
            using var dbContext = chipBotDbContextFactory.CreateDbContext();
            var shouldSave = false;

            foreach (var (key, str) in KeyDict)
            {
                var row = dbContext.Dictionary.Find(key);
                if (row != null) continue;

                dbContext.Dictionary.Add(new ChipKeyValue() { Key = key, Value = str });
                shouldSave = true;
            }

            if (shouldSave)
            {
                dbContext.SaveChanges();
            }
        }

        private string GetMessage(string key)
        {
            using var dbContext = chipBotDbContextFactory.CreateDbContext();
            var row = dbContext.Dictionary.Find(key);

            return row != null ? row.Value : KeyDict[key];
        }

        private void LogOperation(ulong userid, long pts, bool won)
        {
            logger.LogDebug($"Logging user {userid} {(won ? "won" : "lost")} {pts}pts");
            using var dbContext = raffleDbContextFactory.CreateDbContext();

            var needUpdate = true;
            var userStats = dbContext.RaffleStats.Find(userid);
            if (userStats == null)
            {
                userStats = new() { UserId = userid };
                dbContext.RaffleStats.Add(userStats);
                needUpdate = false;
            }

            if (won)
            {
                userStats.TotalWon++;
                userStats.TotalPtsWon += pts;
                if (userStats.BiggestWin <= pts)
                {
                    userStats.BiggestWin = pts;
                }
            }
            else
            {
                userStats.TotalLost++;
                userStats.TotalPtsLost += pts;
                if (userStats.BiggestLose <= pts)
                {
                    userStats.BiggestLose = pts;
                }
            }

            if (needUpdate)
            {
                dbContext.RaffleStats.Update(userStats);
            }
            dbContext.SaveChanges();
        }

        public IEnumerable<RaffleStat> GetData()
        {
            using var dbContext = raffleDbContextFactory.CreateDbContext();
            return dbContext.RaffleStats.ToList();
        }

        public RaffleLeaderboardleStatistics GetLeaderboard()
        {
            var stats = GetData();

            var biggestWinner = stats.OrderByDescending(d => d.BiggestWin).FirstOrDefault();
            var biggestLoser = stats.OrderByDescending(d => d.BiggestLose).FirstOrDefault();

            var biggestTotalWinner = stats.OrderByDescending(d => d.TotalPtsWon).FirstOrDefault();
            var biggestTotalLoser = stats.OrderByDescending(d => d.TotalPtsWon).FirstOrDefault();

            var result = new RaffleLeaderboardleStatistics()
            {
                BiggestWin = biggestWinner?.BiggestWin ?? 0,
                BiggestWinUserId = biggestWinner?.UserId ?? 0,

                BiggestLose = biggestLoser?.BiggestLose ?? 0,
                BiggestLoseUserId = biggestLoser?.UserId ?? 0,

                TotalPtsWon = biggestTotalWinner?.TotalPtsWon ?? 0,
                TotalPtsWonUserId = biggestTotalWinner?.UserId ?? 0,

                TotalPtsLost = biggestTotalLoser?.TotalPtsLost ?? 0,
                TotalPtsLostUserId = biggestTotalLoser?.UserId ?? 0,
            };

            return result;
        }

        public RaffleStat GetUserData(IUser user) => GetUserData(user.Id);
        public RaffleStat GetUserData(ulong id)
        {
            using var dbContext = raffleDbContextFactory.CreateDbContext();
            return dbContext.RaffleStats.Find(id);
        }

        private readonly Dictionary<string, string> KeyDict = new()
        {
            { BigWinFormatKey, BigWinFormat },
            { WinFormatKey, WinFormat },
            { LoseFormatKey, LoseFormat },
            { BigLoseFormatKey, BigLoseFormat },
        };

        private const string BigWinFormatKey = "raffle_big_win_format";
        private const string WinFormatKey = "raffle_win_format";
        private const string LoseFormatKey = "raffle_lose_format";
        private const string BigLoseFormatKey = "raffle_big_lose_format";
        private const string BigWinFormat = "{0} WOW! You've rolled {1} and __won__ {2}{3}. You have {4}{3} now!";
        private const string WinFormat = "{0} congrats! You've rolled {1} and __won__ {2}{3}. You have {4}{3} now!";
        private const string LoseFormat = "{0} what a shame. You've rolled only {1} and __lost__ {2}{3}. You have {4}{3} now.";
        private const string BigLoseFormat = "{0} atleast you can call yourself a man now. You've rolled {1}. You lost {2}{3}.";
    }
}
