﻿using RafflePlugin.Data.Models;
using RafflePlugin.Shared;

namespace RafflePlugin.Extensions
{
    public static class ApiExtensions
    {
        public static RaffleStatistics ToApi(this RaffleStat raffleStat)
        {
            return new RaffleStatistics()
            {
                BiggestLose = raffleStat.BiggestLose,
                BiggestWin = raffleStat.BiggestWin,
                TotalLost = raffleStat.TotalLost,
                TotalWon = raffleStat.TotalWon,
                TotalPtsLost = raffleStat.TotalPtsLost,
                TotalPtsWon = raffleStat.TotalPtsWon,
            };
        }
    }
}
