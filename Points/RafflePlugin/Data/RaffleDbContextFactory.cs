﻿using ChipBot.Core.Data;

namespace RafflePlugin.Data
{
    public class RaffleDbContextFactory : ChipBotDbContextFactory<RaffleDbContext> { }
}
