﻿using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using RafflePlugin.Data.Models;

namespace RafflePlugin.Data
{
    [ChipDbContext("RafflePlugin")]
    public class RaffleDbContext : DbContext
    {
        public DbSet<RaffleStat> RaffleStats { get; set; }

        public RaffleDbContext(DbContextOptions options) : base(options) { }
    }
}
