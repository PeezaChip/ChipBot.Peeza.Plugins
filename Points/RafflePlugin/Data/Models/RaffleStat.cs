﻿using RafflePlugin.Shared;
using System.ComponentModel.DataAnnotations;

namespace RafflePlugin.Data.Models
{
    public class RaffleStat : RaffleStatistics
    {
        [Key]
        public ulong UserId { get; set; }
    }
}
