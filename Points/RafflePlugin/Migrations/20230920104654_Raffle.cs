﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RafflePlugin.Migrations
{
    /// <inheritdoc />
    public partial class Raffle : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RaffleStats",
                columns: table => new
                {
                    UserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    TotalWon = table.Column<long>(type: "bigint", nullable: false),
                    BiggestWin = table.Column<long>(type: "bigint", nullable: false),
                    TotalLost = table.Column<long>(type: "bigint", nullable: false),
                    BiggestLose = table.Column<long>(type: "bigint", nullable: false),
                    TotalPtsWon = table.Column<long>(type: "bigint", nullable: false),
                    TotalPtsLost = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaffleStats", x => x.UserId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RaffleStats");
        }
    }
}
