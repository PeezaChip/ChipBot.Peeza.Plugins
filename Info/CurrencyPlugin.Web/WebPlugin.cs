﻿using ChipBot.Core.Web.Abstract;

namespace CurrencyPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "CurrencyPlugin";
    }
}
