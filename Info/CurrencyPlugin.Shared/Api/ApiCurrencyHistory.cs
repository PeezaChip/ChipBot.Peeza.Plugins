﻿namespace CurrencyPlugin.Shared.Api
{
    public class ApiCurrencyHistory
    {
        public double Current { get; set; }
        public int CurrentNominal { get; set; }
        public double Previous { get; set; }
        public int PreviousNominal { get; set; }
    }
}
