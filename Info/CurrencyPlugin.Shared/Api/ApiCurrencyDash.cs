﻿namespace CurrencyPlugin.Shared.Api
{
    public class ApiCurrencyDash
    {
        public List<ApiCurrencyInfo> Currencies { get; set; }
        public Dictionary<string, ApiCurrencyHistory> Histories { get; set; }
    }
}
