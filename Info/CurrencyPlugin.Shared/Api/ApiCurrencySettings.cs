﻿namespace CurrencyPlugin.Shared.Api
{
    public class ApiCurrencySettings
    {
        public List<ApiCurrencyInfo> Currencies { get; set; }
        public List<string> Subscriptions { get; set; }
    }
}
