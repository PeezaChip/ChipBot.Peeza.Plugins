﻿namespace CurrencyPlugin.Shared.Api
{
    public class ApiCurrencyInfo
    {
        public string Id { get; set; }
        public string CharCode { get; set; }
        public string Name { get; set; }
    }
}
