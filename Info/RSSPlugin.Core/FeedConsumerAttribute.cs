﻿using ChipBot.Core.Attributes;

namespace RSSPlugin.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FeedConsumerAttribute(string uri, ChipjectType type = ChipjectType.Singleton)
        : ChipjectAttribute(type, feedConsumerType)
    {
        public string Uri { get; set; } = uri;

        private static readonly Type feedConsumerType = typeof(IFeedConsumer);
    }
}
