﻿using Discord;

namespace RSSPlugin.Core
{
    public static class FeedExtensions
    {
        public static EmbedBuilder GetEmbed(this FeedInfo info)
        {
            var eb = new EmbedBuilder()
                .WithTitle(string.IsNullOrWhiteSpace(info.Uri?.Title) ? string.IsNullOrWhiteSpace(info.Title) ? "RSS" : info.Title : info.Uri?.Title);

            if (!string.IsNullOrWhiteSpace(info.Description))
            {
                eb.WithDescription(info.Description);
            }

            if (info.Uri != null)
            {
                eb.WithUrl(info.Uri.Uri.ToString());
            }

            if (!string.IsNullOrWhiteSpace(info.Image))
            {
                eb.WithThumbnailUrl(info.Image);
            }

            return eb;
        }
    }
}
