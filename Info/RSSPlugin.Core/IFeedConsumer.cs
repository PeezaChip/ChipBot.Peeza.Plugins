﻿namespace RSSPlugin.Core
{
    public interface IFeedConsumer
    {
        Task Consume(Feed feed);
    }
}
