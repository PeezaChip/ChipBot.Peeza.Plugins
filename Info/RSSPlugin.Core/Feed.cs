﻿using System.ServiceModel.Syndication;

namespace RSSPlugin.Core
{
    public class Feed
    {
        public FeedInfo Info { get; set; }
        public List<SyndicationItem> Items { get; set; }
    }
}
