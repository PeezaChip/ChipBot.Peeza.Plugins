﻿using System.ServiceModel.Syndication;

namespace RSSPlugin.Core
{
    public class FeedInfo
    {
        public SyndicationLink Uri { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }
}
