﻿using ChipBot.Core.Implementations;
using CurrencyPlugin.Services;
using Discord;
using Discord.Interactions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyPlugin.Modules
{
    [Group("currency", "currency commands")]
    public class CurrencyModule : ChipInteractionModuleBase
    {
        public CurrencyService Api { get; set; }

        [SlashCommand("subscribe", "Subscribes to currency")]
        public async Task Subscribe([Summary("currency"), Autocomplete(typeof(CurrencyAutoCompleter))] string currencyId)
        {
            Api.Subscribe(Context.User, currencyId);
            await ReplySuccessAsync();
        }

        [SlashCommand("unsubscribe", "Unsubscribes from currency")]
        public async Task Unsubscribe([Summary("currency"), Autocomplete(typeof(CurrencyAutoCompleter))] string currencyId)
        {
            Api.Unsubscribe(Context.User, currencyId);
            await ReplySuccessAsync();
        }

        private class CurrencyAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var service = services.GetRequiredService<CurrencyService>();
                var currencies = service.GetCurrencyList();

                var options = currencies.Select(o => new AutocompleteResult($"{o.CharCode} - {o.Name}", o.Id));

                var query = autocompleteInteraction.Data.Current.Value.ToString().Trim();

                if (!string.IsNullOrWhiteSpace(query))
                {
                    options = options.Where(o => o.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
                }

                return Task.FromResult(AutocompletionResult.FromSuccess(options.Take(10)));
            }
        }
    }
}
