﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using CurrencyPlugin.Models;
using Microsoft.Extensions.Logging;
using RestEase;
using System;
using System.Net.Http;

namespace CurrencyPlugin.Services
{
    [Chipject]
    public class CurrencyApiService : ApiService<ICurrencyApi>
    {
        private const string ApiUrl = "http://www.cbr.ru/scripts/";

        public CurrencyApiService(ILogger<CurrencyApiService> log, SettingsService<NetworkConfig> provider) : base(log, provider)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(ApiUrl)
                })
            {
                ResponseDeserializer = new CurrencyDeserializer()
            }.For<ICurrencyApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
