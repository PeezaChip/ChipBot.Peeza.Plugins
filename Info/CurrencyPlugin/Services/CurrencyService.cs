﻿using BirthdayPlugin.Data.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using CurrencyPlugin.Data;
using CurrencyPlugin.Data.Models;
using CurrencyPlugin.Extensions;
using CurrencyPlugin.Models;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyPlugin.Services
{
    [Notify]
    [Chipject]
    public class CurrencyService : ChipService<CurrencyService>, INotifyService
    {
        private readonly ChipBotDbContextFactory<CurrencyDbContext> dbContextFactory;
        private readonly CurrencyApiService api;
        private readonly UserSettingsService<CurrencyUserConfig> userConfig;
        private readonly DiscordSocketClient discord;

        public CurrencyService(ILogger<CurrencyService> log, ChipBotDbContextFactory<CurrencyDbContext> dbContextFactory, CurrencyApiService api, UserSettingsService<CurrencyUserConfig> userConfig, DiscordSocketClient discord) : base(log)
        {
            this.dbContextFactory = dbContextFactory;
            this.api = api;
            this.userConfig = userConfig;
            this.discord = discord;
        }

        public async Task Notify()
        {
            await UpdateCurrencies();
            await SendCurrenciesToUsers();
        }

        private async Task UpdateCurrencies()
        {
            logger.LogInformation("Updating currency info");
            using var dbContext = dbContextFactory.CreateDbContext();

            var daily = await api.ApiClient.GetCurrencies();
            var date = DateTimeOffset.UtcNow;

            foreach (var currency in daily.Valute)
            {
                var model = dbContext.Currencies.Find(currency.ID);
                if (model == null)
                {
                    model = currency.ToCurrencyInfo();
                    dbContext.Currencies.Add(model);
                }
                else
                {
                    model.CharCode = currency.CharCode;
                    model.Name = currency.Name;
                    model.NumCode = currency.NumCode;
                    dbContext.Currencies.Update(model);
                }

                dbContext.CurrenciesHistory.Add(new CurrencyValue()
                {
                    CurrencyInfo = model,
                    Date = date,
                    Value = double.Parse(currency.Value, CurrencyDeserializer.Culture),
                    Nominal = currency.Nominal
                });
            }

            dbContext.SaveChanges();
            logger.LogInformation("Updated currency info");
        }

        private async Task SendCurrenciesToUsers()
        {
            using var dbContext = dbContextFactory.CreateDbContext();

            foreach (var (userId, userSettings) in userConfig.UserSettings)
            {
                try
                {
                    if (!userSettings.SubscribedCurrencies.Any()) continue;

                    logger.LogInformation($"Sending user ({userId}) daily currencies");
                    var currencies = dbContext.Currencies.Where(c => userSettings.SubscribedCurrencies.Contains(c.Id)).ToList();

                    var embed = GetEmbedBase();
                    foreach (var currency in currencies)
                    {
                        var history = dbContext.CurrenciesHistory.Where(c => c.CurrencyInfoId == currency.Id).OrderByDescending(c => c.Date).Take(2).ToList();
                        if (!history.Any()) continue;

                        embed.AddField(currency.ToEmbedField(history.First(), history.Count > 1 ? history.Last() : null));
                    }

                    var ch = await discord.GetUser(userId).CreateDMChannelAsync();
                    await ch.SendMessageAsync(embed: embed.Build());

                    await Task.Delay(1000);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't send user ({userId}) daily currencies");
                }
            }
        }

        public IEnumerable<CurrencyInfo> GetCurrencyList()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.Currencies.ToList();
        }

        public void Subscribe(IUser user, string sub) => Subscribe(user.Id, sub);
        public void Subscribe(ulong userId, string sub)
        {
            var cfg = userConfig.Get(userId);

            if (cfg.SubscribedCurrencies.Contains(sub)) return;

            cfg.SubscribedCurrencies.Add(sub);
            userConfig.SaveConfiguration(cfg, userId);
        }

        public void Unsubscribe(IUser user, string sub) => Unsubscribe(user.Id, sub);
        public void Unsubscribe(ulong userId, string sub)
        {
            var cfg = userConfig.Get(userId);

            if (!cfg.SubscribedCurrencies.Contains(sub)) return;

            cfg.SubscribedCurrencies.Remove(sub);
            userConfig.SaveConfiguration(cfg, userId);
        }

        public static EmbedBuilder GetEmbedBase()
            => new EmbedBuilder().WithCurrentTimestamp().WithThumbnailUrl("https://www.cbr.ru/common/images/home-logo.svg");
    }
}
