﻿using System.Collections.Generic;

namespace CurrencyPlugin
{
    public class CurrencyUserConfig
    {
        public List<string> SubscribedCurrencies { get; set; } = new List<string>();
    }
}
