﻿using RestEase;
using System.Threading.Tasks;

namespace CurrencyPlugin.Models
{
    public interface ICurrencyApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("XML_daily.asp")]
        Task<ValCurs> GetCurrencies();
    }
}
