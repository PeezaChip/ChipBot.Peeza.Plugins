﻿using System;
using System.Xml.Serialization;

namespace CurrencyPlugin.Models
{
    [Serializable]
    public class ValCursValute
    {
        public ushort NumCode { get; set; }

        public string CharCode { get; set; }

        public ushort Nominal { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        [XmlAttribute]
        public string ID { get; set; }
    }
}
