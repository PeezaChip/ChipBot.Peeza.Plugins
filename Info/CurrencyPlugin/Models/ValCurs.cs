﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace CurrencyPlugin.Models
{
    [Serializable]
    public class ValCurs
    {
        [XmlElement("Valute")]
        public ValCursValute[] Valute { get; set; }

        [XmlAttribute]
        public string Date { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        public Dictionary<string, ValCursValute> ToDictionary()
        {
            return Valute.ToDictionary(kv => kv.CharCode, kv => kv);
        }
    }
}
