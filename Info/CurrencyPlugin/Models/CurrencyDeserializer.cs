﻿using RestEase;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Xml.Serialization;

namespace CurrencyPlugin.Models
{
    public class CurrencyDeserializer : ResponseDeserializer
    {
        public static readonly CultureInfo Culture = CultureInfo.GetCultureInfo("ru-RU");

        public override T Deserialize<T>(string content, HttpResponseMessage response, ResponseDeserializerInfo info)
        {
            var serializer = new XmlSerializer(typeof(ValCurs), new[] { typeof(ValCursValute) });
            using var stringReader = new StringReader(content);
            return (T)serializer.Deserialize(stringReader);
        }
    }
}
