﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CurrencyPlugin.Migrations
{
    /// <inheritdoc />
    public partial class Currency : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    NumCode = table.Column<int>(type: "integer", nullable: false),
                    CharCode = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CurrenciesHistory",
                columns: table => new
                {
                    CurrencyInfoId = table.Column<string>(type: "text", nullable: false),
                    Date = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Nominal = table.Column<int>(type: "integer", nullable: false),
                    Value = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrenciesHistory", x => new { x.CurrencyInfoId, x.Date });
                    table.ForeignKey(
                        name: "FK_CurrenciesHistory_Currencies_CurrencyInfoId",
                        column: x => x.CurrencyInfoId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrenciesHistory");

            migrationBuilder.DropTable(
                name: "Currencies");
        }
    }
}
