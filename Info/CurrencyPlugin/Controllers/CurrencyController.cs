﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using ChipBot.Core.Data;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using CurrencyPlugin.Data;
using CurrencyPlugin.Extensions;
using CurrencyPlugin.Services;
using CurrencyPlugin.Shared.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "currency", Version = "v1", Title = "ChipBot Currency V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class CurrencyController : ControllerBase
    {
        private readonly ChipBotDbContextFactory<CurrencyDbContext> dbContextFactory;
        private readonly UserSettingsService<CurrencyUserConfig> userConfig;
        private readonly CurrencyService currencyService;

        public CurrencyController(ChipBotDbContextFactory<CurrencyDbContext> dbContextFactory, UserSettingsService<CurrencyUserConfig> userConfig, CurrencyService currencyService)
        {
            this.dbContextFactory = dbContextFactory;
            this.userConfig = userConfig;
            this.currencyService = currencyService;
        }

        [HttpGet("subscriptionList")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiCurrencySettings))]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesUserCache)]
        public IActionResult SubscriptionList()
        {
            var userId = HttpContext.GetCurrentDiscordUserId();
            var settings = userConfig.Get(userId);

            var result = new ApiCurrencySettings()
            {
                Currencies = currencyService.GetCurrencyList().Select(c => c.ToApiCurrencyInfo()).ToList(),
                Subscriptions = settings.SubscribedCurrencies
            };
            return Ok(result);
        }

        [HttpGet("dashboard")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiCurrencyDash))]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesUserCache)]
        public IActionResult Dashboard()
        {
            var userId = HttpContext.GetCurrentDiscordUserId();
            var settings = userConfig.Get(userId);

            if (!settings.SubscribedCurrencies.Any()) return null;

            using var dbContext = dbContextFactory.CreateDbContext();
            var currencies = dbContext.Currencies.Where(c => settings.SubscribedCurrencies.Contains(c.Id)).ToList();

            var result = new ApiCurrencyDash()
            {
                Currencies = new List<ApiCurrencyInfo>(),
                Histories = new Dictionary<string, ApiCurrencyHistory>()
            };

            foreach (var currency in currencies)
            {
                var history = dbContext.CurrenciesHistory.Where(c => c.CurrencyInfoId == currency.Id).OrderByDescending(c => c.Date).Take(2).ToList();
                if (!history.Any()) continue;

                var (k, v) = currency.ToApiDashboard(history.First(), history.Count > 1 ? history.Last() : null);
                result.Currencies.Add(k);
                result.Histories.Add(k.Id, v);
            }

            return Ok(result);
        }

        [HttpPost("subscribe")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Subscribe(string currencyId)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var info = dbContext.Currencies.SingleOrDefault(c => c.Id == currencyId);
            if (info == null) return BadRequest();

            currencyService.Subscribe(HttpContext.GetCurrentDiscordUserId(), currencyId);
            return Ok();
        }

        [HttpPost("unsubscribe")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Unsubscribe(string currencyId)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var info = dbContext.Currencies.SingleOrDefault(c => c.Id == currencyId);
            if (info == null) return BadRequest();

            currencyService.Unsubscribe(HttpContext.GetCurrentDiscordUserId(), currencyId);
            return Ok();
        }
    }
}
