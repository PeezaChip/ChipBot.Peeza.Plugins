﻿using BirthdayPlugin.Data.Models;
using CurrencyPlugin.Data.Models;
using CurrencyPlugin.Models;
using CurrencyPlugin.Shared.Api;
using Discord;

namespace CurrencyPlugin.Extensions
{
    public static class ModelsExtensions
    {
        public static CurrencyInfo ToCurrencyInfo(this ValCursValute valute)
        {
            return new CurrencyInfo()
            {
                Id = valute.ID,
                CharCode = valute.CharCode,
                Name = valute.Name,
                NumCode = valute.NumCode
            };
        }

        public static ApiCurrencyInfo ToApiCurrencyInfo(this CurrencyInfo currencyInfo)
        {
            return new ApiCurrencyInfo()
            {
                Id = currencyInfo.Id,
                CharCode = currencyInfo.CharCode,
                Name = currencyInfo.Name,
            };
        }

        public static (ApiCurrencyInfo, ApiCurrencyHistory) ToApiDashboard(this CurrencyInfo currencyInfo, CurrencyValue newValue, CurrencyValue oldValue)
            => (currencyInfo.ToApiCurrencyInfo(), new ApiCurrencyHistory()
            {
                Current = newValue.Value,
                CurrentNominal = newValue.Nominal,
                Previous = oldValue?.Value ?? 0,
                PreviousNominal = oldValue?.Nominal ?? 0
            });

        public static EmbedFieldBuilder ToEmbedField(this CurrencyInfo currencyInfo, CurrencyValue newValue, CurrencyValue oldValue)
        {
            var embedField = new EmbedFieldBuilder()
                .WithName($"{currencyInfo.CharCode}")
                .WithIsInline(true);

            var value = $"{currencyInfo.CharCode}`{newValue.Nominal}` = `{newValue.Value:0.00}`₽";

            if (oldValue != null)
            {
                var percent = newValue.Value / oldValue.Value - 1;
                value += $"\n(`{percent:P2}` {(percent >= 0 ? "📈" : "📉")})";
            }

            embedField.Value = value;

            return embedField;
        }
    }
}
