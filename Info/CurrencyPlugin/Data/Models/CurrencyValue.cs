﻿using BirthdayPlugin.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace CurrencyPlugin.Data.Models
{
    [PrimaryKey(nameof(CurrencyInfoId), nameof(Date))]
    public class CurrencyValue
    {
        public string CurrencyInfoId { get; set; }
        public DateTimeOffset Date { get; set; }

        public ushort Nominal { get; set; }
        public double Value { get; set; }

        public virtual CurrencyInfo CurrencyInfo { get; set; }
    }
}
