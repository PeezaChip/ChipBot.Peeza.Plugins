﻿using CurrencyPlugin.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BirthdayPlugin.Data.Models
{
    public class CurrencyInfo
    {
        public ushort NumCode { get; set; }

        public string CharCode { get; set; }

        public string Name { get; set; }

        [Key]
        public string Id { get; set; }

        public virtual IEnumerable<CurrencyValue> CurrencyValues { get; set; }
    }
}
