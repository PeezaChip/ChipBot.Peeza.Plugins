﻿using BirthdayPlugin.Data.Models;
using ChipBot.Core.Attributes;
using CurrencyPlugin.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CurrencyPlugin.Data
{
    [ChipDbContext("CurrencyPlugin")]
    public class CurrencyDbContext : DbContext
    {
        public DbSet<CurrencyInfo> Currencies { get; set; }
        public DbSet<CurrencyValue> CurrenciesHistory { get; set; }

        public CurrencyDbContext(DbContextOptions options) : base(options) { }
    }
}
