﻿using ChipBot.Core.Data;

namespace CurrencyPlugin.Data
{
    public class CurrencyDbContextFactory : ChipBotDbContextFactory<CurrencyDbContext> { }
}
