﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using KanobuPlugin.Configuration;
using KanobuPlugin.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanobuPlugin.Services
{
    [Notify(NoEarlierThan = 0, RepeatIntervalSeconds = 60 * 60)] // 1 hour
    [Chipject(ChipjectType.Transient)]
    public class KanobuService : ChipService<KanobuService>, INotifyService
    {
        private const string LastNewsIdKey = "kanobu_last_news_id";

        private readonly KanobuApiService kanobuApi;
        private readonly DiscordSocketClient discord;
        private readonly SettingsService<KanobuConfig> config;
        private readonly ChipBotDbContextFactory<ChipBotDbContext> dbContextFactory;

        public KanobuService(ILogger<KanobuService> logger, KanobuApiService kanobuApi, DiscordSocketClient discord,
            SettingsService<KanobuConfig> config, ChipBotDbContextFactory<ChipBotDbContext> dbContextFactory) : base(logger)
        {
            this.kanobuApi = kanobuApi;
            this.discord = discord;
            this.config = config;
            this.dbContextFactory = dbContextFactory;
        }

        public Task Notify() => GetNewsList();
        private async Task GetNewsList()
        {
            logger.LogInformation("Checking news");
            var news = (await kanobuApi.ApiClient.GetNews(limit: 30)).Results;
            var subChannels = config.Settings.Channels;

            var lastNewsId = GetLastNewsId();

            foreach (var article in news)
            {
                if (article.Id == lastNewsId) break;
                await Task.Delay(2500);

                try
                {
                    logger.LogDebug($"Getting extended news info {article.Id}");
                    var extended = await kanobuApi.ApiClient.GetNews(article.Id);
                    var shouldSendTo = new List<SubscriptionChannel>();

                    foreach (var sub in subChannels)
                    {
                        if (sub.Subscriptions.Any(s => s.ShouldSend(extended)))
                        {
                            shouldSendTo.Add(sub);
                        }
                    }

                    if (!shouldSendTo.Any()) continue;

                    logger.LogDebug($"Preparing embed for {article.Id}");
                    var eb = new EmbedBuilder()
                        .WithTitle(extended.Metadata.Opengraph.Title).WithImageUrl(extended.Metadata.Opengraph.ImageResized?.ToString()).WithUrl($"{KanobuApiService.KanobuBaseUrl}{extended.Metadata.Opengraph.Url}")
                        .WithColor(Color.LightOrange).WithDescription(extended.Metadata.Opengraph.Description)
                        .WithTimestamp(extended.Pubdate).WithThumbnailUrl("https://kanobu.ru/images/0d91381ee3fba214f904bc37320b9dfd.png");

                    if (extended.Author != null)
                    {
                        eb.WithAuthor(extended.Author.Name, url: KanobuApiService.KanobuBaseUrl);
                    }

                    var embed = eb.Build();

                    foreach (var channel in shouldSendTo)
                    {
                        try
                        {
                            if (channel.IsTextChannel)
                            {
                                logger.LogDebug($"Sending embed to channel {channel.Id}");
                                var ch = (ITextChannel)discord.GetChannel(channel.Id);
                                await ch.SendMessageAsync(embed: embed);
                            }
                            else
                            {
                                logger.LogDebug($"Sending embed to user {channel.Id}");
                                var ch = await discord.GetUser(channel.Id).CreateDMChannelAsync();
                                await ch.SendMessageAsync(embed: embed);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Couldn't send article {article.Id} to {channel.Id}");
                        }
                        await Task.Delay(1000);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't parse article {article.Id}");
                }
            }

            SetLastNewsId(news.First().Id);
        }

        private long GetLastNewsId()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var last = dbContext.Dictionary.Find(LastNewsIdKey);
            return last == null ? 0 : long.Parse(last.Value);
        }

        private void SetLastNewsId(long id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();

            var last = dbContext.Dictionary.Find(LastNewsIdKey);
            if (last == null)
            {
                last = new ChipBot.Core.Data.Models.ChipKeyValue()
                {
                    Key = LastNewsIdKey,
                    Value = id.ToString()
                };
                dbContext.Dictionary.Add(last);
            }
            else
            {
                last.Value = id.ToString();
                dbContext.Dictionary.Update(last);
            }

            dbContext.SaveChanges();
        }
    }
}
