﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using KanobuPlugin.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestEase;
using System;
using System.Net.Http;

namespace KanobuPlugin.Services
{
    [Chipject]
    public class KanobuApiService : ApiService<IKanobuApi>
    {
        private const string KanobuUrl = "https://kanobu.ru";
        private const string ApiUrn = "/api/v3/";
        private const string KanobuUri = KanobuUrl + ApiUrn;

        public static string KanobuBaseUrl => KanobuUrl;

        public KanobuApiService(ILogger<KanobuApiService> log, SettingsService<NetworkConfig> networkConfig) : base(log, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient(
                    new HttpClientHandler()
                    {
                        AutomaticDecompression = System.Net.DecompressionMethods.GZip
                    })
                {
                    BaseAddress = new Uri(KanobuUri)
                })
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    DateParseHandling = DateParseHandling.None,
                    NullValueHandling = NullValueHandling.Ignore,
                }
            }.For<IKanobuApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
