﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord;
using KanobuPlugin.Configuration;

namespace KanobuPlugin.Services
{
    [Chipject]
    public class KanobuSettingsService
    {
        private readonly SettingsService<KanobuConfig> config;

        public KanobuSettingsService(SettingsService<KanobuConfig> config)
            => this.config = config;

        public void Subscribe(IEntity<ulong> entity, long id, SubscriptionType type)
        {
            var sub = GetSubscription(id, type);

            switch (entity)
            {
                case IUser user:
                    config.Settings.AddSubscription(user, sub);
                    config.SaveConfiguration(config.Settings);
                    break;

                case ITextChannel textChannel:
                    config.Settings.AddSubscription(textChannel, sub);
                    config.SaveConfiguration(config.Settings);
                    break;
            }
        }

        public void Unsubscribe(IEntity<ulong> entity, long id, SubscriptionType type)
        {
            var sub = GetSubscription(id, type);

            switch (entity)
            {
                case IUser user:
                    config.Settings.RemoveSubscription(user, sub);
                    config.SaveConfiguration(config.Settings);
                    break;

                case ITextChannel textChannel:
                    config.Settings.RemoveSubscription(textChannel, sub);
                    config.SaveConfiguration(config.Settings);
                    break;
            }
        }

        private static Subscription GetSubscription(long id, SubscriptionType type)
            => new() { Id = id, Type = type };
    }
}
