﻿using System.Collections.Generic;

namespace KanobuPlugin.Configuration
{
    public class SubscriptionChannel
    {
        public bool IsTextChannel { get; set; }

        public ulong Id { get; set; }

        public List<Subscription> Subscriptions { get; set; } = new();
    }
}
