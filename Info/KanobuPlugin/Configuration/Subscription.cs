﻿namespace KanobuPlugin.Configuration
{
    public class Subscription
    {
        public long Id { get; set; }

        public SubscriptionType Type { get; set; }
    }
}
