﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using KanobuPlugin.Configuration;
using KanobuPlugin.Services;
using System;
using System.Threading.Tasks;

namespace KanobuPlugin.Modules
{
    [Group("kanobu", "kanobu commands")]
    public class KanobuModule : ChipInteractionModuleBase
    {
        public KanobuSettingsService KanobuSettings { get; set; }

        [SlashCommand("subscribe", "subscribes self to a tag or vertical")]
        public async Task Subscribe([Summary("type", "type of subscription")] SubscriptionType type, [Summary("id", "id of a tag")] int id)
        {
            try
            {
                KanobuSettings.Subscribe(Context.User, id, type);
                await ReplySuccessAsync();
            }
            catch (Exception ex)
            {
                await ReplyError(ex.Message);
            }
        }

        [SlashCommand("subscribe-channel", "subscribes channel self to a tag or vertical")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task Subscribe([Summary("channel", "channel to subscribe")] ITextChannel text, [Summary("type", "type of subscription")] SubscriptionType type, [Summary("id", "id of a tag")] int id)
        {
            try
            {
                KanobuSettings.Subscribe(text, id, type);
                await ReplySuccessAsync();
            }
            catch (Exception ex)
            {
                await ReplyError(ex.Message);
            }
        }

        [SlashCommand("unsubscribe", "unsubscribes self from a tag or vertical")]
        public async Task Unsubscribe([Summary("type", "type of subscription")] SubscriptionType type, [Summary("id", "id of a tag")] int id)
        {
            try
            {
                KanobuSettings.Unsubscribe(Context.User, id, type);
                await ReplySuccessAsync();
            }
            catch (Exception ex)
            {
                await ReplyError(ex.Message);
            }
        }

        [SlashCommand("unsubscribe-channel", "unsubscribes channel self from a tag or vertical")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task Unsubscribe([Summary("channel", "channel to subscribe")] ITextChannel text, [Summary("type", "type of subscription")] SubscriptionType type, [Summary("id", "id of a tag")] int id)
        {
            try
            {
                KanobuSettings.Unsubscribe(text, id, type);
                await ReplySuccessAsync();
            }
            catch (Exception ex)
            {
                await ReplyError(ex.Message);
            }
        }
    }
}
