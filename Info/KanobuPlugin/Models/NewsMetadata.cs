﻿using Newtonsoft.Json;

namespace KanobuPlugin.Models
{
    public class NewsMetadata
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("opengraph")]
        public NewsOpengraph Opengraph { get; set; }
    }
}
