﻿using Newtonsoft.Json;

namespace KanobuPlugin.Models
{
    public class NewsAuthor
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        //[JsonProperty("photo")]
        //public object Photo { get; set; }
    }
}
