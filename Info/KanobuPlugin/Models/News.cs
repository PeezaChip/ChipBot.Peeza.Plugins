﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KanobuPlugin.Models
{
    public class News
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("pubdate")]
        public DateTimeOffset Pubdate { get; set; }

        [JsonProperty("pic")]
        public NewsPic Pic { get; set; }

        //[JsonProperty("person")]
        //public object Person { get; set; }

        [JsonProperty("verticals")]
        public List<NewsTag> Verticals { get; set; }

        [JsonProperty("tags")]
        public List<NewsTag> Tags { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("comment_count")]
        public long CommentCount { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        //[JsonProperty("partner_stamp")]
        //public object PartnerStamp { get; set; }

        [JsonProperty("snippet")]
        public string Snippet { get; set; }

        [JsonProperty("author")]
        public NewsAuthor Author { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("favorite")]
        public bool Favorite { get; set; }
    }
}
