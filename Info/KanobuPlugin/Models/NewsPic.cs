﻿using Newtonsoft.Json;

namespace KanobuPlugin.Models
{
    public class NewsPic
    {
        [JsonProperty("thumb")]
        public string Thumb { get; set; }

        [JsonProperty("origin")]
        public string Origin { get; set; }
    }
}
