﻿using Newtonsoft.Json;

namespace KanobuPlugin.Models
{
    public class NewsOpengraph
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("image-resized")]
        public string ImageResized { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}