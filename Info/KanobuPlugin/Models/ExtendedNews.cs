﻿using Newtonsoft.Json;
using System;

namespace KanobuPlugin.Models
{
    public class ExtendedNews : News
    {
        [JsonProperty("pic_description")]
        public string PicDescription { get; set; }

        //[JsonProperty("person")]
        //public object Person { get; set; }

        //[JsonProperty("content")]
        //public NewsContent Content { get; set; }

        //[JsonProperty("source_title")]
        //public object SourceTitle { get; set; }

        //[JsonProperty("source_link")]
        //public object SourceLink { get; set; }

        [JsonProperty("metadata")]
        public NewsMetadata Metadata { get; set; }

        [JsonProperty("author_aux")]
        public string AuthorAux { get; set; }

        [JsonProperty("modified")]
        public DateTimeOffset Modified { get; set; }

        //[JsonProperty("games")]
        //public List<Game> Games { get; set; }

        [JsonProperty("is_published")]
        public bool IsPublished { get; set; }

        [JsonProperty("block_advertising")]
        public bool BlockAdvertising { get; set; }

        //[JsonProperty("plot")]
        //public object Plot { get; set; }

        [JsonProperty("ads_data")]
        public string AdsData { get; set; }

        [JsonProperty("is_comments_disabled")]
        public bool IsCommentsDisabled { get; set; }

        [JsonProperty("comments_channel_name")]
        public string CommentsChannelName { get; set; }

        [JsonProperty("content_type")]
        public long ContentType { get; set; }
    }
}
