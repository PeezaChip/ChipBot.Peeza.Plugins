﻿using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanobuPlugin.Models
{
    [Header("Accept-Encoding", "gzip")]
    public interface IKanobuApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("news/")]
        Task<KanobuPaging<News>> GetNews(string to = "", uint limit = 10, string verticals = "");

        [Get("news/{id}/")]
        Task<ExtendedNews> GetNews([Path] long id);

        [Get("verticals/")]
        Task<List<NewsTag>> GetVerticals();
    }
}
