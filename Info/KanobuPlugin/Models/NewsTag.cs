﻿using Newtonsoft.Json;

namespace KanobuPlugin.Models
{
    public class NewsTag
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("public")]
        public bool? Public { get; set; }
    }
}
