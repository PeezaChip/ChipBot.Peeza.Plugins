﻿using KanobuPlugin.Configuration;
using KanobuPlugin.Models;
using System.Linq;

namespace KanobuPlugin.Extensions
{
    public static class ConfigurationExtensions
    {
        public static bool ShouldSend(this Subscription sub, ExtendedNews news)
        {
            var tags = sub.Type == SubscriptionType.Tag ? news.Tags : news.Verticals;
            var id = sub.Id;
            return tags.Any(t => t.Id == id);
        }
    }
}
