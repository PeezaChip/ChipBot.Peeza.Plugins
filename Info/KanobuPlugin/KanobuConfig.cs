﻿using Discord;
using KanobuPlugin.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace KanobuPlugin
{
    public class KanobuConfig
    {
        public List<SubscriptionChannel> Channels { get; set; } = new();

        public IEnumerable<Subscription> GetUserSubscriptions(IUser user)
            => GetChannel(user)?.Subscriptions;

        public IEnumerable<Subscription> GetChannelSubscriptions(ITextChannel channel)
            => GetChannel(channel)?.Subscriptions;

        public void AddSubscription(IUser user, Subscription sub)
            => AddSubscription(GetOrCreateChannel(user), sub);

        public void AddSubscription(ITextChannel channel, Subscription sub)
            => AddSubscription(GetOrCreateChannel(channel), sub);

        public void RemoveSubscription(IUser user, Subscription sub)
            => AddSubscription(GetOrCreateChannel(user), sub, true);

        public void RemoveSubscription(ITextChannel channel, Subscription sub)
            => AddSubscription(GetOrCreateChannel(channel), sub, true);

        private static void AddSubscription(SubscriptionChannel channel, Subscription sub, bool remove = false)
        {
            var subList = channel.Subscriptions;

            var current = subList.FirstOrDefault(s => s.Id == sub.Id && s.Type == sub.Type);
            if (remove)
            {
                if (current == null) return;
                subList.Remove(current);
            }
            else
            {
                if (current != null) return;
                subList.Add(sub);
            }
        }

        private SubscriptionChannel GetOrCreateChannel(IEntity<ulong> entity)
        {
            var channel = GetChannel(entity);

            if (channel == null)
            {
                channel = new SubscriptionChannel()
                {
                    Id = entity.Id,
                    IsTextChannel = entity is ITextChannel
                };
                Channels.Add(channel);
            };

            return channel;
        }

        private SubscriptionChannel GetChannel(IEntity<ulong> entity)
            => GetSubscriptionChannel(entity.Id);

        private SubscriptionChannel GetSubscriptionChannel(ulong id)
            => Channels.FirstOrDefault(c => c.Id == id);
    }
}
