﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RSSPlugin.Core;
using System.Reflection;
using System.ServiceModel.Syndication;
using System.Xml;

namespace RSSPlugin.Services
{
    [Notify(NoEarlierThan = 0, RepeatIntervalSeconds = 60 * 5)] // 5 minutes
    [Chipject]
    public class RSSDownloader : ChipService<RSSDownloader>, INotifyService
    {
        private readonly DictionaryService dictionaryService;
        private readonly IEnumerable<IFeedConsumer> consumers;

        public RSSDownloader(ILogger<RSSDownloader> log, DictionaryService dictionaryService, IEnumerable<IFeedConsumer> consumers) : base(log)
        {
            this.dictionaryService = dictionaryService;
            this.consumers = consumers;
        }

        public async Task Notify()
        {
            await CheckFeeds();
            await Task.Delay(TimeSpan.FromMinutes(5));
        }

        private async Task CheckFeeds()
        {
            logger.LogInformation($"Checking feeds");
            foreach (var consumer in consumers)
            {
                var type = consumer.GetType();

                try
                {
                    var attribute = type.GetCustomAttribute<FeedConsumerAttribute>();
                    if (attribute == null)
                    {
                        logger.LogWarning($"Missing FeedConsumerAttribute for type '{type.FullName}'");
                        continue;
                    }

                    logger.LogInformation($"Getting feed for '{type.Name}' from {attribute.Uri}");
                    var feed = CheckFeed(attribute.Uri);

                    if (feed == null)
                    {
                        logger.LogInformation($"No new items for feed {attribute.Uri}");
                        continue;
                    }
                    else
                    {
                        logger.LogInformation($"Found {feed.Items.Count} new items for feed {attribute.Uri}");
                        await consumer.Consume(feed);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't proccess feed for '{type.FullName}'");
                }
            }
        }

        private Feed CheckFeed(string uri)
        {
            try
            {
                var syndicationFeed = DownloadFeed(uri);
                var lastKnownId = dictionaryService.Get($"{lastItemIdKey}{uri}");

                var newItems = syndicationFeed.Items.TakeWhile(item => item.Id != lastKnownId);

                if (newItems.Any())
                {
                    var feed = new Feed()
                    {
                        Info = GetFeedInfo(syndicationFeed, uri),
                        Items = newItems.ToList()
                    };
                    dictionaryService.AddOrUpdate($"{lastItemIdKey}{uri}", newItems.First().Id);
                    return feed;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.LogWarning(ex, $"Couldn't check feed from {uri}");
                throw;
            }
        }

        private SyndicationFeed DownloadFeed(string uri)
        {
            logger.LogInformation($"Downloading RSS from {uri}");
            using var reader = XmlReader.Create(uri);
            return SyndicationFeed.Load(reader);
        }

        private static FeedInfo GetFeedInfo(SyndicationFeed feed, string uri)
            => new()
            {
                Title = feed.Title?.Text ?? uri,
                Description = feed.Description?.Text,
                Image = feed.ImageUrl.ToString(),
                Uri = feed.Links.FirstOrDefault(l => l.RelationshipType == "alternate") ?? feed.Links.FirstOrDefault()
            };

        private const string lastItemIdKey = "rss_feed_";
    }
}
