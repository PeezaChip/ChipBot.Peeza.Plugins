﻿namespace WoWPlugin.Shared.Utilities
{
    public static class ClassColorUtility
    {
        private class LogColor(byte r, byte g, byte b, byte breakpoint = 0)
        {
            public byte r = r;
            public byte g = g;
            public byte b = b;
            public byte breakpoint = breakpoint;
        }

        private static readonly Dictionary<string, string> ColorMapping = new()
        {
            { "deathknight", "C41E3A" },
            { "demonhunter", "A330C9" },
            { "druid", "FF7C0A" },
            { "evoker", "33937F" },
            { "hunter", "AAD372" },
            { "mage", "3FC7EB" },
            { "monk", "00FF98" },
            { "paladin", "F48CBA" },
            { "priest", "FFFFFF" },
            { "rogue", "FFF468" },
            { "shaman", "0070DD" },
            { "warlock", "8788EE" },
            { "warrior", "C69B6D" },
        };

        private static readonly List<LogColor> LogColors =
        [
            new LogColor(128, 128, 128, 10),
            new LogColor(0, 196, 31, 30),
            new LogColor(0, 99, 207, 50),
            new LogColor(156, 0, 255, 70),
            new LogColor(255, 143, 0, 95),
            new LogColor(254, 0, 255, 100),
        ];

        private static LogColor InterpolateColor(LogColor color1, LogColor color2, double percent)
        {
            return new LogColor(
                (byte)(color1.r + percent * (color2.r - color1.r)),
                (byte)(color1.g + percent * (color2.g - color1.g)),
                (byte)(color1.b + percent * (color2.b - color1.b))
                );
        }

        private static string LogColorToString(LogColor color)
        {
            return $"rgb({color.r} {color.g} {color.b})";
        }
        
        public static string GetLogColor(double point)
        {
            var leftColor = LogColors.Where(color => color.breakpoint >= 0 && color.breakpoint < point).LastOrDefault();
            var rightColor = LogColors.Where(color => color.breakpoint >= point).FirstOrDefault();

            if (leftColor != null && rightColor == null) return LogColorToString(leftColor);
            if (leftColor == null && rightColor != null) return LogColorToString(rightColor);

            return LogColorToString(InterpolateColor(leftColor, rightColor, (point - leftColor.breakpoint) / (rightColor.breakpoint - leftColor.breakpoint)));
        }

        public static string GetColor(string className)
        {
            className ??= "";
            className = className.ToLower().Replace(" ", "");
            return ColorMapping.TryGetValue(className, out var color) ? color : "333333";
        }
    }
}
