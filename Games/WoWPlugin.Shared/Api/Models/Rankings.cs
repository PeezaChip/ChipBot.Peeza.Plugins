﻿using Newtonsoft.Json;

namespace WoWPlugin.Shared.Api.Models
{
    public partial class Rankings
    {
        [JsonProperty("encounterID")]
        public int EncounterId { get; set; }

        [JsonProperty("encounterName")]
        public string EncounterName { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("spec")]
        public string Spec { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }

        [JsonProperty("outOf")]
        public long OutOf { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("startTime")]
        public long StartTime { get; set; }

        [JsonProperty("reportID")]
        public string ReportId { get; set; }

        [JsonProperty("fightID")]
        public long FightId { get; set; }

        [JsonProperty("difficulty")]
        public long Difficulty { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        [JsonProperty("characterID")]
        public long CharacterId { get; set; }

        [JsonProperty("characterName")]
        public string CharacterName { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("percentile")]
        public double Percentile { get; set; }

        [JsonProperty("ilvlKeyOrPatch")]
        public long IlvlKeyOrPatch { get; set; }

        [JsonProperty("talents")]
        public Talent[] Talents { get; set; }

        [JsonProperty("gear")]
        public Gear[] Gear { get; set; }

        [JsonProperty("azeritePowers")]
        public object[] AzeritePowers { get; set; }

        [JsonProperty("total")]
        public double Total { get; set; }

        [JsonProperty("estimated")]
        public bool Estimated { get; set; }
    }

    public partial class Gear
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("quality")]
        public string Quality { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("itemLevel")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ItemLevel { get; set; }

        [JsonProperty("bonusIDs", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DecodeArrayConverter))]
        public long[] BonusIDs { get; set; }

        [JsonProperty("permanentEnchant", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? PermanentEnchant { get; set; }

        [JsonProperty("gems", NullValueHandling = NullValueHandling.Ignore)]
        public Gem[] Gems { get; set; }

        [JsonProperty("temporaryEnchant", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? TemporaryEnchant { get; set; }
    }

    public partial class Gem
    {
        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("itemLevel")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ItemLevel { get; set; }
    }

    public partial class Talent
    {
        [JsonProperty("entryID")]
        public long EntryId { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }
    }

    internal class DecodeArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long[]);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            reader.Read();
            var value = new List<long>();
            while (reader.TokenType != JsonToken.EndArray)
            {
                var converter = ParseStringConverter.Singleton;
                var arrayItem = (long)converter.ReadJson(reader, typeof(long), null, serializer);
                value.Add(arrayItem);
                reader.Read();
            }
            return value.ToArray();
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (long[])untypedValue;
            writer.WriteStartArray();
            foreach (var arrayItem in value)
            {
                var converter = ParseStringConverter.Singleton;
                converter.WriteJson(writer, arrayItem, serializer);
            }
            writer.WriteEndArray();
            return;
        }

        public static readonly DecodeArrayConverter Singleton = new DecodeArrayConverter();
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            return long.TryParse(value, out var l) ? (object)l : throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new();
    }
}
