﻿using Newtonsoft.Json;

namespace WoWPlugin.Shared.Api.Models
{
    public partial class Zones
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("frozen")]
        public bool Frozen { get; set; }

        [JsonProperty("encounters")]
        public Encounter[] Encounters { get; set; }

        [JsonProperty("brackets")]
        public Brackets Brackets { get; set; }

        [JsonProperty("partitions", NullValueHandling = NullValueHandling.Ignore)]
        public Partition[] Partitions { get; set; }
    }

    public partial class Brackets
    {
        [JsonProperty("min")]
        public long Min { get; set; }

        [JsonProperty("max")]
        public double Max { get; set; }

        [JsonProperty("bucket")]
        public double Bucket { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(TypeEnumConverter))]
        [System.Text.Json.Serialization.JsonConverter(typeof(System.Text.Json.Serialization.JsonStringEnumConverter))]
        public BracketTypeEnum Type { get; set; }

        [JsonProperty("sub_bucket", NullValueHandling = NullValueHandling.Ignore)]
        public long? SubBucket { get; set; }
    }

    public partial class Encounter
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Partition
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("compact")]
        public string Compact { get; set; }

        [JsonProperty("default", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Default { get; set; }
    }

    public enum BracketTypeEnum { ItemLevel, KeystoneLevel, Patch };

    internal class TypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(BracketTypeEnum) || t == typeof(BracketTypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Item Level":
                case "ItemLevel":
                    return BracketTypeEnum.ItemLevel;
                case "Keystone Level":
                case "KeystoneLevel":
                    return BracketTypeEnum.KeystoneLevel;
                case "Patch":
                    return BracketTypeEnum.Patch;
                default:
                    break;
            }
            throw new Exception("Cannot unmarshal type TypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (BracketTypeEnum)untypedValue;
            switch (value)
            {
                case BracketTypeEnum.ItemLevel:
                    serializer.Serialize(writer, "ItemLevel");
                    return;
                case BracketTypeEnum.KeystoneLevel:
                    serializer.Serialize(writer, "KeystoneLevel");
                    return;
                case BracketTypeEnum.Patch:
                    serializer.Serialize(writer, "Patch");
                    return;
            }
            throw new Exception("Cannot marshal type TypeEnum");
        }

        public static readonly TypeEnumConverter Singleton = new();
    }
}
