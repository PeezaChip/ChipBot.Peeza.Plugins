﻿using Newtonsoft.Json;

namespace WoWPlugin.Shared.Api.Models
{
    public class Classes
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("specs")]
        public Spec[] Specs { get; set; }
    }

    public class Spec
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
