﻿using ChipBot.Core.Web.Abstract;

namespace HoyolabPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "HoyolabWebPlugin";
    }
}
