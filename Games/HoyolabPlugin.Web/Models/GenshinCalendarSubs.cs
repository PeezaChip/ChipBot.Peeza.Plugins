﻿namespace HoyolabPlugin.Web.Models
{
    public class GenshinCalendarSubs
    {
        public List<string> Characters { get; set; } = [];
        public List<string> Weapons { get; set; } = [];

        public const string localStorageKey = "hoyo-gi-calendar";
    }
}
