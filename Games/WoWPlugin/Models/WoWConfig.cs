﻿namespace WoWPlugin.Models
{
    public class WoWConfig
    {
        public string ApiKey { get; set; }
        public int DefaultZoneId { get; set; }
    }
}
