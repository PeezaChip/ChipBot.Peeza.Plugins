﻿using RestEase;
using WoWPlugin.Shared.Api.Models;

namespace WoWPlugin.Api
{
    public interface IWarcraftLogsApi
    {
        public const string BaseAddress = "https://www.warcraftlogs.com/v1/";

        [Query("api_key")]
        string Key { get; set; }

        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("classes")]
        Task<IEnumerable<Classes>> GetClasses();

        [Get("zones")]
        Task<IEnumerable<Zones>> GetZones();

        [Get("rankings/character/{character}/{server}/{region}")]
        Task<IEnumerable<Rankings>> GetRankings([Path] string character, [Path] string server, [Path] string region, int zone, string metric = "dps");
    }
}
