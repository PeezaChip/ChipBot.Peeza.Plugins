﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestEase;
using System.Collections.Concurrent;
using WoWPlugin.Api;
using WoWPlugin.Models;
using WoWPlugin.Shared.Api.Models;

namespace WoWPlugin.Services
{
    [Chipject]
    [Notify(NoEarlierThan = 1, RepeatIntervalSeconds = 60 * 60 * 24)] // 1 day
    public class WarcraftLogsApiService : ApiService<IWarcraftLogsApi>, INotifyService
    {
        private readonly SettingsService<WoWConfig> config;
        private readonly ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory;
        private readonly IMemoryCache memoryCache;

        private List<Zones> zones;
        private List<Classes> classes;

        public WarcraftLogsApiService(ILogger<WarcraftLogsApiService> log, SettingsService<WoWConfig> config, SettingsService<NetworkConfig> networkConfig, ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory, IMemoryCache memoryCache) : base(log, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(IWarcraftLogsApi.BaseAddress)
                }).For<IWarcraftLogsApi>();

            ApiClient.UserAgent = UserAgent;
            ApiClient.Key = config.Settings.ApiKey;

            this.config = config;
            this.chipBotDbContextFactory = chipBotDbContextFactory;
            this.memoryCache = memoryCache;

            LoadData();
        }

        public async Task<Dictionary<int, Rankings>> GetCharacterRankings(string name, string server, string region, int? zone = null)
        {
            return await memoryCache.GetOrCreateAsync($"wow_logs_rankings_{name}_{server}_{region}_{zone}", async (cacheEntry) =>
            {
                cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(2);
                zone ??= config.Settings.DefaultZoneId;
                var rankings = await ApiClient.GetRankings(name, server, region, zone.Value);
                return rankings.GroupBy(g => g.EncounterId).ToDictionary(g => g.Key, g => g.OrderByDescending(r => r.Difficulty).First());
            });
        }

        public async Task<Dictionary<string, Dictionary<int, Rankings>>> GetCharacterRankings(IEnumerable<string> characters, string region, int? zone = null)
        {
            var result = new ConcurrentDictionary<string, Dictionary<int, Rankings>>();
            await Parallel.ForEachAsync(characters, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, async (source, ct) =>
            {
                try
                {
                    var split = source.Split('-');
                    var rankings = await GetCharacterRankings(split[0], split[1], region, zone);
                    result.TryAdd(source, rankings);
                }
                catch (Exception ex)
                {
                    logger.LogWarning(ex, $"Couldn't get ranking info for {source} {region} {zone}");
                }
            });

            return result.ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        public async Task<Dictionary<int, Zones>> GetZones()
        {
            await LoadZones();
            return zones.ToDictionary(z => z.Id, z => z);
        }

        public async Task<IEnumerable<Classes>> GetClasses()
        {
            await LoadClasses();
            return classes;
        }

        private bool isDownloading = false;
        public async Task Notify()
        {
            if (isDownloading) return;
            isDownloading = true;

            try
            {
                logger.LogInformation("Downloading wow data");
                var startTime = DateTime.Now;

                var newZones = await ApiClient.GetZones();
                var newClasses = await ApiClient.GetClasses();

                logger.LogInformation($"Download done in {DateTime.Now - startTime}");
                zones = newZones.ToList();
                classes = newClasses.ToList();
                SaveData();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't download wow data");
            }
            finally
            {
                isDownloading = false;
            }
        }

        private void SaveData()
        {
            try
            {
                using var dbContext = chipBotDbContextFactory.CreateDbContext();
                var zonesRow = dbContext.Dictionary.Find(db_dictionary_zones_key);
                var classesRow = dbContext.Dictionary.Find(db_dictionary_classes_key);

                var zonesValue = JsonConvert.SerializeObject(zones);
                var classesValue = JsonConvert.SerializeObject(classes);

                if (zonesRow != null)
                {
                    zonesRow.Value = zonesValue;
                    dbContext.Dictionary.Update(zonesRow);
                }
                else
                {
                    dbContext.Dictionary.Add(new ChipKeyValue() { Key = db_dictionary_zones_key, Value = zonesValue });
                }

                if (classesRow != null)
                {
                    classesRow.Value = classesValue;
                    dbContext.Dictionary.Update(classesRow);
                }
                else
                {
                    dbContext.Dictionary.Add(new ChipKeyValue() { Key = db_dictionary_classes_key, Value = classesValue });
                }

                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't save wow data");
            }
        }

        private async void LoadData()
        {
            await LoadZones();
            await LoadClasses();
        }

        private async Task LoadZones()
        {
            try
            {
                if (zones != null) return;

                using var dbContext = chipBotDbContextFactory.CreateDbContext();
                var row = dbContext.Dictionary.Find(db_dictionary_zones_key);

                if (row == null)
                {
                    await Notify();
                }
                else
                {
                    zones = JsonConvert.DeserializeObject<IEnumerable<Zones>>(row.Value).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't load zones data");
            }
        }

        private async Task LoadClasses()
        {
            try
            {
                if (classes != null) return;

                using var dbContext = chipBotDbContextFactory.CreateDbContext();
                var row = dbContext.Dictionary.Find(db_dictionary_classes_key);

                if (row == null)
                {
                    await Notify();
                }
                else
                {
                    classes = JsonConvert.DeserializeObject<IEnumerable<Classes>>(row.Value).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't load classes data");
            }
        }

        private readonly string db_dictionary_zones_key = "wow_logs_zones";
        private readonly string db_dictionary_classes_key = "wow_logs_classes";
    }
}
