﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WoWPlugin.Services;
using WoWPlugin.Shared.Api.Models;

namespace WoWPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "warcraft", Version = "v1", Title = "ChipBot WarCraft V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class WarcraftController : ControllerBase
    {
        private readonly WarcraftLogsApiService logsService;

        public WarcraftController(WarcraftLogsApiService logsService)
        {
            this.logsService = logsService;
        }

        [HttpGet("rankings")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<int, Rankings>))]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesUserCache)]
        public async Task<IActionResult> GetRankings(string name, string server, string region, int? zone = null) => Ok(await logsService.GetCharacterRankings(name, server, region, zone));

        [HttpGet("rankingsbulk")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<string, Dictionary<int, Rankings>>))]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesUserCache)]   
        public async Task<IActionResult> GetRankingsBulk(string characters, string region, int? zone = null) => Ok(await logsService.GetCharacterRankings(characters.Split(';'), region, zone));

        [HttpGet("zones")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<int, Zones>))]
        [ResponseCache(CacheProfileName = CachingConstants.HourUserCache)]
        public async Task<IActionResult> GetZones() => Ok(await logsService.GetZones());

        [HttpGet("classes")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Classes>))]
        [ResponseCache(CacheProfileName = CachingConstants.HourUserCache)]
        public async Task<IActionResult> GetClasses() => Ok(await logsService.GetClasses());
    }
}
