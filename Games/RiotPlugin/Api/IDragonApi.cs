﻿using RestEase;
using RiotPlugin.Api.ApiModels;
using System.Threading.Tasks;

namespace RiotPlugin.Api
{
    public interface IDragonApi
    {
        public const string BaseUrl = "https://ddragon.leagueoflegends.com";

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Get("realms/{platform}.json")]
        Task<RealmInfo> GetRealmInfo([Path] string platform);

        [Get("cdn/{version}/data/{lang}/champion.json")]
        Task<ChampionsData> GetChampionsInfo([Path] string version, [Path] string lang);
    }
}
