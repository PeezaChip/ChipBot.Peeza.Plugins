﻿using RestEase;
using RiotPlugin.Api.ApiModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RiotPlugin.Api
{
    public interface IStaticRiotApi
    {
        public const string BaseUrl = "https://static.developer.riotgames.com";

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Get("docs/lol/queues.json")]
        Task<IEnumerable<QueueInfo>> GetQueueInfo();

        [Get("docs/lol/maps.json")]
        Task<IEnumerable<MapInfo>> GetMapInfo();
    }
}
