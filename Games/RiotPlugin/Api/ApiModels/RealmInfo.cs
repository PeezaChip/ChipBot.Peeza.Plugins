﻿using Newtonsoft.Json;
using System;

namespace RiotPlugin.Api.ApiModels
{
    public class RealmInfo
    {
        [JsonProperty("n")]
        public RealmVersionInfo RealmVersion { get; set; }

        [JsonProperty("v")]
        public string Version { get; set; }

        [JsonProperty("l")]
        public string Lang { get; set; }

        [JsonProperty("cdn")]
        public string Cdn { get; set; }

        [JsonProperty("dd")]
        public string Dd { get; set; }

        [JsonProperty("lg")]
        public string Lg { get; set; }

        [JsonProperty("css")]
        public string Css { get; set; }

        [JsonProperty("profileiconmax")]
        public long Profileiconmax { get; set; }
    }

    public class RealmVersionInfo
    {
        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("rune")]
        public string Rune { get; set; }

        [JsonProperty("mastery")]
        public string Mastery { get; set; }

        [JsonProperty("summoner")]
        public string Summoner { get; set; }

        [JsonProperty("champion")]
        public string Champion { get; set; }

        [JsonProperty("profileicon")]
        public string Profileicon { get; set; }

        [JsonProperty("map")]
        public string Map { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("sticker")]
        public string Sticker { get; set; }
    }
}
