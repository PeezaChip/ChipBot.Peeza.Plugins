﻿using Newtonsoft.Json;

namespace RiotPlugin.Api.ApiModels
{

    public class QueueInfo
    {
        [JsonProperty("queueId")]
        public int QueueId { get; set; }

        [JsonProperty("map")]
        public string Map { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }
    }
}
