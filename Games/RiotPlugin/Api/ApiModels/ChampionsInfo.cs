﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RiotPlugin.Api.ApiModels
{
    public class ChampionsData
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("data")]
        public Dictionary<string, ChampionData> Champions { get; set; }

        [JsonIgnore]
        private Dictionary<string, ChampionData> ChampionsIgnoreCase { get; set; }
        [JsonIgnore]
        private Dictionary<int, ChampionData> ChampionsKeys { get; set; }

        public ChampionData GetChampionById(string id)
        {
            ChampionsIgnoreCase ??= new Dictionary<string, ChampionData>(Champions, StringComparer.InvariantCultureIgnoreCase);

            ChampionsIgnoreCase.TryGetValue(id, out var champion);
            return champion;
        }

        public ChampionData GetChampionByKey(int key)
        {
            ChampionsKeys = Champions.Values.ToDictionary(champ => int.Parse(champ.Key), champ => champ);

            ChampionsKeys.TryGetValue(key, out var champion);
            return champion;
        }
    }

    public class ChampionData
    {
        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("blurb")]
        public string Blurb { get; set; }

        [JsonProperty("info")]
        public ChampionInfo Info { get; set; }

        [JsonProperty("image")]
        public ChampionImage Image { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("partype")]
        public string Partype { get; set; }

        [JsonProperty("stats")]
        public Dictionary<string, double> Stats { get; set; }
    }

    public class ChampionImage
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        [JsonProperty("sprite")]
        public string Sprite { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("w")]
        public long W { get; set; }

        [JsonProperty("h")]
        public long H { get; set; }
    }

    public class ChampionInfo
    {
        [JsonProperty("attack")]
        public int Attack { get; set; }

        [JsonProperty("defense")]
        public int Defense { get; set; }

        [JsonProperty("magic")]
        public int Magic { get; set; }

        [JsonProperty("difficulty")]
        public int Difficulty { get; set; }
    }
}
