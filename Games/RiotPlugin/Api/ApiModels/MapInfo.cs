﻿using Newtonsoft.Json;

namespace RiotPlugin.Api.ApiModels
{
    public class MapInfo
    {
        [JsonProperty("mapId")]
        public int MapId { get; set; }

        [JsonProperty("mapName")]
        public string MapName { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }
    }
}
