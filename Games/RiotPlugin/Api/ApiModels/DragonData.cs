﻿using System;

namespace RiotPlugin.Api.ApiModels
{
    public class DragonData<T> where T : class
    {
        private readonly TimeSpan cacheTime;
        private DateTime lastUpdated;
        public T Data { get; private set; }

        public bool ShouldUpdate => Data == null || DateTime.Now - lastUpdated > cacheTime;

        public DragonData(T data, TimeSpan? cacheTime = null)
        {
            Data = data;
            lastUpdated = DateTime.Now;
            this.cacheTime = cacheTime ?? TimeSpan.FromDays(1);
        }

        public void Update(T value)
        {
            Data = value;
            lastUpdated = DateTime.Now;
        }
    }
}
