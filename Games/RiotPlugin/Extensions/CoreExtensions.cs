﻿using System.Text;

namespace RiotPlugin.Extensions
{
    public static class CoreExtensions
    {
        public static string Spaceify(this string camelCase)
        {
            var sb = new StringBuilder();
            var lowerCase = true;
            foreach (var c in camelCase)
            {
                if (lowerCase && char.IsUpper(c))
                {
                    sb.Append(' ');
                    lowerCase = false;
                }
                else if (char.IsLower(c))
                {
                    lowerCase = true;
                }
                sb.Append(c);
            }
            return sb.ToString().Trim();
        }
    }
}
