﻿using Camille.Enums;
using Camille.RiotGames.ChampionMasteryV4;
using Camille.RiotGames.LeagueV4;
using Camille.RiotGames.SummonerV4;
using Discord;
using Humanizer;
using RiotPlugin.Data.Models;
using RiotPlugin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiotPlugin.Extensions
{
    public static class EmbedExtensions
    {
        public static async Task<Embed> GetEmbed(this RiotAccount account, DragonApiService dragonApi, string additionalInfo = null)
        {
            return (await AddSummonerInfo(GetEmbedBase(), account.Name, account.ProfileIconId, account.SummonerLevel, dragonApi, additionalInfo))
                .Build();
        }

        public static async Task<EmbedBuilder> GetEmbed(this Summoner summoner, DragonApiService dragonApi)
            => await AddSummonerInfo(GetEmbedBase(), summoner.Name, summoner.ProfileIconId, summoner.SummonerLevel, dragonApi);

        public static EmbedBuilder AddLeagueInfoToEmbed(this IEnumerable<LeagueEntry> leagueEntries, EmbedBuilder embed)
        {
            foreach(var leagueEntry in leagueEntries)
            {
                string title;
                switch(leagueEntry.QueueType)
                {
                    case QueueType.RANKED_SOLO_5x5:
                        title = "SoloQ";
                        break;
                    case QueueType.RANKED_FLEX_SR:
                        title = "Flex";
                        break;
                    default:
                        continue;
                }

                var wr = ((double)leagueEntry.Wins) / (leagueEntry.Wins + leagueEntry.Losses);
                embed.AddField(title, $"{leagueEntry.Tier} {leagueEntry.Rank} `{leagueEntry.LeaguePoints}`LP\n`{leagueEntry.Wins}`W `{leagueEntry.Losses}`L `{wr:P0}`{(leagueEntry.HotStreak ? "\nWin Streak" : "")}", true);
            }
            return embed;
        }

        public static async Task<EmbedBuilder> AddMasteryInfoToEmbed(this IEnumerable<ChampionMastery> masteries, EmbedBuilder embed, DragonApiService dragonApi)
        {
            if (!masteries.Any()) return embed;

            var sb = new StringBuilder();
            foreach (var mastery in masteries)
            {
                var champion = await dragonApi.GetChampionData(mastery.ChampionId.ToString());
                sb.AppendLine($"{champion.Name} `{mastery.ChampionPoints:N0}`, last played {DateTimeOffset.FromUnixTimeMilliseconds(mastery.LastPlayTime).Humanize()}");
            }
            return embed.AddField("Mastery", sb.ToString());
        }

        private static async Task<EmbedBuilder> AddSummonerInfo(EmbedBuilder embed, string name, int profileIconId, long level, DragonApiService dragonApi, string additionalInfo = null)
        {
            var sb = new StringBuilder();
            if (additionalInfo != null)
            {
                sb.AppendLine(additionalInfo);
            }
            sb.AppendLine($"Summoner Level: `{level}`");

            return embed
                .WithTitle(name)
                .WithThumbnailUrl(await dragonApi.GetProfileIconUrl(profileIconId))
                .WithDescription(sb.ToString());
        }

        private static EmbedBuilder GetEmbedBase()
        {
            return new EmbedBuilder()
                .WithColor(152155).WithCurrentTimestamp();
        }
    }
}
