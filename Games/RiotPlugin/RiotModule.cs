﻿using Camille.Enums;
using Camille.RiotGames;
using Camille.RiotGames.MatchV5;
using Camille.RiotGames.SummonerV4;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using Microsoft.Extensions.DependencyInjection;
using RiotPlugin.Data.Models;
using RiotPlugin.Extensions;
using RiotPlugin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RiotPlugin
{
    [Group("lol", "LoL related commands")]
    public class RiotModule : ChipInteractionModuleBase
    {
        public RiotAccountService AccountService { get; set; }
        public RiotApiService RiotApi { get; set; }
        public DragonApiService DragonApi { get; set; }

        [SlashCommand("add-account", "Adds an account")]
        public async Task AddAccount(string summonerName, PlatformRoute platform)
        {
            var info = await RiotApi.ApiClient.SummonerV4().GetBySummonerNameAsync(platform, summonerName);

            var account = new RiotAccount()
            {
                UserId = Context.User.Id,
                SummonerId = info.Id,
                AccountId = info.AccountId,
                PuuId = info.Puuid,
                Name = summonerName,
                ProfileIconId = info.ProfileIconId,
                RevisionDate = info.RevisionDate,
                SummonerLevel = info.SummonerLevel,
                Platform = platform,
                Region = platform switch
                {
                    PlatformRoute.RU => RegionalRoute.EUROPE,
                    PlatformRoute.EUW1 => RegionalRoute.EUROPE,
                    _ => throw new Exception("Oops. Something happend.")
                }
            };
            AccountService.AddAccount(account);

            await ReplyEmbedAsync(await account.GetEmbed(DragonApi, "Account added."));
        }

        [SlashCommand("remove-account", "Removes an account")]
        public async Task RemoveAccount([Autocomplete(typeof(AccountNameAutoCompleter))] string id)
        {
            AccountService.RemoveAccount(Context.User, id);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("display-account", "Displays an account")]
        public async Task DisplayAccount([Autocomplete(typeof(AccountNameAutoCompleter))] string id)
        {
            var account = AccountService.GetAccount(Context.User, id);
            if (account == null)
            {
                await ReplyFailAsync(true);
                return;
            }

            await DisplaySummonerEmbed(account.Platform, await RiotApi.ApiClient.SummonerV4().GetBySummonerIdAsync(account.Platform, account.SummonerId));
        }

        [SlashCommand("find-account", "Finds an account and displays info about it")]
        public async Task FindAccount(string summonerName, PlatformRoute platform)
        {
            var info = await RiotApi.ApiClient.SummonerV4().GetBySummonerNameAsync(platform, summonerName);
            await DisplaySummonerEmbed(platform, info);
        }

        [SlashCommand("setting", "Changes account settings")]
        public async Task ChangeAccount([Autocomplete(typeof(AccountNameAutoCompleter))] string id, [Autocomplete(typeof(AccountSettingAutoCompleter))][Summary("setting", "Setting to change")] string propname, [Summary("enable")] IsOn ison)
        {
            var account = AccountService.GetAccount(Context.User, id);
            if (account == null)
            {
                await ReplyFailAsync(true);
                return;
            }

            var prop = account.GetType().GetProperty(propname);
            prop.SetValue(account, ison == IsOn.enable);

            AccountService.AddAccount(account);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("setting-match-data", "Changes account post match info settings")]
        public async Task ChangeAccountPostMatchInfo([Autocomplete(typeof(AccountNameAutoCompleter))] string id, [Autocomplete(typeof(AccountPostMatchInfoSettingAutoCompleter))][Summary("setting", "Setting to change")] string propname, [Summary("enable")] IsOn ison)
        {
            var account = AccountService.GetAccount(Context.User, id);
            if (account == null)
            {
                await ReplyFailAsync(true);
                return;
            }

            var shouldSave = ison == IsOn.enable ? account.AddPostMatchInfo(propname) : account.RemovePostMatchInfo(propname);
            if (shouldSave)
            {
                AccountService.AddAccount(account);
            }
            await ReplySuccessAsync(true);
        }

        private async Task DisplaySummonerEmbed(PlatformRoute platform, Summoner summoner)
        {
            await DeferAsync();

            var embed = await summoner.GetEmbed(DragonApi);

            var leagueInfo = await RiotApi.ApiClient.LeagueV4().GetLeagueEntriesForSummonerAsync(platform, summoner.Id);
            leagueInfo.AddLeagueInfoToEmbed(embed);

            var masteries = await RiotApi.ApiClient.ChampionMasteryV4().GetTopChampionMasteriesAsync(platform, summoner.Id);
            await masteries.AddMasteryInfoToEmbed(embed, DragonApi);

            await ModifyOriginalResponseAsync(m => m.Embed = embed.Build());
        }

        private class AccountNameAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var accountService = services.GetRequiredService<RiotAccountService>();

                var accounts = accountService.GetAccounts(context.User);

                return Task.FromResult(AutocompletionResult.FromSuccess(accounts.Take(25).Select(a => new AutocompleteResult($"{a.Name} - {a.SummonerLevel}lvl - {a.Platform}", a.SummonerId))));
            }
        }

        private class AccountSettingAutoCompleter : AutocompleteHandler
        {
            private static readonly IEnumerable<PropertyInfo> props = typeof(RiotAccountSettings).GetProperties().Where(p => p.PropertyType == typeof(bool));

            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
                => Task.FromResult(AutocompletionResult.FromSuccess(props.Take(25).Select(a => new AutocompleteResult($"{a.Name.Spaceify()}", a.Name))));
        }

        private class AccountPostMatchInfoSettingAutoCompleter : AutocompleteHandler
        {
            private static readonly IEnumerable<PropertyInfo> props = typeof(Participant).GetProperties();

            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var result = props;
                var text = autocompleteInteraction.Data.Current.Value.ToString().Trim().ToLower();
                if (!string.IsNullOrWhiteSpace(text))
                {
                    result = result.Where(p => p.Name.Contains(text, StringComparison.InvariantCultureIgnoreCase));
                }

                return Task.FromResult(AutocompletionResult.FromSuccess(result.Take(25).Select(a => new AutocompleteResult($"{a.Name.Spaceify()}", a.Name))));
            }
        }

        public enum IsOn
        {
            enable,
            disable
        }
    }
}
