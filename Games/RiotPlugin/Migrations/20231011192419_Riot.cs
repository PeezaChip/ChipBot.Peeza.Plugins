﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RiotPlugin.Migrations
{
    /// <inheritdoc />
    public partial class Riot : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RiotAccounts",
                columns: table => new
                {
                    SummonerId = table.Column<string>(type: "text", nullable: false),
                    AccountId = table.Column<string>(type: "text", nullable: false),
                    PuuId = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    ProfileIconId = table.Column<int>(type: "integer", nullable: false),
                    RevisionDate = table.Column<long>(type: "bigint", nullable: false),
                    SummonerLevel = table.Column<long>(type: "bigint", nullable: false),
                    Platform = table.Column<byte>(type: "smallint", nullable: false),
                    Region = table.Column<byte>(type: "smallint", nullable: false),
                    UserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    LastMatchId = table.Column<string>(type: "text", nullable: true),
                    WatchMatches = table.Column<bool>(type: "boolean", nullable: false),
                    AutoRefreshInfo = table.Column<bool>(type: "boolean", nullable: false),
                    SendPostMatchInfo = table.Column<bool>(type: "boolean", nullable: false),
                    ShowVersusInfo = table.Column<bool>(type: "boolean", nullable: false),
                    AdditionalProps = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiotAccounts", x => x.SummonerId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RiotAccounts_AccountId_PuuId_Name",
                table: "RiotAccounts",
                columns: new[] { "AccountId", "PuuId", "Name" },
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RiotAccounts");
        }
    }
}
