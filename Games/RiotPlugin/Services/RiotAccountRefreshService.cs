﻿using Camille.RiotGames;
using Camille.RiotGames.SummonerV4;
using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using Microsoft.Extensions.Logging;
using RiotPlugin.Data.Models;
using System;
using System.Threading.Tasks;

namespace RiotPlugin.Services
{
    [Notify(NoEarlierThan = 1)]
    [Chipject(ChipjectType.Transient)]
    public class RiotAccountRefreshService : ChipService<RiotAccountRefreshService>, INotifyService
    {
        private readonly RiotAccountService accountService;
        private readonly RiotApiService riotApi;

        public RiotAccountRefreshService(ILogger<RiotAccountRefreshService> log, RiotAccountService accountService, RiotApiService riotApi) : base(log)
        {
            this.accountService = accountService;
            this.riotApi = riotApi;
        }

        public async Task Notify()
        {
            logger.LogInformation("Starting to refresh accounts info");
            var accounts = accountService.GetAllAccounts();

            foreach (var account in accounts)
            {
                if (!account.AutoRefreshInfo) continue;
                var result = await GetNewAccountInfo(account);
                try
                {
                    if (result.RevisionDate == account.RevisionDate) continue;

                    logger.LogInformation($"Saving account ({account.SummonerId}) info");
                    account.Merge(result);
                    accountService.AddAccount(account);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't save account ({account.SummonerId}) info");
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private Task<Summoner> GetNewAccountInfo(RiotAccount account)
            => riotApi.ApiClient.SummonerV4().GetBySummonerIdAsync(account.Platform, account.SummonerId);
    }
}
