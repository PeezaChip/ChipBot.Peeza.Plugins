﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RestEase;
using RiotPlugin.Api;
using RiotPlugin.Api.ApiModels;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RiotPlugin.Services
{
    [Chipject]
    public class DragonApiService : ApiService<IDragonApi>
    {
        private readonly DragonData<RealmInfo> realmInfo = new(null);
        private readonly DragonData<ChampionsData> championsData = new(null);

        public DragonApiService(ILogger<DragonApiService> log, SettingsService<NetworkConfig> networkConfig) : base(log, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(IDragonApi.BaseUrl)
                }).For<IDragonApi>();

            ApiClient.UserAgent = UserAgent;
        }

        public async Task<RealmInfo> GetRealmInfo()
        {
            if (realmInfo.ShouldUpdate)
            {
                realmInfo.Update(await ApiClient.GetRealmInfo("ru"));
            }
            return realmInfo.Data;
        }

        public async Task<ChampionsData> GetChampionsData()
        {
            var info = await GetRealmInfo();
            if (championsData.ShouldUpdate)
            {
                championsData.Update(await ApiClient.GetChampionsInfo(info.RealmVersion.Champion, "en_US"));
            }
            return championsData.Data;
        }

        public async Task<ChampionData> GetChampionData(string id)
        {
            var data = await GetChampionsData();
            return data.GetChampionById(id);
        }

        public async Task<ChampionData> GetChampionData(int id)
        {
            var data = await GetChampionsData();
            return data.GetChampionByKey(id);
        }

        public async Task<string> GetProfileIconUrl(int id)
        {
            var info = await GetRealmInfo();
            return $"{info.Cdn}/{info.RealmVersion.Profileicon}/img/profileicon/{id}.png";
        }

        public async Task<string> GetChampionSquareImage(int id)
        {
            var info = await GetRealmInfo();
            var champ = await GetChampionData(id);
            return $"{info.Cdn}/{info.RealmVersion.Champion}/img/champion/{champ.Image.Full}";
        }

        public async Task<string> GetMapImage(int id)
        {
            var info = await GetRealmInfo();
            return $"{info.Cdn}/{info.RealmVersion.Map}/img/map/map{id}.png";
        }
    }
}
