﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RestEase;
using RiotPlugin.Api;
using RiotPlugin.Api.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RiotPlugin.Services
{
    [Chipject]
    public class StaticRiotApiService : ApiService<IStaticRiotApi>
    {
        private readonly DragonData<Dictionary<int, QueueInfo>> queueInfo = new(null);
        private readonly DragonData<Dictionary<int, MapInfo>> mapInfo = new(null);

        public StaticRiotApiService(ILogger<StaticRiotApiService> log, SettingsService<NetworkConfig> networkConfig) : base(log, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(IStaticRiotApi.BaseUrl)
                }).For<IStaticRiotApi>();

            ApiClient.UserAgent = UserAgent;
        }

        public async Task<QueueInfo> GetQueueInfo(int id)
        {
            var data = await GetQueueInfo();
            data.TryGetValue(id, out var queueInfo);
            return queueInfo;
        }

        public async Task<MapInfo> GetMapInfo(int id)
        {
            var data = await GetMapInfo();
            data.TryGetValue(id, out var mapInfo);
            return mapInfo;
        }

        private async Task<Dictionary<int, QueueInfo>> GetQueueInfo()
        {
            if (queueInfo.ShouldUpdate)
            {
                var data = await ApiClient.GetQueueInfo();
                queueInfo.Update(data.ToDictionary(q => q.QueueId, q => q));
            }
            return queueInfo.Data;
        }

        private async Task<Dictionary<int, MapInfo>> GetMapInfo()
        {
            if (mapInfo.ShouldUpdate)
            {
                var data = await ApiClient.GetMapInfo();
                mapInfo.Update(data.ToDictionary(m => m.MapId, m => m));
            }
            return mapInfo.Data;
        }
    }
}
