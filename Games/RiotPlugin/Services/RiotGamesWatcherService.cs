﻿using Camille.RiotGames;
using Camille.RiotGames.MatchV5;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using Discord;
using Discord.WebSocket;
using Humanizer;
using Microsoft.Extensions.Logging;
using RiotPlugin.Data;
using RiotPlugin.Data.Models;
using RiotPlugin.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RiotPlugin.Services
{
    [Chipject]
    public class RiotGamesWatcherService : BackgroundService<RiotGamesWatcherService>
    {
        private readonly RiotApiService riotApi;
        private readonly DragonApiService dragonApi;
        private readonly StaticRiotApiService staticApi;
        private readonly RiotAccountService accountService;
        private readonly DiscordSocketClient discord;
        private readonly ChipBotDbContextFactory<RiotDbContext> dbContextFactory;

        public override bool IsWorking => Task != null;

        private CancellationTokenSource cts = null;
        private Task Task = null;

        public RiotGamesWatcherService(ILogger<RiotGamesWatcherService> log, RiotApiService riotApi, DragonApiService dragonApi, StaticRiotApiService staticApi, RiotAccountService accountService, DiscordSocketClient discord, ChipBotDbContextFactory<RiotDbContext> dbContextFactory) : base(log)
        {
            this.riotApi = riotApi;
            this.dragonApi = dragonApi;
            this.staticApi = staticApi;
            this.accountService = accountService;
            this.discord = discord;
            this.dbContextFactory = dbContextFactory;
        }

        public override Task StartAsync()
        {
            if (IsWorking) return Task.CompletedTask;
            cts = new CancellationTokenSource();
            Task = MainLoop(cts.Token);
            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            if (!IsWorking) return Task.CompletedTask;
            cts?.Cancel();
            Task = null;
            cts = null;
            return Task.CompletedTask;
        }

        public async Task MainLoop(CancellationToken token)
        {
            while (true)
            {
                try
                {
                    logger.LogInformation("Checking new games");
                    var accounts = accountService.GetAllAccounts();

                    logger.LogTrace("Getting latest matches");
                    var gameIds = await GetLatestMatches(accounts);
                    logger.LogTrace("Filtering and saving latest matches");
                    var newGames = FilterAndSaveKnownMatches(gameIds);

                    foreach (var kv in newGames)
                    {
                        logger.LogInformation($"Getting match {kv.Key}");
                        var match = riotApi.ApiClient.MatchV5().GetMatch(accounts.First(a => a.SummonerId == kv.Value.First()).Region, kv.Key);
                        foreach (var summonerId in kv.Value)
                        {
                            var user = accounts.First(a => a.SummonerId == summonerId);
                            if (!user.SendPostMatchInfo) continue;
                            logger.LogInformation($"Sending user ({user.UserId}) {user.Name} match result");
                            await BuildAndSendEmbed(user, match);
                            await Task.Delay(TimeSpan.FromSeconds(1), token);
                        }
                    }

                    if (token.IsCancellationRequested) throw new TaskCanceledException("Received cancellation request");
                    await Task.Delay(TimeSpan.FromMinutes(1), token);
                }
                catch (TaskCanceledException)
                {
                    logger.LogInformation("Stopping main loop");
                    break;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Error while watching games");
                }
            }
        }

        private async Task<Dictionary<string, string>> GetLatestMatches(IEnumerable<RiotAccount> accounts)
        {
            var gameIds = new Dictionary<string, string>();
            foreach (var account in accounts)
            {
                if (!account.WatchMatches) continue;
                var lastMatches = await riotApi.ApiClient.MatchV5().GetMatchIdsByPUUIDAsync(account.Region, account.PuuId, 1);
                if (lastMatches == null || !lastMatches.Any()) continue;

                gameIds.Add(account.SummonerId, lastMatches.First());
            }
            return gameIds;
        }

        private Dictionary<string, IEnumerable<string>> FilterAndSaveKnownMatches(Dictionary<string, string> gameIds)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var newGameIds = new Dictionary<string, string>();
            foreach (var kv in gameIds)
            {
                var account = dbContext.RiotAccounts.Find(kv.Key);
                if (account?.LastMatchId != kv.Value)
                {
                    newGameIds.Add(kv.Key, kv.Value);
                    account.LastMatchId = kv.Value;
                }
            }

            dbContext.SaveChanges();

            return newGameIds.GroupBy(kv => kv.Value).ToDictionary(group => group.Key, group => group.Select(kv => kv.Key));
        }

        private async Task BuildAndSendEmbed(RiotAccount account, Match match)
        {
            var embeds = await BuildEmbeds(account, match);
            var ch = await discord.GetUser(account.UserId).CreateDMChannelAsync();
            await ch.SendMessageAsync(embeds: embeds.ToArray());
        }

        private async Task<IEnumerable<Embed>> BuildEmbeds(RiotAccount account, Match match)
        {
            var info = match.Info;
            var player = info.Participants.First(p => p.SummonerId == account.SummonerId);
            var enemyTeam = info.Participants.Where(p => p.TeamId != player.TeamId);
            var enemyPlayer = enemyTeam.FirstOrDefault(p => p.TeamPosition == player.TeamPosition);

            var queue = await staticApi.GetQueueInfo((int)info.QueueId);

            var playerEmbed = (await GetEmbedParticipantBase(account, player, new EmbedFieldBuilder()
                    .WithName("Position")
                    .WithValue(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(player.TeamPosition.ToLower()))
                    .WithIsInline(true)))
                .WithTitle(player.SummonerName + (player.Win ? " with a Win" : " with a Defeat"))
                .WithAuthor(queue.Description, await dragonApi.GetMapImage((int)info.MapId))
                .WithDescription($"🕒 {TimeSpan.FromSeconds(info.GameDuration).Humanize()}");

            var footerEmbed = playerEmbed;
            EmbedBuilder enemyEmbed = null;

            if (account.ShowVersusInfo && enemyPlayer != null)
            {
                enemyEmbed = (await GetEmbedParticipantBase(account, enemyPlayer))
                    .WithTitle($"Playing against {enemyPlayer.SummonerName}");
                footerEmbed = enemyEmbed;
            }

            footerEmbed.WithTimestamp(DateTimeOffset.FromUnixTimeMilliseconds(info.GameEndTimestamp ?? info.GameStartTimestamp));

            if (player.GameEndedInEarlySurrender)
            {
                footerEmbed.WithFooter("FF15");
            }
            else if (player.GameEndedInSurrender)
            {
                footerEmbed.WithFooter("FF");
            }

            return new List<EmbedBuilder> { playerEmbed, enemyEmbed }
                .Where(e => e != null)
                .Select(e => e.Build());
        }

        private async Task<EmbedBuilder> GetEmbedParticipantBase(RiotAccount account, Participant player, EmbedFieldBuilder fieldBuilder = null)
        {
            var eb = new EmbedBuilder()
                .WithColor((uint)(player.Win ? 3289750 : 9843250))
                .WithThumbnailUrl(await dragonApi.GetChampionSquareImage((int)player.ChampionId));

            if (fieldBuilder != null)
            {
                eb.AddField(fieldBuilder);
            }

            eb.AddField("K/D/A", $"`{player.Kills}/{player.Deaths}/{player.Assists}`", true);
            eb.AddField("Stats", $"Gold `{player.GoldEarned}`\nMinions `{player.TotalMinionsKilled + player.NeutralMinionsKilled}`\nTotal Damage Dealt To Champions `{player.TotalDamageDealtToChampions}`");

            var props = account.GetAdditionalProps();
            if (props != null && props.Any())
            {
                var typeInfo = player.GetType();
                var sb = new StringBuilder();
                foreach (var prop in props)
                {
                    var propInfo = typeInfo.GetProperty(prop);
                    if (propInfo == null) continue;
                    var value = propInfo.GetValue(player);
                    if (value == null) continue;
                    sb.AppendLine($"{prop.Spaceify()} `{value.ToString()}`");
                }

                var additionalStats = sb.ToString();
                if (!string.IsNullOrWhiteSpace(additionalStats))
                {
                    eb.AddField("Additional Stats", additionalStats);
                }
            }

            return eb;
        }
    }
}
