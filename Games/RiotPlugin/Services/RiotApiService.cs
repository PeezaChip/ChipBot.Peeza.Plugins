﻿using Camille.RiotGames;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RiotPlugin.Data.Models;

namespace RiotPlugin.Services
{
    [Chipject]
    public class RiotApiService : ApiService<RiotGamesApi>
    {
        public RiotApiService(ILogger<RiotApiService> log, SettingsService<NetworkConfig> networkConfig, SettingsService<RiotConfig> riotConfig)
            : base(log, networkConfig)
            => ApiClient = RiotGamesApi.NewInstance(riotConfig.Settings.Token);
    }
}
