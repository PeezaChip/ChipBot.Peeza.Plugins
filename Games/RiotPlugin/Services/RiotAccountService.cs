﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Extensions;
using Discord;
using Microsoft.Extensions.Logging;
using RiotPlugin.Data;
using RiotPlugin.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace RiotPlugin.Services
{
    [Chipject]
    public class RiotAccountService : ChipService<RiotAccountService>
	{
		private readonly ChipBotDbContextFactory<RiotDbContext> dbContextFactory;

        public RiotAccountService(ILogger<RiotAccountService> log, ChipBotDbContextFactory<RiotDbContext> dbContextFactory) : base(log)
            => this.dbContextFactory = dbContextFactory;

        public void AddAccount(RiotAccount account)
		{
			logger.LogInformation($"Adding account with id {account.SummonerId}");
            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.RiotAccounts.AddOrUpdate(account, account.SummonerId);
            dbContext.SaveChanges();
		}

        public void RemoveAccount(IUser user, string id) => RemoveAccount(user.Id, id);
		public void RemoveAccount(ulong user, string id)
		{
            using var dbContext = dbContextFactory.CreateDbContext();
            var account = dbContext.RiotAccounts.Find(id);
            if (account == null) return;
            if (account.UserId == user)
            {
                logger.LogInformation($"Removing account with id {id}");
                dbContext.RiotAccounts.Remove(account);
                dbContext.SaveChanges();
            }
		}

		public IEnumerable<RiotAccount> GetAccounts(IUser user) => GetAccounts(user.Id);
		public IEnumerable<RiotAccount> GetAccounts(ulong user)
		{
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.RiotAccounts.Where(a => a.UserId == user).ToList();
		}

        public Dictionary<ulong, List<RiotAccount>> GetAllAccountsDictionary()
            => GetAllAccounts().GroupBy(a => a.UserId).ToDictionary(g => g.Key, g => g.ToList());

        public List<RiotAccount> GetAllAccounts()
		{
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.RiotAccounts.ToList();
		}

        public RiotAccount GetAccount(IUser user, string id) => GetAccount(user.Id, id);
        public RiotAccount GetAccount(ulong user, string id)
		{
            using var dbContext = dbContextFactory.CreateDbContext();
            var account = dbContext.RiotAccounts.Find(id);
            return account == null || account.UserId != user ? null : account;
        }
    }
}
