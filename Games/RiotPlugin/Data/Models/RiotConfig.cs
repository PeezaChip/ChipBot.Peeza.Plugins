﻿using Newtonsoft.Json;

namespace RiotPlugin.Data.Models
{
    public class RiotConfig
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
