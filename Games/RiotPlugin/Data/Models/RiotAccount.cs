﻿using Camille.Enums;
using Camille.RiotGames.SummonerV4;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RiotPlugin.Data.Models
{
    [Index(nameof(AccountId), nameof(PuuId), nameof(Name), IsUnique = true)]
    public class RiotAccount : RiotAccountSettings
    {
        [Key]
        public string SummonerId { get; set; }

        [Required]
        public string AccountId { get; set; }

        [Required]
        public string PuuId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int ProfileIconId { get; set; }

        [Required]
        public long RevisionDate { get; set; }

        [Required]
        public long SummonerLevel { get; set; }

        [Required]
        public PlatformRoute Platform { get; set; }

        [Required]
        public RegionalRoute Region { get; set; }

        [Required]
        public ulong UserId { get; set; }

        public void Merge(Summoner info)
        {
            SummonerLevel = info.SummonerLevel;
            ProfileIconId = info.ProfileIconId;
            RevisionDate = info.RevisionDate;
            Name = info.Name;
        }

        public bool AddPostMatchInfo(string propname)
        {
            var list = GetAdditionalProps();
            if (list.Contains(propname)) return false;
            list.Add(propname);
            SaveAdditionalProps(list);
            return true;
        }

        public bool RemovePostMatchInfo(string propname)
        {
            var list = GetAdditionalProps();
            if (!list.Contains(propname)) return false;
            list.Remove(propname);
            SaveAdditionalProps(list);
            return true;
        }

        public List<string> GetAdditionalProps() => AdditionalProps.Split(',').ToList();
        private void SaveAdditionalProps(IEnumerable<string> props) => AdditionalProps = string.Join(",", props.Where(p => !string.IsNullOrWhiteSpace(p)).OrderBy(p => p[0]));
    }
}
