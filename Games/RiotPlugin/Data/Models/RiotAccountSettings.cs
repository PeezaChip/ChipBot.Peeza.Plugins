﻿namespace RiotPlugin.Data.Models
{
    public class RiotAccountSettings : RiotLastGame
    {
        public bool WatchMatches { get; set; } = true;
        public bool AutoRefreshInfo { get; set; } = true;
        public bool SendPostMatchInfo { get; set; } = true;
        public bool ShowVersusInfo { get; set; } = true;
        public string AdditionalProps { get; set; } = "";
    }
}
