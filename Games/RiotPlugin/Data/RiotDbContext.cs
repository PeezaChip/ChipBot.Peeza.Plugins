﻿using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using RiotPlugin.Data.Models;

namespace RiotPlugin.Data
{
    [ChipDbContext("RiotPlugin")]
    public class RiotDbContext : DbContext
    {
        public DbSet<RiotAccount> RiotAccounts { get; set; }

        public RiotDbContext(DbContextOptions options) : base(options) { }
    }
}
