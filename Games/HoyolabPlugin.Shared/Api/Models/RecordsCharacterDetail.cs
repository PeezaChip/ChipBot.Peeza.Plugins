﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class RecordsCharacterDetailRequestPayload
    {
        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("role_id")]
        public string RoleId { get; set; }

        [JsonProperty("character_ids")]
        public long[] CharacterIds { get; set; }
    }

    public class RecordsCharacterDetail
    {
        [JsonProperty("list")]
        public RecordsCharacterDetailList[] List { get; set; }

        [JsonProperty("property_map")]
        public Dictionary<string, RecordsCharacterDetailPropertyMap> PropertyMap { get; set; }

        [JsonProperty("relic_property_options")]
        public RecordsCharacterDetailRelicPropertyOptions RelicPropertyOptions { get; set; }

        [JsonProperty("relic_wiki")]
        public Dictionary<string, Uri> RelicWiki { get; set; }

        [JsonProperty("weapon_wiki")]
        public Dictionary<string, Uri> WeaponWiki { get; set; }

        [JsonProperty("avatar_wiki")]
        public Dictionary<string, Uri> AvatarWiki { get; set; }
    }

    public class RecordsCharacterDetailList
    {
        [JsonProperty("base")]
        public RecordsCharacter Base { get; set; }

        [JsonProperty("weapon")]
        public RecordsCharacterDetailListWeapon Weapon { get; set; }

        [JsonProperty("relics")]
        public RecordsCharacterDetailRelic[] Relics { get; set; }

        [JsonProperty("constellations")]
        public RecordsCharacterDetailConstellation[] Constellations { get; set; }

        [JsonProperty("costumes")]
        public object[] Costumes { get; set; }

        [JsonProperty("selected_properties")]
        public RecordsCharacterDetailProperty[] SelectedProperties { get; set; }

        [JsonProperty("base_properties")]
        public RecordsCharacterDetailProperty[] BaseProperties { get; set; }

        [JsonProperty("extra_properties")]
        public RecordsCharacterDetailProperty[] ExtraProperties { get; set; }

        [JsonProperty("element_properties")]
        public RecordsCharacterDetailProperty[] ElementProperties { get; set; }

        [JsonProperty("skills")]
        public RecordsCharacterDetailSkill[] Skills { get; set; }

        [JsonProperty("recommend_relic_property")]
        public RecordsCharacterDetailRecommendRelicProperty RecommendRelicProperty { get; set; }
    }

    public partial class RecordsCharacterDetailProperty
    {
        [JsonProperty("property_type")]
        public long PropertyType { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("add")]
        public string Add { get; set; }

        [JsonProperty("final")]
        public string Final { get; set; }
    }

    public partial class RecordsCharacterDetailConstellation
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("effect")]
        public string Effect { get; set; }

        [JsonProperty("is_actived")]
        public bool IsActived { get; set; }

        [JsonProperty("pos")]
        public long Pos { get; set; }
    }

    public partial class RecordsCharacterDetailRecommendRelicProperty
    {
        [JsonProperty("recommend_properties")]
        public RecordsCharacterDetailRelicPropertyOptions RecommendProperties { get; set; }

        [JsonProperty("custom_properties")]
        public object CustomProperties { get; set; }

        [JsonProperty("has_set_recommend_prop")]
        public bool HasSetRecommendProp { get; set; }
    }

    public partial class RecordsCharacterDetailRelicPropertyOptions
    {
        [JsonProperty("sand_main_property_list")]
        public long[] SandMainPropertyList { get; set; }

        [JsonProperty("goblet_main_property_list")]
        public long[] GobletMainPropertyList { get; set; }

        [JsonProperty("circlet_main_property_list")]
        public long[] CircletMainPropertyList { get; set; }

        [JsonProperty("sub_property_list")]
        public long[] SubPropertyList { get; set; }
    }

    public partial class RecordsCharacterDetailRelic
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("pos")]
        public long Pos { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("set")]
        public RecordsCharacterDetailSet Set { get; set; }

        [JsonProperty("pos_name")]
        public string PosName { get; set; }

        [JsonProperty("main_property")]
        public RecordsCharacterDetailMainProperty MainProperty { get; set; }

        [JsonProperty("sub_property_list")]
        public RecordsCharacterDetailMainProperty[] SubPropertyList { get; set; }
    }

    public partial class RecordsCharacterDetailMainProperty
    {
        [JsonProperty("property_type")]
        public long PropertyType { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("times")]
        public long Times { get; set; }
    }

    public partial class RecordsCharacterDetailSet
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("affixes")]
        public RecordsCharacterDetailAffix[] Affixes { get; set; }
    }

    public partial class RecordsCharacterDetailAffix
    {
        [JsonProperty("activation_number")]
        public long ActivationNumber { get; set; }

        [JsonProperty("effect")]
        public string Effect { get; set; }
    }

    public partial class RecordsCharacterDetailSkill
    {
        [JsonProperty("skill_id")]
        public long SkillId { get; set; }

        [JsonProperty("skill_type")]
        public long SkillType { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("skill_affix_list")]
        public RecordsCharacterDetailSkillAffixList[] SkillAffixList { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("is_unlock")]
        public bool IsUnlock { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class RecordsCharacterDetailSkillAffixList
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class RecordsCharacterDetailListWeapon
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("promote_level")]
        public long PromoteLevel { get; set; }

        [JsonProperty("type_name")]
        public string TypeName { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("affix_level")]
        public long AffixLevel { get; set; }

        [JsonProperty("main_property")]
        public RecordsCharacterDetailProperty MainProperty { get; set; }

        [JsonProperty("sub_property")]
        public RecordsCharacterDetailProperty SubProperty { get; set; }
    }

    public partial class RecordsCharacterDetailPropertyMap
    {
        [JsonProperty("property_type")]
        public long PropertyType { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("filter_name")]
        public string FilterName { get; set; }
    }
}
