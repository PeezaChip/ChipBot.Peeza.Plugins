﻿using HoyolabPlugin.Shared.Api.Models.Converters;
using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class SpiralAbyssData
    {
        [JsonProperty("schedule_id")]
        public long ScheduleId { get; set; }

        [JsonProperty("start_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long StartTime { get; set; }

        [JsonProperty("end_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long EndTime { get; set; }

        [JsonProperty("total_battle_times")]
        public long TotalBattleTimes { get; set; }

        [JsonProperty("total_win_times")]
        public long TotalWinTimes { get; set; }

        [JsonProperty("max_floor")]
        public string MaxFloor { get; set; }

        [JsonProperty("reveal_rank")]
        public RankData[] RevealRank { get; set; }

        [JsonProperty("defeat_rank")]
        public RankData[] DefeatRank { get; set; }

        [JsonProperty("damage_rank")]
        public RankData[] DamageRank { get; set; }

        [JsonProperty("take_damage_rank")]
        public RankData[] TakeDamageRank { get; set; }

        [JsonProperty("normal_skill_rank")]
        public RankData[] NormalSkillRank { get; set; }

        [JsonProperty("energy_skill_rank")]
        public RankData[] EnergySkillRank { get; set; }

        [JsonProperty("floors")]
        public FloorData[] Floors { get; set; }

        [JsonProperty("total_star")]
        public long TotalStar { get; set; }

        [JsonProperty("is_unlock")]
        public bool IsUnlock { get; set; }
    }

    public class RankData
    {
        [JsonProperty("avatar_id")]
        public long AvatarId { get; set; }

        [JsonProperty("avatar_icon")]
        public Uri AvatarIcon { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }
    }

    public class FloorData
    {
        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("is_unlock")]
        public bool IsUnlock { get; set; }

        [JsonProperty("settle_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long SettleTime { get; set; }

        [JsonProperty("star")]
        public long Star { get; set; }

        [JsonProperty("max_star")]
        public long MaxStar { get; set; }

        [JsonProperty("levels")]
        public Level[] Levels { get; set; }
    }

    public class Level
    {
        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("star")]
        public long Star { get; set; }

        [JsonProperty("max_star")]
        public long MaxStar { get; set; }

        [JsonProperty("battles")]
        public Battle[] Battles { get; set; }
    }

    public class Battle
    {
        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("timestamp")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long Timestamp { get; set; }

        [JsonProperty("avatars")]
        public Avatar[] Avatars { get; set; }
    }

    public class Avatar
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }
    }
}
