﻿using HoyolabPlugin.Shared.Api.Models.Converters;
using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class RecordsCharacterListRequestPayload
    {
        [JsonProperty("role_id")]
        public string RoleId { get; set; }

        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("sort_type")]
        public long SortType { get; set; } = 1;
    }

    public class RecordsCharacterList
    {
        [JsonProperty("list")]
        public RecordsCharacter[] List { get; set; }
    }

    public class RecordsCharacter
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("element")]
        [JsonConverter(typeof(ElementConverter))]
        [System.Text.Json.Serialization.JsonConverter(typeof(ElementConverterText))]
        public Element Element { get; set; }

        [JsonProperty("fetter")]
        public long Fetter { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("actived_constellation_num")]
        public long ActivedConstellationNum { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("is_chosen")]
        public bool IsChosen { get; set; }

        [JsonProperty("side_icon")]
        public Uri SideIcon { get; set; }

        [JsonProperty("weapon_type")]
        public long WeaponType { get; set; }

        [JsonProperty("weapon")]
        public RecordsWeapon Weapon { get; set; }
    }

    public class RecordsWeapon
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("affix_level")]
        public long AffixLevel { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public enum Element { None, Anemo, Cryo, Dendro, Electro, Geo, Hydro, Pyro };
}
