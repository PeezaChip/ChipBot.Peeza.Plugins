﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class RecordsIndexData
    {
        [JsonProperty("role")]
        public RecordsIndexRole Role { get; set; }

        [JsonProperty("avatars")]
        public List<RecordsIndexAvatar> Avatars { get; set; }

        [JsonProperty("stats")]
        public RecordsIndexStats Stats { get; set; }

        [JsonProperty("city_explorations")]
        public List<object> CityExplorations { get; set; }

        [JsonProperty("world_explorations")]
        public List<RecordsIndexWorldExploration> WorldExplorations { get; set; }

        [JsonProperty("homes")]
        public List<RecordsIndexHome> Homes { get; set; }

        [JsonProperty("query_tool_link")]
        public string QueryToolLink { get; set; }

        [JsonProperty("query_tool_image")]
        public string QueryToolImage { get; set; }
    }

    public class RecordsIndexAvatar
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("element")]
        public string Element { get; set; }

        [JsonProperty("fetter")]
        public long Fetter { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("actived_constellation_num")]
        public long ActivedConstellationNum { get; set; }

        [JsonProperty("card_image")]
        public string CardImage { get; set; }

        [JsonProperty("is_chosen")]
        public bool IsChosen { get; set; }
    }

    public class RecordsIndexHome
    {
        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("visit_num")]
        public long VisitNum { get; set; }

        [JsonProperty("comfort_num")]
        public long ComfortNum { get; set; }

        [JsonProperty("item_num")]
        public long ItemNum { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("comfort_level_name")]
        public string ComfortLevelName { get; set; }

        [JsonProperty("comfort_level_icon")]
        public string ComfortLevelIcon { get; set; }
    }

    public class RecordsIndexRole
    {
        [JsonProperty("AvatarUrl")]
        public string AvatarUrl { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("game_head_icon")]
        public string GameHeadIcon { get; set; }
    }

    public class RecordsIndexStats
    {
        [JsonProperty("active_day_number")]
        public long ActiveDayNumber { get; set; }

        [JsonProperty("achievement_number")]
        public long AchievementNumber { get; set; }

        [JsonProperty("anemoculus_number")]
        public long AnemoculusNumber { get; set; }

        [JsonProperty("geoculus_number")]
        public long GeoculusNumber { get; set; }

        [JsonProperty("avatar_number")]
        public long AvatarNumber { get; set; }

        [JsonProperty("way_point_number")]
        public long WayPointNumber { get; set; }

        [JsonProperty("domain_number")]
        public long DomainNumber { get; set; }

        [JsonProperty("spiral_abyss")]
        public string SpiralAbyss { get; set; }

        [JsonProperty("precious_chest_number")]
        public long PreciousChestNumber { get; set; }

        [JsonProperty("luxstringous_chest_number")]
        public long LuxstringousChestNumber { get; set; }

        [JsonProperty("exquisite_chest_number")]
        public long ExquisiteChestNumber { get; set; }

        [JsonProperty("common_chest_number")]
        public long CommonChestNumber { get; set; }

        [JsonProperty("electroculus_number")]
        public long ElectroculusNumber { get; set; }

        [JsonProperty("magic_chest_number")]
        public long MagicChestNumber { get; set; }

        [JsonProperty("dendroculus_number")]
        public long DendroculusNumber { get; set; }

        [JsonProperty("hydroculus_number")]
        public long HydroculusNumber { get; set; }

        [JsonProperty("field_ext_map")]
        public Dictionary<string, RecordsIndexFieldExtMap> FieldExtMap { get; set; }
    }

    public class RecordsIndexFieldExtMap
    {
        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("backup_link")]
        public string BackupLink { get; set; }
    }

    public class RecordsIndexWorldExploration
    {
        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("exploration_percentage")]
        public long ExplorationPercentage { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("offerings")]
        public List<RecordsIndexOffering> Offerings { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("parent_id")]
        public long ParentId { get; set; }

        [JsonProperty("map_url")]
        public string MapUrl { get; set; }

        [JsonProperty("strategy_url")]
        public string StrategyUrl { get; set; }

        [JsonProperty("background_image")]
        public string BackgroundImage { get; set; }

        [JsonProperty("inner_icon")]
        public string InnerIcon { get; set; }

        [JsonProperty("cover")]
        public string Cover { get; set; }

        [JsonProperty("area_exploration_list")]
        public List<RecordsIndexAreaExplorationList> AreaExplorationList { get; set; }

        [JsonProperty("boss_list")]
        public List<RecordsIndexBossList> BossList { get; set; }

        [JsonProperty("is_hot")]
        public bool IsHot { get; set; }
    }

    public class RecordsIndexAreaExplorationList
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("exploration_percentage")]
        public long ExplorationPercentage { get; set; }
    }

    public class RecordsIndexBossList
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("kill_num")]
        public long KillNum { get; set; }
    }

    public class RecordsIndexOffering
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
