﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public partial class ForgottenHallData
    {
        [JsonProperty("schedule_id")]
        public long ScheduleId { get; set; }

        [JsonProperty("begin_time")]
        public ForgottenHallTime BeginTime { get; set; }

        [JsonProperty("end_time")]
        public ForgottenHallTime EndTime { get; set; }

        [JsonProperty("star_num")]
        public long StarNum { get; set; }

        [JsonProperty("max_floor")]
        public string MaxFloor { get; set; }

        [JsonProperty("battle_num")]
        public long BattleNum { get; set; }

        [JsonProperty("has_data")]
        public bool HasData { get; set; }

        [JsonProperty("max_floor_detail")]
        public object MaxFloorDetail { get; set; }

        [JsonProperty("all_floor_detail")]
        public ForgottenHallAllFloorDetail[] AllFloorDetail { get; set; }
    }

    public partial class ForgottenHallAllFloorDetail
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("round_num")]
        public long RoundNum { get; set; }

        [JsonProperty("star_num")]
        public long StarNum { get; set; }

        [JsonProperty("node_1")]
        public ForgottenHallNode Node1 { get; set; }

        [JsonProperty("node_2")]
        public ForgottenHallNode Node2 { get; set; }

        [JsonProperty("is_chaos")]
        public bool IsChaos { get; set; }
    }

    public partial class ForgottenHallNode
    {
        [JsonProperty("challenge_time")]
        public ForgottenHallTime ChallengeTime { get; set; }

        [JsonProperty("avatars")]
        public ForgottenHallAvatar[] Avatars { get; set; }
    }

    public partial class ForgottenHallAvatar
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("element")]
        public string Element { get; set; }
    }

    public partial class ForgottenHallTime
    {
        [JsonProperty("year")]
        public long Year { get; set; }

        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("day")]
        public long Day { get; set; }

        [JsonProperty("hour")]
        public long Hour { get; set; }

        [JsonProperty("minute")]
        public long Minute { get; set; }
    }
}
