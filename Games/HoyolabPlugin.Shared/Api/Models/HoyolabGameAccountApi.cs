﻿namespace HoyolabPlugin.Shared.Api.Models
{
    public class HoyolabGameAccountApi
    {
        public int Uid { get; set; }

        public string Game { get; set; }

        public string Region { get; set; }

        public string DisplayName { get; set; }

        public int HoyolabAccountId { get; set; }

        public RecordsIndexHsrData HsrData { get; set; }

        public RecordsIndexData GenshinData { get; set; }
    }
}
