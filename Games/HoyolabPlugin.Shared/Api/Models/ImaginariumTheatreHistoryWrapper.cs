﻿namespace HoyolabPlugin.Shared.Api.Models
{
    public class ImaginariumTheatreHistoryWrapper
    {
        public ImaginariumTheaterData ImaginariumTheater { get; set; }
        public RecordsCharacterDetail CharacterDetails { get; set; }
    }
}
