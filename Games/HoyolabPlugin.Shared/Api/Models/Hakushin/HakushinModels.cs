﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace HoyolabPlugin.Shared.Api.Models.Hakushin
{
    public class HakushinData
    {
        public Dictionary<string, Character> Characters { get; set; } = [];
        public Dictionary<string, Weapon> Weapons { get; set; } = [];
        public Dictionary<string, Item> Items { get; set; } = [];

        public Dictionary<string, CharacterFull> CharactersFull { get; set; } = [];
        public Dictionary<string, WeaponFull> WeaponsFull { get; set; } = [];
        public Dictionary<string, ItemFull> ItemsFull { get; set; } = [];

        public HakushinData ToHalf()
        {
            return new HakushinData
            {
                Characters = Characters,
                Weapons = Weapons,
                Items = Items,
            };
        }
    }

    public partial class Item
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Rank")]
        public long? Rank { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Week", NullValueHandling = NullValueHandling.Ignore)]
        public long? Week { get; set; }
    }

    public partial class WeaponFull
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("WeaponType")]
        [System.Text.Json.Serialization.JsonConverter(typeof(WeaponTypeTextConverter))]
        public WeaponType WeaponType { get; set; }

        [JsonProperty("Rarity")]
        public long Rarity { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("StatsModifier")]
        public WeaponFullStatsModifier StatsModifier { get; set; }

        [JsonProperty("XPRequirements")]
        public Dictionary<string, double> XpRequirements { get; set; }

        [JsonProperty("Ascension")]
        public Dictionary<string, AscensionValue> Ascension { get; set; }

        [JsonProperty("Refinement")]
        public Dictionary<string, Refinement> Refinement { get; set; }

        [JsonProperty("Materials")]
        public Dictionary<string, Material> Materials { get; set; }
    }

    public partial class AscensionValue
    {
        [JsonProperty("FIGHT_PROP_BASE_ATTACK")]
        public double FightPropBaseAttack { get; set; }
    }

    public partial class Material
    {
        [JsonProperty("Mats")]
        public List<Mat> Mats { get; set; }

        [JsonProperty("Cost")]
        public long Cost { get; set; }
    }

    public partial class Mat
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Count")]
        public long Count { get; set; }
    }

    public partial class Refinement
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("ParamList")]
        public List<double> ParamList { get; set; }

        [JsonProperty("Unlock", NullValueHandling = NullValueHandling.Ignore)]
        public long? Unlock { get; set; }
    }

    public partial class WeaponFullStatsModifier
    {
        [JsonProperty("ATK")]
        public Atk Atk { get; set; }

        [JsonProperty("FIGHT_PROP_CRITICAL_HURT")]
        public Atk FightPropCriticalHurt { get; set; }
    }

    public partial class Atk
    {
        [JsonProperty("Base")]
        public double Base { get; set; }

        [JsonProperty("Levels")]
        public Dictionary<string, double> Levels { get; set; }
    }

    public partial class Weapon
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }

        [JsonProperty("type")]
        [System.Text.Json.Serialization.JsonConverter(typeof(WeaponTypeTextConverter))]
        public WeaponType Type { get; set; }

        [JsonProperty("EN")]
        public string En { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("KR")]
        public string Kr { get; set; }

        [JsonProperty("CHS")]
        public string Chs { get; set; }

        [JsonProperty("JP")]
        public string Jp { get; set; }
    }

    public partial class ItemFull
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("Rank")]
        public long? Rank { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("ItemType")]
        public string ItemType { get; set; }

        [JsonProperty("MaterialType")]
        public string MaterialType { get; set; }

        [JsonProperty("JumpDescs")]
        public List<string> JumpDescs { get; set; }

        [JsonProperty("SourceList")]
        public List<string> SourceList { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Effect")]
        public string Effect { get; set; }

        [JsonProperty("Special")]
        public string Special { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Week", NullValueHandling = NullValueHandling.Ignore)]
        public long? Week { get; set; }
    }

    public partial class CharacterFull
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("CharaInfo")]
        public CharaInfo CharaInfo { get; set; }

        [JsonProperty("Weapon")]
        [System.Text.Json.Serialization.JsonConverter(typeof(WeaponTypeTextConverter))]
        public WeaponType Weapon { get; set; }

        [JsonProperty("Rarity")]
        public string Rarity { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("StaminaRecovery")]
        public long StaminaRecovery { get; set; }

        [JsonProperty("BaseHP")]
        public double BaseHp { get; set; }

        [JsonProperty("BaseATK")]
        public double BaseAtk { get; set; }

        [JsonProperty("BaseDEF")]
        public double BaseDef { get; set; }

        [JsonProperty("CritRate")]
        public double CritRate { get; set; }

        [JsonProperty("CritDMG")]
        public double CritDmg { get; set; }

        [JsonProperty("StatsModifier")]
        public CharacterFullStatsModifier StatsModifier { get; set; }

        [JsonProperty("Skills")]
        public List<Skill> Skills { get; set; }

        [JsonProperty("Passives")]
        public List<Refinement> Passives { get; set; }

        [JsonProperty("Constellations")]
        public List<Refinement> Constellations { get; set; }

        [JsonProperty("Materials")]
        public Materials Materials { get; set; }
    }

    public partial class CharaInfo
    {
        [JsonProperty("ReleaseDate")]
        public DateTimeOffset ReleaseDate { get; set; }

        [JsonProperty("Birth")]
        public List<long> Birth { get; set; }

        [JsonProperty("Vision")]
        [System.Text.Json.Serialization.JsonConverter(typeof(VisionTextConverter))]
        public Vision Vision { get; set; }

        [JsonProperty("Constellation")]
        public string Constellation { get; set; }

        [JsonProperty("Region")]
        public string Region { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Native")]
        public string Native { get; set; }

        [JsonProperty("Detail")]
        public string Detail { get; set; }

        [JsonProperty("VA")]
        public Va Va { get; set; }

        [JsonProperty("Stories")]
        public List<Story> Stories { get; set; }

        [JsonProperty("Quotes")]
        public List<Quote> Quotes { get; set; }

        [JsonProperty("SpecialFood")]
        public SpecialFood SpecialFood { get; set; }

        [JsonProperty("Namecard")]
        public Namecard Namecard { get; set; }

        [JsonProperty("Costume")]
        public List<Namecard> Costume { get; set; }
    }

    public partial class Namecard
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("Quality")]
        public object Quality { get; set; }
    }

    public partial class Quote
    {
        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("Unlocked")]
        public List<string> Unlocked { get; set; }
    }

    public partial class SpecialFood
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Recipe")]
        public long Recipe { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("Rank")]
        public long Rank { get; set; }
    }

    public partial class Story
    {
        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("Unlock")]
        public List<string> Unlock { get; set; }
    }

    public partial class Va
    {
        [JsonProperty("Chinese")]
        public string Chinese { get; set; }

        [JsonProperty("Japanese")]
        public string Japanese { get; set; }

        [JsonProperty("English")]
        public string English { get; set; }

        [JsonProperty("Korean")]
        public string Korean { get; set; }
    }

    public partial class Materials
    {
        [JsonProperty("Ascensions")]
        public List<Material> Ascensions { get; set; }

        [JsonProperty("Talents")]
        public List<List<Material>> Talents { get; set; }
    }

    public partial class Skill
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Desc")]
        public string Desc { get; set; }

        [JsonProperty("Promote")]
        public Dictionary<string, Promote> Promote { get; set; }
    }

    public partial class Promote
    {
        [JsonProperty("Level")]
        public long Level { get; set; }

        [JsonProperty("Icon")]
        public string Icon { get; set; }

        [JsonProperty("Desc")]
        public List<string> Desc { get; set; }

        [JsonProperty("Param")]
        public List<double> Param { get; set; }
    }

    public partial class CharacterFullStatsModifier
    {
        [JsonProperty("HP")]
        public Dictionary<string, double> Hp { get; set; }

        [JsonProperty("ATK")]
        public Dictionary<string, double> Atk { get; set; }

        [JsonProperty("DEF")]
        public Dictionary<string, double> Def { get; set; }

        [JsonProperty("Ascension")]
        public List<Dictionary<string, double>> Ascension { get; set; }
    }

    public partial class Character
    {
        [JsonProperty("birth")]
        public List<long> Birth { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("rank")]
        public string Rank { get; set; }

        [JsonProperty("weapon")]
        [System.Text.Json.Serialization.JsonConverter(typeof(WeaponTypeTextConverter))]
        public WeaponType Weapon { get; set; }

        [JsonProperty("release")]
        public DateTimeOffset Release { get; set; }

        [JsonProperty("EN")]
        public string En { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("element")]
        [System.Text.Json.Serialization.JsonConverter(typeof(VisionTextConverter))]
        public Vision Element { get; set; }

        [JsonProperty("KR")]
        public string Kr { get; set; }

        [JsonProperty("CHS")]
        public string Chs { get; set; }

        [JsonProperty("JP")]
        public string Jp { get; set; }
    }

    public enum WeaponType { WeaponBow, WeaponCatalyst, WeaponClaymore, WeaponPole, WeaponSwordOneHand, Unknown };

    public enum Vision { Anemo, Cryo, Dendro, Electro, Geo, Hydro, Pyro, Unknown };

    public static class HakushinConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
                {
                    WeaponTypeConverter.Singleton,
                    VisionConverter.Singleton,
                    new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
                },
        };
    }

    internal class WeaponTypeTextConverter : System.Text.Json.Serialization.JsonConverter<WeaponType>
    {
        public override WeaponType Read(ref System.Text.Json.Utf8JsonReader reader, Type typeToConvert, System.Text.Json.JsonSerializerOptions options)
        {
            var value = reader.GetString();
            return value switch
            {
                "WEAPON_BOW" => WeaponType.WeaponBow,
                "WEAPON_CATALYST" => WeaponType.WeaponCatalyst,
                "WEAPON_CLAYMORE" => WeaponType.WeaponClaymore,
                "WEAPON_POLE" => WeaponType.WeaponPole,
                "WEAPON_SWORD_ONE_HAND" => WeaponType.WeaponSwordOneHand,
                _ => WeaponType.Unknown,
            };
        }

        public override void Write(System.Text.Json.Utf8JsonWriter writer, WeaponType value, System.Text.Json.JsonSerializerOptions options)
        {
            switch (value)
            {
                case WeaponType.WeaponBow:
                    writer.WriteStringValue("WEAPON_BOW");
                    return;
                case WeaponType.WeaponCatalyst:
                    writer.WriteStringValue("WEAPON_CATALYST");
                    return;
                case WeaponType.WeaponClaymore:
                    writer.WriteStringValue("WEAPON_CLAYMORE");
                    return;
                case WeaponType.WeaponPole:
                    writer.WriteStringValue("WEAPON_POLE");
                    return;
                case WeaponType.WeaponSwordOneHand:
                    writer.WriteStringValue("WEAPON_SWORD_ONE_HAND");
                    return;
                case WeaponType.Unknown:
                    writer.WriteStringValue("Unknown");
                    return;
            }
            throw new Exception("Cannot marshal type WeaponType");
        }
    }

    internal class WeaponTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(WeaponType) || t == typeof(WeaponType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            return value switch
            {
                "WEAPON_BOW" => WeaponType.WeaponBow,
                "WEAPON_CATALYST" => WeaponType.WeaponCatalyst,
                "WEAPON_CLAYMORE" => WeaponType.WeaponClaymore,
                "WEAPON_POLE" => WeaponType.WeaponPole,
                "WEAPON_SWORD_ONE_HAND" => WeaponType.WeaponSwordOneHand,
                _ => WeaponType.Unknown,
            };
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (WeaponType)untypedValue;
            switch (value)
            {
                case WeaponType.WeaponBow:
                    serializer.Serialize(writer, "WEAPON_BOW");
                    return;
                case WeaponType.WeaponCatalyst:
                    serializer.Serialize(writer, "WEAPON_CATALYST");
                    return;
                case WeaponType.WeaponClaymore:
                    serializer.Serialize(writer, "WEAPON_CLAYMORE");
                    return;
                case WeaponType.WeaponPole:
                    serializer.Serialize(writer, "WEAPON_POLE");
                    return;
                case WeaponType.WeaponSwordOneHand:
                    serializer.Serialize(writer, "WEAPON_SWORD_ONE_HAND");
                    return;
                case WeaponType.Unknown:
                    serializer.Serialize(writer, "Unknown");
                    return;
            }
            throw new Exception("Cannot marshal type WeaponType");
        }

        public static readonly WeaponTypeConverter Singleton = new WeaponTypeConverter();
    }

    internal class VisionTextConverter : System.Text.Json.Serialization.JsonConverter<Vision>
    {
        public override Vision Read(ref System.Text.Json.Utf8JsonReader reader, Type typeToConvert, System.Text.Json.JsonSerializerOptions options)
        {
            var value = reader.GetString();
            return value switch
            {
                "Anemo" => Vision.Anemo,
                "Cryo" => Vision.Cryo,
                "Dendro" => Vision.Dendro,
                "Electro" => Vision.Electro,
                "Geo" => Vision.Geo,
                "Hydro" => Vision.Hydro,
                "Pyro" => Vision.Pyro,
                _ => Vision.Unknown,
            };
        }

        public override void Write(System.Text.Json.Utf8JsonWriter writer, Vision value, System.Text.Json.JsonSerializerOptions options)
        {
            switch (value)
            {
                case Vision.Anemo:
                    writer.WriteStringValue("Anemo");
                    return;
                case Vision.Cryo:
                    writer.WriteStringValue("Cryo");
                    return;
                case Vision.Dendro:
                    writer.WriteStringValue("Dendro");
                    return;
                case Vision.Electro:
                    writer.WriteStringValue("Electro");
                    return;
                case Vision.Geo:
                    writer.WriteStringValue("Geo");
                    return;
                case Vision.Hydro:
                    writer.WriteStringValue("Hydro");
                    return;
                case Vision.Pyro:
                    writer.WriteStringValue("Pyro");
                    return;
                case Vision.Unknown:
                    writer.WriteStringValue("Unknown");
                    return;
            }
            throw new Exception("Cannot marshal type Vision");
        }
    }

    internal class VisionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Vision) || t == typeof(Vision?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            return value switch
            {
                "Anemo" => Vision.Anemo,
                "Cryo" => Vision.Cryo,
                "Dendro" => Vision.Dendro,
                "Electro" => Vision.Electro,
                "Geo" => Vision.Geo,
                "Hydro" => Vision.Hydro,
                "Pyro" => Vision.Pyro,
                _ => Vision.Unknown,
            };
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Vision)untypedValue;
            switch (value)
            {
                case Vision.Anemo:
                    serializer.Serialize(writer, "Anemo");
                    return;
                case Vision.Cryo:
                    serializer.Serialize(writer, "Cryo");
                    return;
                case Vision.Dendro:
                    serializer.Serialize(writer, "Dendro");
                    return;
                case Vision.Electro:
                    serializer.Serialize(writer, "Electro");
                    return;
                case Vision.Geo:
                    serializer.Serialize(writer, "Geo");
                    return;
                case Vision.Hydro:
                    serializer.Serialize(writer, "Hydro");
                    return;
                case Vision.Pyro:
                    serializer.Serialize(writer, "Pyro");
                    return;
                case Vision.Unknown:
                    serializer.Serialize(writer, "Unknown");
                    return;
            }
            throw new Exception("Cannot marshal type Vision");
        }

        public static readonly VisionConverter Singleton = new VisionConverter();
    }
}