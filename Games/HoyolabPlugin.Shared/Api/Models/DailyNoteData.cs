﻿using HoyolabPlugin.Shared.Api.Models.Converters;
using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class DailyNoteData
    {
        [JsonProperty("current_resin")]
        public long CurrentResin { get; set; }

        [JsonProperty("max_resin")]
        public long MaxResin { get; set; }

        [JsonProperty("resin_recovery_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long ResinRecoveryTime { get; set; }

        [JsonProperty("finished_task_num")]
        public long FinishedTaskNum { get; set; }

        [JsonProperty("total_task_num")]
        public long TotalTaskNum { get; set; }

        [JsonProperty("is_extra_task_reward_received")]
        public bool IsExtraTaskRewardReceived { get; set; }

        [JsonProperty("remain_resin_discount_num")]
        public long RemainResinDiscountNum { get; set; }

        [JsonProperty("resin_discount_num_limit")]
        public long ResinDiscountNumLimit { get; set; }

        [JsonProperty("current_expedition_num")]
        public long CurrentExpeditionNum { get; set; }

        [JsonProperty("max_expedition_num")]
        public long MaxExpeditionNum { get; set; }

        [JsonProperty("expeditions")]
        public List<Expedition> Expeditions { get; set; }

        [JsonProperty("current_home_coin")]
        public long CurrentHomeCoin { get; set; }

        [JsonProperty("max_home_coin")]
        public long MaxHomeCoin { get; set; }

        [JsonProperty("home_coin_recovery_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long HomeCoinRecoveryTime { get; set; }

        [JsonProperty("calendar_url")]
        public string CalendarUrl { get; set; }

        [JsonProperty("transformer")]
        public Transformer Transformer { get; set; }
    }

    public class Expedition
    {
        [JsonProperty("avatar_side_icon")]
        public Uri AvatarSideIcon { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("remained_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long RemainedTime { get; set; }
    }

    public class Transformer
    {
        [JsonProperty("obtained")]
        public bool Obtained { get; set; }

        [JsonProperty("recovery_time")]
        public RecoveryTime RecoveryTime { get; set; }

        [JsonProperty("wiki")]
        public string Wiki { get; set; }

        [JsonProperty("noticed")]
        public bool Noticed { get; set; }

        [JsonProperty("latest_job_id")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long LatestJobId { get; set; }
    }

    public class RecoveryTime
    {
        [JsonProperty("Day")]
        public long Day { get; set; }

        [JsonProperty("Hour")]
        public long Hour { get; set; }

        [JsonProperty("Minute")]
        public long Minute { get; set; }

        [JsonProperty("Second")]
        public long Second { get; set; }

        [JsonProperty("reached")]
        public bool Reached { get; set; }
    }
}
