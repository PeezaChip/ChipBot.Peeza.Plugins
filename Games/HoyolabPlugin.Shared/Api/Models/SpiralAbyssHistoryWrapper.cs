﻿namespace HoyolabPlugin.Shared.Api.Models
{
    public class SpiralAbyssHistoryWrapper
    {
        public SpiralAbyssData SpiralAbyss { get; set; }
        public RecordsCharacterDetail CharacterDetails { get; set; }
    }
}
