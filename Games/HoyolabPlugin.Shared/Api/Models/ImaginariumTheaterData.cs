﻿using HoyolabPlugin.Shared.Api.Models.Converters;
using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public class ImaginariumTheaterDataRoot
    {
        [JsonProperty("data")]
        public ImaginariumTheaterData[] Data { get; set; }

        [JsonProperty("is_unlock")]
        public bool IsUnlock { get; set; }

        [JsonProperty("links")]
        public ImaginariumTheaterLinks Links { get; set; }
    }

    public class ImaginariumTheaterData
    {
        [JsonProperty("detail")]
        public ImaginariumTheaterDetail Detail { get; set; }

        [JsonProperty("stat")]
        public ImaginariumTheaterStat Stat { get; set; }

        [JsonProperty("schedule")]
        public ImaginariumTheaterSchedule Schedule { get; set; }

        [JsonProperty("has_data")]
        public bool HasData { get; set; }

        [JsonProperty("has_detail_data")]
        public bool HasDetailData { get; set; }
    }

    public class ImaginariumTheaterDetail
    {
        [JsonProperty("rounds_data")]
        public ImaginariumTheaterRoundsDatum[] RoundsData { get; set; }

        [JsonProperty("detail_stat")]
        public ImaginariumTheaterStat DetailStat { get; set; }

        [JsonProperty("lineup_link")]
        public string LineupLink { get; set; }

        [JsonProperty("backup_avatars")]
        public ImaginariumTheaterAvatar[] BackupAvatars { get; set; }

        [JsonProperty("fight_statisic")]
        public ImaginariumTheaterStatFightStatisic FightStatisic { get; set; }
    }

    public class ImaginariumTheaterAvatar
    {
        [JsonProperty("avatar_id")]
        public long AvatarId { get; set; }

        [JsonProperty("avatar_type")]
        public long AvatarType { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("element")]
        [JsonConverter(typeof(ElementConverter))]
        [System.Text.Json.Serialization.JsonConverter(typeof(ElementConverterText))]
        public Element Element { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }
    }

    public class ImaginariumTheaterStat
    {
        [JsonProperty("difficulty_id")]
        public long DifficultyId { get; set; }

        [JsonProperty("max_round_id")]
        public long MaxRoundId { get; set; }

        [JsonProperty("heraldry")]
        public long Heraldry { get; set; }

        [JsonProperty("get_medal_round_list")]
        public long[] GetMedalRoundList { get; set; }

        [JsonProperty("medal_num")]
        public long MedalNum { get; set; }

        [JsonProperty("coin_num")]
        public long CoinNum { get; set; }

        [JsonProperty("avatar_bonus_num")]
        public long AvatarBonusNum { get; set; }

        [JsonProperty("rent_cnt")]
        public long RentCnt { get; set; }
    }

    public partial class ImaginariumTheaterStatFightStatisic
    {
        [JsonProperty("max_defeat_avatar")]
        public ImaginariumTheaterMaxDamageAvatar MaxDefeatAvatar { get; set; }

        [JsonProperty("max_damage_avatar")]
        public ImaginariumTheaterMaxDamageAvatar MaxDamageAvatar { get; set; }

        [JsonProperty("max_take_damage_avatar")]
        public ImaginariumTheaterMaxDamageAvatar MaxTakeDamageAvatar { get; set; }

        [JsonProperty("total_coin_consumed")]
        public ImaginariumTheaterMaxDamageAvatar TotalCoinConsumed { get; set; }

        [JsonProperty("shortest_avatar_list")]
        public ImaginariumTheaterMaxDamageAvatar[] ShortestAvatarList { get; set; }

        [JsonProperty("total_use_time")]
        public long TotalUseTime { get; set; }

        [JsonProperty("is_show_battle_stats")]
        public bool IsShowBattleStats { get; set; }
    }

    public class ImaginariumTheaterMaxDamageAvatar
    {
        [JsonProperty("avatar_id")]
        public long AvatarId { get; set; }

        [JsonProperty("avatar_icon")]
        public string AvatarIcon { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }
    }

    public class ImaginariumTheaterRoundsDatum
    {
        [JsonProperty("avatars")]
        public ImaginariumTheaterAvatar[] Avatars { get; set; }

        [JsonProperty("choice_cards")]
        public ImaginariumTheaterChoiceCard[] ChoiceCards { get; set; }

        [JsonProperty("buffs")]
        public object[] Buffs { get; set; }

        [JsonProperty("is_get_medal")]
        public bool IsGetMedal { get; set; }

        [JsonProperty("round_id")]
        public long RoundId { get; set; }

        [JsonProperty("finish_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long FinishTime { get; set; }

        [JsonProperty("finish_date_time")]
        [JsonConverter(typeof(GenshinDateTimeConverter))]
        public DateTime FinishDateTime { get; set; }

        [JsonProperty("enemies")]
        public ImaginariumTheaterEnemy[] Enemies { get; set; }

        [JsonProperty("splendour_buff")]
        public ImaginariumTheaterSplendourBuff SplendourBuff { get; set; }
    }

    public class ImaginariumTheaterChoiceCard
    {
        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("is_enhanced")]
        public bool IsEnhanced { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }
    }

    public class ImaginariumTheaterEnemy
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public Uri Icon { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("level_effect", NullValueHandling = NullValueHandling.Ignore)]
        public object[] LevelEffect { get; set; }
    }

    public class ImaginariumTheaterSplendourBuff
    {
        [JsonProperty("summary")]
        public ImaginariumTheaterSummary Summary { get; set; }

        [JsonProperty("buffs")]
        public ImaginariumTheaterEnemy[] Buffs { get; set; }
    }

    public class ImaginariumTheaterSummary
    {
        [JsonProperty("total_level")]
        public long TotalLevel { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }
    }

    public class ImaginariumTheaterSchedule
    {
        [JsonProperty("start_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long StartTime { get; set; }

        [JsonProperty("end_time")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long EndTime { get; set; }

        [JsonProperty("schedule_type")]
        public long ScheduleType { get; set; }

        [JsonProperty("schedule_id")]
        public long ScheduleId { get; set; }

        [JsonProperty("start_date_time")]
        [JsonConverter(typeof(GenshinDateTimeConverter))]
        public DateTime StartDateTime { get; set; }

        [JsonProperty("end_date_time")]
        [JsonConverter(typeof(GenshinDateTimeConverter))]
        public DateTime EndDateTime { get; set; }
    }

    public class ImaginariumTheaterLinks
    {
        [JsonProperty("lineup_link")]
        public Uri LineupLink { get; set; }

        [JsonProperty("lineup_link_pc")]
        public Uri LineupLinkPc { get; set; }

        [JsonProperty("strategy_link")]
        public string StrategyLink { get; set; }

        [JsonProperty("lineup_publish_link")]
        public string LineupPublishLink { get; set; }

        [JsonProperty("lineup_publish_link_pc")]
        public string LineupPublishLinkPc { get; set; }
    }
}
