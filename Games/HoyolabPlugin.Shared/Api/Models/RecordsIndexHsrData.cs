﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public partial class RecordsIndexHsrData
    {
        [JsonProperty("stats")]
        public RecordsIndexHsrStats Stats { get; set; }

        [JsonProperty("avatar_list")]
        public List<RecordsIndexHsrAvatarList> AvatarList { get; set; }

        [JsonProperty("cur_head_icon_url")]
        public string CurHeadIconUrl { get; set; }

        [JsonProperty("phone_background_image_url")]
        public string PhoneBackgroundImageUrl { get; set; }
    }

    public partial class RecordsIndexHsrAvatarList
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("element")]
        public string Element { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("rarity")]
        public long Rarity { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }

        [JsonProperty("is_chosen")]
        public bool IsChosen { get; set; }
    }

    public partial class RecordsIndexHsrStats
    {
        [JsonProperty("active_days")]
        public long ActiveDays { get; set; }

        [JsonProperty("avatar_num")]
        public long AvatarNum { get; set; }

        [JsonProperty("achievement_num")]
        public long AchievementNum { get; set; }

        [JsonProperty("chest_num")]
        public long ChestNum { get; set; }

        [JsonProperty("abyss_process")]
        public string AbyssProcess { get; set; }

        [JsonProperty("field_ext_map")]
        public RecordsIndexHsrFieldExtMap FieldExtMap { get; set; }
    }

    public class RecordsIndexHsrFieldExtMap
    {
        [JsonProperty("active_days")]
        public RecordsIndexHsrAchievementNum ActiveDays { get; set; }

        [JsonProperty("avatar_num")]
        public RecordsIndexHsrAchievementNum AvatarNum { get; set; }

        [JsonProperty("achievement_num")]
        public RecordsIndexHsrAchievementNum AchievementNum { get; set; }

        [JsonProperty("chest_num")]
        public RecordsIndexHsrAchievementNum ChestNum { get; set; }
    }

    public class RecordsIndexHsrAchievementNum
    {
        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("backup_link")]
        public string BackupLink { get; set; }
    }
}
