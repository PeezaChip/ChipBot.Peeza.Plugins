﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models.Converters
{
    public class ElementConverterText : System.Text.Json.Serialization.JsonConverter<Element>
    {
        public override Element Read(ref System.Text.Json.Utf8JsonReader reader, Type typeToConvert, System.Text.Json.JsonSerializerOptions options)
        {
            var value = reader.GetString();
            return value switch
            {
                "Anemo" => Element.Anemo,
                "Cryo" => Element.Cryo,
                "Dendro" => Element.Dendro,
                "Electro" => Element.Electro,
                "Geo" => Element.Geo,
                "Hydro" => Element.Hydro,
                "Pyro" => Element.Pyro,
                _ => Element.None
            };
        }

        public override void Write(System.Text.Json.Utf8JsonWriter writer, Element value, System.Text.Json.JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }

    public class ElementConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Element) || t == typeof(Element?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            return value switch
            {
                "Anemo" => Element.Anemo,
                "Cryo" => Element.Cryo,
                "Dendro" => Element.Dendro,
                "Electro" => Element.Electro,
                "Geo" => Element.Geo,
                "Hydro" => Element.Hydro,
                "Pyro" => Element.Pyro,
                _ => Element.None
            };
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Element)untypedValue;
            serializer.Serialize(writer, value.ToString());
        }

        public static readonly ElementConverter Singleton = new();
    }
}
