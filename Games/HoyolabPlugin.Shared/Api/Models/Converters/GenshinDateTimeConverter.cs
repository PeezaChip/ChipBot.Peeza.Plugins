﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models.Converters
{
    public class GenshinDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<GenshinDateTime>(reader);
            return new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second);
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DateTime)untypedValue;
            serializer.Serialize(writer, GenshinDateTime.FromDateTime(value));
            return;
        }

        public static readonly GenshinDateTimeConverter Singleton = new();

        private class GenshinDateTime
        {
            [JsonProperty("year")]
            public int Year { get; set; }

            [JsonProperty("month")]
            public int Month { get; set; }

            [JsonProperty("day")]
            public int Day { get; set; }

            [JsonProperty("hour")]
            public int Hour { get; set; }

            [JsonProperty("minute")]
            public int Minute { get; set; }

            [JsonProperty("second")]
            public int Second { get; set; }

            public static GenshinDateTime FromDateTime(DateTime dateTime)
            {
                return new GenshinDateTime()
                {
                    Year = dateTime.Year,
                    Month = dateTime.Month,
                    Day = dateTime.Day,
                    Hour = dateTime.Hour,
                    Minute = dateTime.Minute,
                    Second = dateTime.Second
                };
            }
        }
    }
}
