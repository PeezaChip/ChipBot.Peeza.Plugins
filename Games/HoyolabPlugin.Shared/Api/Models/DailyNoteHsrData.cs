﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Shared.Api.Models
{
    public partial class DailyNoteHsrData
    {
        [JsonProperty("current_stamina")]
        public long CurrentStamina { get; set; }

        [JsonProperty("max_stamina")]
        public long MaxStamina { get; set; }

        [JsonProperty("stamina_recover_time")]
        public long StaminaRecoverTime { get; set; }

        [JsonProperty("accepted_epedition_num")]
        public long AcceptedEpeditionNum { get; set; }

        [JsonProperty("total_expedition_num")]
        public long TotalExpeditionNum { get; set; }

        [JsonProperty("expeditions")]
        public Expedition[] Expeditions { get; set; }
    }

    public partial class HsrExpedition
    {
        [JsonProperty("avatars")]
        public Uri[] Avatars { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("remaining_time")]
        public long RemainingTime { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
