﻿namespace HoyolabPlugin.Shared.Api.Models
{
    public class GameAccountKey
    {
        public string Game { get; private set; }
        public string UId { get; private set; }

        public GameAccountKey(string game, string uid)
        {
            Game = game;
            UId = uid;
        }

        public override string ToString()
            => Build(Game, UId);

        public static GameAccountKey Parse(string key)
        {
            var split = key.Split('|');
            return new GameAccountKey(split[0], split[1]);
        }

        public static string Build(HoyolabGameAccountApi account) => Build(account.Game, account.Uid);

        public static string Build(string game, int uid) => Build(game, uid.ToString());

        public static string Build(string game, string uid)
            => $"{game}|{uid}";
    }
}
