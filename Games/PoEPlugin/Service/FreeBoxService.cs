﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using RSSPlugin.Core;

namespace PoEPlugin.Service
{
    [FeedConsumer("https://www.pathofexile.com/news/rss")]
    public class FreeBoxService : ChipService<FreeBoxService>, IFeedConsumer
    {
        private readonly SettingsService<PoEConfig> config;
        private readonly DiscordSocketClient discord;

        public FreeBoxService(ILogger<FreeBoxService> log, SettingsService<PoEConfig> config, DiscordSocketClient discord) : base(log)
        {
            this.config = config;
            this.discord = discord;
        }

        public async Task Consume(Feed feed)
        {
            if (config.Settings.ChannelId == 0) return;

            foreach (var item in feed.Items)
            {
                if (item.Title.Text.Contains("Free") && item.Title.Text.Contains("Mystery Box"))
                {
                    var channel = await discord.GetChannelAsync(config.Settings.ChannelId) as ITextChannel;
                    var embed = feed.Info.GetEmbed().WithDescription($"{item.Title.Text}\n{string.Concat(Enumerable.Repeat(config.Settings.LoginEmoji, 3))}");

                    if (item.Links.Any())
                    {
                        embed.WithUrl(item.Links.First().Uri.ToString());
                    }

                    await channel.SendMessageAsync(embed: embed.Build());
                }
            }
        }
    }
}
