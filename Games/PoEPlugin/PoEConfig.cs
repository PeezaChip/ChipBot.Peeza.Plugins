﻿namespace PoEPlugin
{
    public class PoEConfig
    {
        public ulong ChannelId { get; set; }
        public string LoginEmoji { get; set; } = "<:poe_login:>";
    }
}
