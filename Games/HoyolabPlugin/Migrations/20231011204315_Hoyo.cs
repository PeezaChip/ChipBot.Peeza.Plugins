﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace HoyolabPlugin.Migrations
{
    /// <inheritdoc />
    public partial class Hoyo : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HoyolabAbyss",
                columns: table => new
                {
                    AbyssId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    Server = table.Column<string>(type: "text", nullable: false),
                    AbyssData = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoyolabAbyss", x => new { x.AbyssId, x.UserId, x.Server });
                });

            migrationBuilder.CreateTable(
                name: "HoyolabAccounts",
                columns: table => new
                {
                    Uid = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Cookies = table.Column<string>(type: "text", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    AvatarUrl = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoyolabAccounts", x => x.Uid);
                });

            migrationBuilder.CreateTable(
                name: "HoyolabKeys",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false),
                    Game = table.Column<string>(type: "text", nullable: false),
                    IsActivated = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoyolabKeys", x => new { x.Key, x.Game });
                });

            migrationBuilder.CreateTable(
                name: "HoyolabGameAccounts",
                columns: table => new
                {
                    Uid = table.Column<int>(type: "integer", nullable: false),
                    Game = table.Column<string>(type: "text", nullable: false),
                    Region = table.Column<string>(type: "text", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    HoyolabAccountId = table.Column<int>(type: "integer", nullable: false),
                    DailySignIn = table.Column<bool>(type: "boolean", nullable: false),
                    ActivateKeys = table.Column<bool>(type: "boolean", nullable: false),
                    StoreAbyssData = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoyolabGameAccounts", x => new { x.Uid, x.Game });
                    table.ForeignKey(
                        name: "FK_HoyolabGameAccounts_HoyolabAccounts_HoyolabAccountId",
                        column: x => x.HoyolabAccountId,
                        principalTable: "HoyolabAccounts",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HoyolabGameAccounts_HoyolabAccountId",
                table: "HoyolabGameAccounts",
                column: "HoyolabAccountId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HoyolabAbyss");

            migrationBuilder.DropTable(
                name: "HoyolabGameAccounts");

            migrationBuilder.DropTable(
                name: "HoyolabKeys");

            migrationBuilder.DropTable(
                name: "HoyolabAccounts");
        }
    }
}
