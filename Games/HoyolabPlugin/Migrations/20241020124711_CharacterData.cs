﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HoyolabPlugin.Migrations
{
    /// <inheritdoc />
    public partial class CharacterData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CharacterData",
                table: "HoyolabTheatre",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CharacterData",
                table: "HoyolabAbyss",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CharacterData",
                table: "HoyolabTheatre");

            migrationBuilder.DropColumn(
                name: "CharacterData",
                table: "HoyolabAbyss");
        }
    }
}
