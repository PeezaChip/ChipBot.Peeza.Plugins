﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HoyolabPlugin.Migrations
{
    /// <inheritdoc />
    public partial class HoyoTheatre : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HoyolabTheatre",
                columns: table => new
                {
                    TheatreId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    Server = table.Column<string>(type: "text", nullable: false),
                    TheatreData = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoyolabTheatre", x => new { x.TheatreId, x.UserId, x.Server });
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HoyolabTheatre");
        }
    }
}
