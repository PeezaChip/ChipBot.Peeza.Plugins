﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HoyolabPlugin.Migrations
{
    /// <inheritdoc />
    public partial class HoyoRecordsData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RecordData",
                table: "HoyolabGameAccounts",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "HoyolabAccounts",
                type: "numeric(20,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecordData",
                table: "HoyolabGameAccounts");

            migrationBuilder.AlterColumn<decimal>(
                name: "UserId",
                table: "HoyolabAccounts",
                type: "numeric(20)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,0)");
        }
    }
}
