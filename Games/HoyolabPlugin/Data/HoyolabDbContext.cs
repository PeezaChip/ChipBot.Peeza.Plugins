﻿using ChipBot.Core.Attributes;
using HoyolabPlugin.Models;
using Microsoft.EntityFrameworkCore;

namespace HoyolabPlugin.Data
{
    [ChipDbContext("HoyolabPlugin")]
    public class HoyolabDbContext : DbContext
    {
        public DbSet<HoyolabAbyss> HoyolabAbyss { get; set; }
        public DbSet<HoyolabTheatre> HoyolabTheatre { get; set; }
        public DbSet<HoyolabAccount> HoyolabAccounts { get; set; }
        public DbSet<HoyolabGameAccount> HoyolabGameAccounts { get; set; }
        public DbSet<HoyolabKey> HoyolabKeys { get; set; }

        public HoyolabDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HoyolabAccount>()
                .Navigation(nameof(HoyolabAccount.GameAccounts))
                .AutoInclude();

            modelBuilder.Entity<HoyolabGameAccount>()
                .Navigation(nameof(HoyolabGameAccount.HoyolabAccount))
                .AutoInclude();
        }
    }
}
