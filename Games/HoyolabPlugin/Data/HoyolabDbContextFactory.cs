﻿using ChipBot.Core.Data;

namespace HoyolabPlugin.Data
{
    public class HoyolabDbContextFactory : ChipBotDbContextFactory<HoyolabDbContext> { }
}
