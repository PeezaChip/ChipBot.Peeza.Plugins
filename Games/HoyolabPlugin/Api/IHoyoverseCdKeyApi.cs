﻿using HoyolabPlugin.Api.ApiModels;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("common/apicdkey/api")]
    public interface IHoyoverseCdKeyApi
    {
        public const string BaseAddress = "https://sg-hk4e-api.hoyoverse.com";

        [Header("Origin")]
        public string Origin { get; set; }

        [Header("Referer")]
        public string Referer { get; set; }

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Query("lang")]
        public string Lang { get; set; }

        [Query("game_biz")]
        public string GameBiz { get; set; }

        [Get("webExchangeCdkey")]
        public Task<HoyoLabResponse<CdKeyData>> ActivateCdKey(string uid, string cdkey, string region);
    }
}
