﻿using HoyolabPlugin.Shared.Api.Models.Hakushin;
using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("gi/data")]
    public interface IHakushinApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("character.json")]
        Task<Dictionary<string, Character>> GetCharacters();

        [Get("weapon.json")]
        Task<Dictionary<string, Weapon>> GetWeapons();

        [Get("en/item.json")]
        Task<Dictionary<string, Item>> GetItems();

        [Get("en/character/{id}.json")]
        Task<CharacterFull> GetCharacter([Path] string id);

        [Get("en/weapon/{id}.json")]
        Task<WeaponFull> GetWeapon([Path] string id);

        [Get("en/item/{id}.json")]
        Task<ItemFull> GetItem([Path] string id);
    }
}
