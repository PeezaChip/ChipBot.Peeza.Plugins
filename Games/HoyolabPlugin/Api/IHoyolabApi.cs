﻿using HoyolabPlugin.Api.ApiModels;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    public interface IHoyolabApi
    {
        public const string BaseAddress = "https://bbs-api-os.hoyolab.com";

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Get("community/post/wapi/userPost")]
        public Task<HoyoLabResponse<HoyoLabPagedResponse<UserPost>>> GetUserPosts(string uid, int size = 15);

        [Get("community/post/wapi/getPostFull")]
        public Task<HoyoLabResponse<FullPost>> GetPostFull(string post_id);

        [Get("community/painter/wapi/user/full")]
        public Task<HoyoLabResponse<HoyoLabUserInfoRoot>> GetUserInfo();
    }
}
