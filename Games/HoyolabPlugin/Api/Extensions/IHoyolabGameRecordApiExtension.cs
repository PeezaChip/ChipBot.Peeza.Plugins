﻿using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Shared.Api.Models;
using HoyolabPlugin.Utility;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api.Extensions
{
    public static class IHoyolabGameRecordApiExtension
    {
        public static Task<HoyoLabResponse<SpiralAbyssData>> GetSpiralAbyssData(this IHoyolabGenshinRecordApi api, string server, string role_id, string schedule_type)
            => api.GetSpiralAbyssData(server, role_id, schedule_type, DsSecretGenerator.GenerateDsSecret());

        public static Task<HoyoLabResponse<ImaginariumTheaterDataRoot>> GetImaginariumTheaterData(this IHoyolabGenshinRecordApi api, string server, string role_id, bool need_detail)
            => api.GetImaginariumTheaterData(server, role_id, need_detail ? "true" : "false", DsSecretGenerator.GenerateDsSecret());

        public static Task<HoyoLabResponse<ForgottenHallData>> GetSpiralAbyssDataHsr(this IHoyolabHsrRecordApi api, string server, string role_id, string schedule_type, bool need_all = true)
            => api.GetSpiralAbyssData(server, role_id, schedule_type, DsSecretGenerator.GenerateDsSecret(), need_all);

        public static Task<HoyoLabResponse<DailyNoteData>> GetDailyNoteData(this IHoyolabGenshinRecordApi api, string server, string role_id)
            => api.GetDailyNoteData(server, role_id, DsSecretGenerator.GenerateDsSecret());

        public static Task<HoyoLabResponse<DailyNoteHsrData>> GetDailyNoteDataHsr(this IHoyolabHsrRecordApi api, string server, string role_id)
            => api.GetDailyNoteData(server, role_id, DsSecretGenerator.GenerateDsSecret());

        public static Task<HoyoLabResponse<RecordsIndexData>> GetRecordsIndexData(this IHoyolabGenshinRecordApi api, string server, string role_id)
            => api.GetIndexData(server, role_id, DsSecretGenerator.GenerateDsSecret());

        public static Task<HoyoLabResponse<RecordsIndexHsrData>> GetRecordsIndexDataHsr(this IHoyolabHsrRecordApi api, string server, string role_id)
            => api.GetIndexData(server, role_id, DsSecretGenerator.GenerateDsSecret());
    }
}
