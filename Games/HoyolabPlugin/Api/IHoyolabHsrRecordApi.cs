﻿using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Shared.Api.Models;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("game_record/hkrpg/api")]
    public interface IHoyolabHsrRecordApi
    {
        public const string BaseAddress = "https://bbs-api-os.hoyolab.com";

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Header("x-rpc-app_version")]
        public string AppVersion { get; set; }

        [Header("x-rpc-client_type")]
        public string ClientType { get; set; }

        [Header("x-rpc-language")]
        public string Lang { get; set; }

        [Get("note")]
        public Task<HoyoLabResponse<DailyNoteHsrData>> GetDailyNoteData(string server, string role_id, [Header("DS")] string ds);

        [Get("challenge")]
        public Task<HoyoLabResponse<ForgottenHallData>> GetSpiralAbyssData(string server, string role_id, string schedule_type, [Header("DS")] string ds, bool need_all = true);

        [Get("index")]
        public Task<HoyoLabResponse<RecordsIndexHsrData>> GetIndexData(string server, string role_id, [Header("DS")] string ds);
    }
}
