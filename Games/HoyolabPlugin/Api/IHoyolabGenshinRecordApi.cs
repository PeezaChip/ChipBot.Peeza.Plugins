﻿using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Shared.Api.Models;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("game_record/genshin/api")]
    public interface IHoyolabGenshinRecordApi
    {
        public const string BaseAddress = "https://bbs-api-os.hoyolab.com";

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Header("x-rpc-app_version")]
        public string AppVersion { get; set; }

        [Header("x-rpc-client_type")]
        public string ClientType { get; set; }

        [Header("x-rpc-language")]
        public string Lang { get; set; }

        [Get("spiralAbyss")]
        public Task<HoyoLabResponse<SpiralAbyssData>> GetSpiralAbyssData(string server, string role_id, string schedule_type, [Header("DS")] string ds);

        // &nickname=Name&active=1 unused?
        [Get("role_combat")]
        public Task<HoyoLabResponse<ImaginariumTheaterDataRoot>> GetImaginariumTheaterData(string server, string role_id, string need_detail, [Header("DS")] string ds, string nickname = "Elsie", int active = 1);

        [Post("character/list")]
        public Task<HoyoLabResponse<RecordsCharacterList>> GetCharacterList([Body] RecordsCharacterListRequestPayload payload);

        [Post("character/detail")]
        public Task<HoyoLabResponse<RecordsCharacterDetail>> GetCharacterDetails([Body] RecordsCharacterDetailRequestPayload payload);

        [Get("dailyNote")]
        public Task<HoyoLabResponse<DailyNoteData>> GetDailyNoteData(string server, string role_id, [Header("DS")] string ds);

        [Get("index")]
        public Task<HoyoLabResponse<RecordsIndexData>> GetIndexData(string server, string role_id, [Header("DS")] string ds);
    }
}
