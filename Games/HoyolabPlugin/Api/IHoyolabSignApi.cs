﻿using HoyolabPlugin.Api.ApiModels;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("event/{event}")]
    public interface IHoyolabSignApi
    {
        public const string BaseGIAddress = "https://sg-hk4e-api.hoyolab.com";
        public const string BaseHSRAddress = "https://sg-public-api.hoyolab.com";

        [Header("Origin")]
        public string Origin { get; set; }

        [Header("Referer")]
        public string Referer { get; set; }

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Query("act_id")]
        public string ActId { get; set; }

        [Query("lang")]
        public string Lang { get; set; }

        [Path("event")]
        public string Event { get; set; }

        [Get("home")]
        public Task<HoyoLabResponse<HomeData>> GetHome();

        [Get("extra_award")]
        public Task<HoyoLabResponse<ExtraAwardData>> GetExtraAward();

        [Get("info")]
        public Task<HoyoLabResponse<InfoData>> GetInfo();

        [Get("resign_info")]
        public Task<HoyoLabResponse<ResignInfoData>> GetResignInfo();

        [Post("sign")]
        public Task<HoyoLabResponse<SignData>> PostSign([Body] SignPayload payload);
    }
}
