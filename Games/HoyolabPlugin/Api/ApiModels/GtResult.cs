﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class GtResult
    {
        [JsonProperty("risk_code")]
        public long RiskCode { get; set; }

        [JsonProperty("gt")]
        public string Gt { get; set; }

        [JsonProperty("challenge")]
        public string Challenge { get; set; }

        [JsonProperty("success")]
        public long Success { get; set; }

        [JsonProperty("is_risk")]
        public bool IsRisk { get; set; }
    }
}
