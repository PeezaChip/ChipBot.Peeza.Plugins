﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HoyolabPlugin.Api.ApiModels
{
    public class HomeData
    {
        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("awards")]
        public List<FluffyAward> Awards { get; set; }

        [JsonProperty("resign")]
        public bool Resign { get; set; }

        [JsonProperty("now")]
        public string Now { get; set; }
    }

    public partial class FluffyAward
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cnt")]
        public long Cnt { get; set; }
    }
}
