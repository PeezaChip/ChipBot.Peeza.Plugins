﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class ResignInfoData
    {
        [JsonProperty("resign_cnt_daily")]
        public long ResignCntDaily { get; set; }

        [JsonProperty("resign_cnt_monthly")]
        public long ResignCntMonthly { get; set; }

        [JsonProperty("resign_limit_daily")]
        public long ResignLimitDaily { get; set; }

        [JsonProperty("resign_limit_monthly")]
        public long ResignLimitMonthly { get; set; }

        [JsonProperty("sign_cnt_missed")]
        public long SignCntMissed { get; set; }

        [JsonProperty("quality_cnt")]
        public long QualityCnt { get; set; }

        [JsonProperty("signed")]
        public bool Signed { get; set; }

        [JsonProperty("sign_cnt")]
        public long SignCnt { get; set; }

        [JsonProperty("cost")]
        public long Cost { get; set; }

        [JsonProperty("month_quality_cnt")]
        public long MonthQualityCnt { get; set; }
    }
}
