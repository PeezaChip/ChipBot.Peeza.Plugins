﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class SignPayload
    {
        [JsonProperty("act_id")]
        public string ActId { get; set; }
    }

    public class SignData
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("gt_result")]
        public GtResult GtResult { get; set; }
    }
}
