﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class CdKeyData
    {
        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("special_shipping")]
        public long SpecialShipping { get; set; }
    }
}
