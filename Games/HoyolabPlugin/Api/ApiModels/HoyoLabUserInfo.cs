﻿using HoyolabPlugin.Shared.Api.Models.Converters;
using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class HoyoLabUserInfoRoot
    {
        [JsonProperty("user_info")]
        public HoyoLabUserInfo UserInfo { get; set; }

        [JsonProperty("creator_info")]
        public CreatorInfo CreatorInfo { get; set; }

        [JsonProperty("paladin_info")]
        public PaladinInfo PaladinInfo { get; set; }

        [JsonProperty("collection_info")]
        public CollectionInfo CollectionInfo { get; set; }
    }

    public class CollectionInfo
    {
        [JsonProperty("num")]
        public long Num { get; set; }
    }

    public class CreatorInfo
    {
        [JsonProperty("show_beta")]
        public bool ShowBeta { get; set; }

        [JsonProperty("can_top")]
        public bool CanTop { get; set; }

        [JsonProperty("can_collect")]
        public bool CanCollect { get; set; }

        [JsonProperty("card_type")]
        public string CardType { get; set; }

        [JsonProperty("card_url")]
        public string CardUrl { get; set; }
    }

    public class PaladinInfo
    {
        [JsonProperty("path")]
        public string Path { get; set; }
    }

    public class HoyoLabUserInfo
    {
        [JsonProperty("uid")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long Uid { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("introduce")]
        public string Introduce { get; set; }

        [JsonProperty("avatar")]
        [JsonConverter(typeof(StringToLongConverter))]
        public long Avatar { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("community_info")]
        public CommunityInfo CommunityInfo { get; set; }

        [JsonProperty("level")]
        public HoyolabAccountLevel Level { get; set; }

        [JsonProperty("pendant")]
        public string Pendant { get; set; }

        [JsonProperty("community_email")]
        public string CommunityEmail { get; set; }

        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty("bg_url")]
        public string BgUrl { get; set; }

        [JsonProperty("pc_bg_url")]
        public string PcBgUrl { get; set; }

        [JsonProperty("nickname_times_left")]
        public string NicknameTimesLeft { get; set; }
    }

    public class CommunityInfo
    {
        [JsonProperty("is_realname")]
        public bool IsRealname { get; set; }

        [JsonProperty("agree_status")]
        public bool AgreeStatus { get; set; }

        [JsonProperty("silent_end_time")]
        public long SilentEndTime { get; set; }

        [JsonProperty("forbid_end_time")]
        public long ForbidEndTime { get; set; }

        [JsonProperty("info_upd_time")]
        public long InfoUpdTime { get; set; }

        [JsonProperty("privacy_invisible")]
        public PrivacyInvisible PrivacyInvisible { get; set; }

        [JsonProperty("notify_disable")]
        public NotifyDisable NotifyDisable { get; set; }

        [JsonProperty("silent_end_time_v2")]
        public string SilentEndTimeV2 { get; set; }

        [JsonProperty("forbid_end_time_v2")]
        public string ForbidEndTimeV2 { get; set; }

        [JsonProperty("info_upd_time_v2")]
        public string InfoUpdTimeV2 { get; set; }
    }

    public class NotifyDisable
    {
        [JsonProperty("reply")]
        public bool Reply { get; set; }

        [JsonProperty("upvote")]
        public bool Upvote { get; set; }

        [JsonProperty("follow")]
        public bool Follow { get; set; }

        [JsonProperty("system")]
        public bool System { get; set; }
    }

    public class PrivacyInvisible
    {
        [JsonProperty("post")]
        public bool Post { get; set; }

        [JsonProperty("collect")]
        public bool Collect { get; set; }

        [JsonProperty("watermark")]
        public bool Watermark { get; set; }

        [JsonProperty("gamerecord")]
        public bool Gamerecord { get; set; }

        [JsonProperty("follow")]
        public bool Follow { get; set; }

        [JsonProperty("follower")]
        public bool Follower { get; set; }

        [JsonProperty("recommend")]
        public Marketing Recommend { get; set; }

        [JsonProperty("marketing")]
        public Marketing Marketing { get; set; }
    }

    public class Marketing
    {
        [JsonProperty("is_opened")]
        public bool IsOpened { get; set; }

        [JsonProperty("hint_type")]
        public long HintType { get; set; }

        [JsonProperty("expire")]
        public string Expire { get; set; }

        [JsonProperty("is_hint")]
        public bool IsHint { get; set; }

        [JsonProperty("is_operation")]
        public bool IsOperation { get; set; }

        [JsonProperty("is_tip", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsTip { get; set; }
    }

    public class HoyolabAccountLevel
    {
        [JsonProperty("level")]
        public long LevelLevel { get; set; }

        [JsonProperty("exp")]
        public long Exp { get; set; }

        [JsonProperty("level_desc")]
        public string LevelDesc { get; set; }

        [JsonProperty("bg_color")]
        public string BgColor { get; set; }

        [JsonProperty("bg_image")]
        public string BgImage { get; set; }
    }
}
