﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace HoyolabPlugin.Api.ApiModels
{
    public class FullPost
    {
        [JsonProperty("post")]
        public UserPost Post { get; set; }
    }

    public class UserPost
    {
        [JsonProperty("post")]
        public Post Post { get; set; }

        //[JsonProperty("forum")]
        //public object Forum { get; set; }

        [JsonProperty("topics")]
        public List<Topic> Topics { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("self_operation")]
        public SelfOperation SelfOperation { get; set; }

        [JsonProperty("stat")]
        public PostStatistic Stat { get; set; }

        [JsonProperty("help_sys")]
        public HelpSys HelpSys { get; set; }

        //[JsonProperty("cover")]
        //public object Cover { get; set; }

        [JsonProperty("image_list")]
        public List<ImageList> ImageList { get; set; }

        [JsonProperty("is_official_master")]
        public bool IsOfficialMaster { get; set; }

        [JsonProperty("is_user_master")]
        public bool IsUserMaster { get; set; }

        [JsonProperty("hot_reply_exist")]
        public bool HotReplyExist { get; set; }

        [JsonProperty("vote_count")]
        public long VoteCount { get; set; }

        [JsonProperty("last_modify_time")]
        public long LastModifyTime { get; set; }

        //[JsonProperty("contribution")]
        //public object Contribution { get; set; }

        //[JsonProperty("classification")]
        //public object Classification { get; set; }

        //[JsonProperty("video")]
        //public object Video { get; set; }

        [JsonProperty("game")]
        public Game Game { get; set; }

        [JsonProperty("data_box")]
        public string DataBox { get; set; }

        [JsonProperty("is_top_icon")]
        public bool IsTopIcon { get; set; }

        [JsonProperty("tags")]
        public Tags Tags { get; set; }

        //[JsonProperty("hot_reply")]
        //public object HotReply { get; set; }

        [JsonProperty("collection")]
        public Collection Collection { get; set; }

        [JsonProperty("is_rich_text")]
        public bool IsRichText { get; set; }
    }

    public class Collection
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("current_index")]
        public long CurrentIndex { get; set; }

        [JsonProperty("post_num")]
        public long PostNum { get; set; }

        [JsonProperty("last_id")]
        public string LastId { get; set; }

        [JsonProperty("next_id")]
        public string NextId { get; set; }
    }

    public class Game
    {
        [JsonProperty("game_id")]
        public long GameId { get; set; }

        [JsonProperty("game_name")]
        public string GameName { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }
    }

    public class HelpSys
    {
        //[JsonProperty("top_up")]
        //public object TopUp { get; set; }
    }

    public class ImageList
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("spoiler")]
        public bool Spoiler { get; set; }

        [JsonProperty("cuts")]
        public List<ImageCut> Cuts { get; set; }
    }

    public class ImageCut
    {
        [JsonProperty("ratio")]
        public long Ratio { get; set; }

        [JsonProperty("lt_point")]
        public ImageCutPoint LtPoint { get; set; }

        [JsonProperty("rb_point")]
        public ImageCutPoint RbPoint { get; set; }
    }

    public class ImageCutPoint
    {
        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }
    }

    public class Post
    {
        [JsonProperty("game_id")]
        public long GameId { get; set; }

        [JsonProperty("post_id")]
        public string PostId { get; set; }

        [JsonProperty("f_forum_id")]
        public long ForumId { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("cover")]
        public string Cover { get; set; }

        [JsonProperty("view_type")]
        public long ViewType { get; set; }

        [JsonProperty("created_at")]
        public long CreatedAt { get; set; }

        //[JsonProperty("images")]
        //public List<object> Images { get; set; }

        [JsonProperty("post_status")]
        public PostStatus PostStatus { get; set; }

        [JsonProperty("topic_ids")]
        public List<long> TopicIds { get; set; }

        [JsonProperty("view_status")]
        public long ViewStatus { get; set; }

        [JsonProperty("max_floor")]
        public long MaxFloor { get; set; }

        [JsonProperty("is_original")]
        public long IsOriginal { get; set; }

        [JsonProperty("republish_authorization")]
        public long RepublishAuthorization { get; set; }

        [JsonProperty("reply_time")]
        public DateTimeOffset ReplyTime { get; set; }

        [JsonProperty("is_deleted")]
        public long IsDeleted { get; set; }

        [JsonProperty("is_interactive")]
        public bool IsInteractive { get; set; }

        [JsonProperty("structured_content")]
        public string StructuredContent { get; set; }

        //[JsonProperty("structured_content_rows")]
        //public List<object> StructuredContentRows { get; set; }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("official_type")]
        public long OfficialType { get; set; }

        //[JsonProperty("reply_forbid")]
        //public object ReplyForbid { get; set; }

        [JsonProperty("video")]
        public string Video { get; set; }

        [JsonProperty("contribution_id")]
        public string ContributionId { get; set; }

        [JsonProperty("event_start_date")]
        public string EventStartDate { get; set; }

        [JsonProperty("event_end_date")]
        public string EventEndDate { get; set; }

        [JsonProperty("classification_id")]
        public string ClassificationId { get; set; }

        [JsonProperty("is_audit")]
        public bool IsAudit { get; set; }

        [JsonProperty("is_multi_language")]
        public bool IsMultiLanguage { get; set; }

        [JsonProperty("origin_lang")]
        public string OriginLang { get; set; }

        [JsonProperty("sub_type")]
        public long SubType { get; set; }

        [JsonProperty("reprint_source")]
        public string ReprintSource { get; set; }

        [JsonProperty("can_edit")]
        public bool CanEdit { get; set; }

        [JsonProperty("last_modify_time")]
        public long LastModifyTime { get; set; }

        //[JsonProperty("multi_language_info")]
        //public object MultiLanguageInfo { get; set; }
    }

    public class PostStatus
    {
        [JsonProperty("is_top")]
        public bool IsTop { get; set; }

        [JsonProperty("is_good")]
        public bool IsGood { get; set; }

        [JsonProperty("is_official")]
        public bool IsOfficial { get; set; }
    }

    public class SelfOperation
    {
        [JsonProperty("attitude")]
        public long Attitude { get; set; }

        [JsonProperty("is_collected")]
        public bool IsCollected { get; set; }
    }

    public class PostStatistic
    {
        [JsonProperty("view_num")]
        public long ViewNum { get; set; }

        [JsonProperty("reply_num")]
        public long ReplyNum { get; set; }

        [JsonProperty("like_num")]
        public long LikeNum { get; set; }

        [JsonProperty("bookmark_num")]
        public long BookmarkNum { get; set; }

        [JsonProperty("share_num")]
        public long ShareNum { get; set; }

        [JsonProperty("view_num_unit")]
        public string ViewNumUnit { get; set; }

        [JsonProperty("reply_num_unit")]
        public string ReplyNumUnit { get; set; }

        [JsonProperty("like_num_unit")]
        public string LikeNumUnit { get; set; }

        [JsonProperty("bookmark_num_unit")]
        public string BookmarkNumUnit { get; set; }

        [JsonProperty("share_num_unit")]
        public string ShareNumUnit { get; set; }

        [JsonProperty("true_view_num")]
        public long TrueViewNum { get; set; }

        [JsonProperty("click_view_num")]
        public long ClickViewNum { get; set; }
    }

    public class Tags
    {
        [JsonProperty("is_user_top")]
        public bool IsUserTop { get; set; }

        [JsonProperty("is_qualified_post")]
        public bool IsQualifiedPost { get; set; }
    }

    public class Topic
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cover")]
        public string Cover { get; set; }

        [JsonProperty("is_top")]
        public bool IsTop { get; set; }

        [JsonProperty("is_good")]
        public bool IsGood { get; set; }

        [JsonProperty("is_interactive")]
        public bool IsInteractive { get; set; }

        [JsonProperty("game_id")]
        public long GameId { get; set; }
    }

    public class User
    {
        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("introduce")]
        public string Introduce { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("certification")]
        public Certification Certification { get; set; }

        [JsonProperty("level_exp")]
        public LevelExp LevelExp { get; set; }

        [JsonProperty("is_following")]
        public bool IsFollowing { get; set; }

        [JsonProperty("is_followed")]
        public bool IsFollowed { get; set; }

        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        //[JsonProperty("auth")]
        //public object Auth { get; set; }

        [JsonProperty("is_logoff")]
        public bool IsLogoff { get; set; }

        [JsonProperty("pendant")]
        public string Pendant { get; set; }

        [JsonProperty("was_following")]
        public bool WasFollowing { get; set; }

        [JsonProperty("post_num")]
        public long PostNum { get; set; }
    }

    public class Certification
    {
        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("icon_url")]
        public string IconUrl { get; set; }

        [JsonProperty("desc")]
        public string Desc { get; set; }
    }

    public class LevelExp
    {
        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("exp")]
        public long Exp { get; set; }
    }
}
