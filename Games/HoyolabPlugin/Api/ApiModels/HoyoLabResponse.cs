﻿using Newtonsoft.Json;

namespace HoyolabPlugin.Api.ApiModels
{
    public class HoyoLabResponse<T> where T : class
    {
        [JsonProperty("retcode")]
        public long Retcode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }
}