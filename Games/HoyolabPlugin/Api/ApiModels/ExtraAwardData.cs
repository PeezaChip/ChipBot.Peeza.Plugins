﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace HoyolabPlugin.Api.ApiModels
{
    public class ExtraAwardData
    {
        [JsonProperty("has_short_act")]
        public bool HasShortAct { get; set; }

        [JsonProperty("awards")]
        public List<PurpleAward> Awards { get; set; }

        [JsonProperty("start_timestamp")]
        public string StartTimestamp { get; set; }

        [JsonProperty("end_timestamp")]
        public string EndTimestamp { get; set; }

        [JsonProperty("total_cnt")]
        public long TotalCnt { get; set; }

        [JsonProperty("login")]
        public bool Login { get; set; }

        [JsonProperty("mc")]
        public MonthCard Mc { get; set; }
    }

    public class PurpleAward
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cnt")]
        public long Cnt { get; set; }

        [JsonProperty("sign_day")]
        public long SignDay { get; set; }

        [JsonProperty("highlight")]
        public bool Highlight { get; set; }
    }

    public class MonthCard
    {
        [JsonProperty("has_month_card")]
        public bool HasMonthCard { get; set; }

        [JsonProperty("start_time")]
        public DateTimeOffset StartTime { get; set; }

        [JsonProperty("open_time")]
        public DateTimeOffset OpenTime { get; set; }

        [JsonProperty("end_time")]
        public DateTimeOffset EndTime { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
