﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HoyolabPlugin.Api.ApiModels
{
    public class HoyoLabPagedResponse<T> where T : class
    {
        [JsonProperty("list")]
        public List<T> List { get; set; }

        [JsonProperty("is_last")]
        public bool IsLast { get; set; }

        [JsonProperty("next_offset")]
        public long NextOffset { get; set; }
    }
}
