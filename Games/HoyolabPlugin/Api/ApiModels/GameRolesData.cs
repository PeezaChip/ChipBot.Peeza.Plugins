﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HoyolabPlugin.Api.ApiModels
{
    public class GameRolesData
    {
        [JsonProperty("list")]
        public List<GameRoles> List { get; set; }
    }

    public class GameRoles
    {
        [JsonProperty("game_biz")]
        public string GameBiz { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("game_uid")]
        public string GameUid { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("is_chosen")]
        public bool IsChosen { get; set; }

        [JsonProperty("region_name")]
        public string RegionName { get; set; }

        [JsonProperty("is_official")]
        public bool IsOfficial { get; set; }
    }
}
