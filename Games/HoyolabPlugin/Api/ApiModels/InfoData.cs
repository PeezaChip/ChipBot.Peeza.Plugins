﻿using Newtonsoft.Json;
using System;

namespace HoyolabPlugin.Api.ApiModels
{
    public class InfoData
    {
        [JsonProperty("total_sign_day")]
        public int TotalSignDay { get; set; }

        [JsonProperty("today")]
        public DateTimeOffset Today { get; set; }

        [JsonProperty("is_sign")]
        public bool IsSign { get; set; }

        [JsonProperty("first_bind")]
        public bool FirstBind { get; set; }

        [JsonProperty("is_sub")]
        public bool IsSub { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("month_last_day")]
        public bool MonthLastDay { get; set; }
    }
}
