﻿using HoyolabPlugin.Api.ApiModels;
using RestEase;
using System.Threading.Tasks;

namespace HoyolabPlugin.Api
{
    [BasePath("account/binding/api")]
    public interface IHoyoverseAccountApi
    {
        public const string BaseAddress = "https://api-account-os.hoyoverse.com";

        [Header("Origin")]
        public string Origin { get; set; }

        [Header("Referer")]
        public string Referer { get; set; }

        [Header("Cookie")]
        public string Cookie { get; set; }

        [Header("User-Agent")]
        public string UserAgent { get; set; }

        [Query("lang")]
        public string Lang { get; set; }

        [Query("game_biz")]
        public string GameBiz { get; set; }

        [Get("getUserGameRolesByCookieToken")]
        public Task<HoyoLabResponse<GameRolesData>> GetUserGameRoles();
    }
}
