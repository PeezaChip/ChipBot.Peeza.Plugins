﻿using System.Collections.Generic;

namespace HoyolabPlugin.Models
{
    public class HoyolabConfig
    {
        public string Lang { get; set; } = "en-us";

        public string RpcAppVersion { get; set; } = "1.5.0";
        public string RpcClientType { get; set; } = "4";

        public ulong HoyolabChannel { get; set; } = 0;

        public string UserToParsePosts { get; set; } = "8009863";
        public int KeyTopic { get; set; } = 857;

        public string ResinEmoji = "<:gi_resin:>";
        public string RealmEmoji = "<:gi_realm:>";
        public string CommissionsEmoji = "<:gi_commissions:>";
        public string MoraEmoji = "<:gi_mora:>";
        public string TransformerEmoji = "<:gi_transformer:>";

        public Dictionary<string, HoyolabGameConfig> GameConfigs { get; set; }
    }
}
