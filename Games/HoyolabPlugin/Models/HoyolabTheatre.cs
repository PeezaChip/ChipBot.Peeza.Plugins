﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace HoyolabPlugin.Models
{
    [PrimaryKey(nameof(TheatreId), nameof(UserId), nameof(Server))]
    public class HoyolabTheatre
    {
        [Required]
        public int TheatreId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Server { get; set; }

        [Required]
        public string TheatreData { get; set; }

        public string CharacterData { get; set; }
    }
}
