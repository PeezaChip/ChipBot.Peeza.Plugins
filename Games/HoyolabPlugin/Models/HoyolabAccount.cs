﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HoyolabPlugin.Models
{
    public class HoyolabAccount
    {
        [Key]
        public int Uid { get; set; }

        [Required]
        public string Cookies { get; set; }

        [Required]
        public string DisplayName { get; set; }
        
        public string AvatarUrl { get; set; }

        [Required]
        public ulong UserId { get; set; }

        public virtual List<HoyolabGameAccount> GameAccounts { get; set; }
    }
}
