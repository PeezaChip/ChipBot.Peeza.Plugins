﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace HoyolabPlugin.Models
{
    [PrimaryKey(nameof(Uid), nameof(Game))]
    public class HoyolabGameAccount : HoyolabAccountSettings
    {
        public int Uid { get; set; }
        public string Game { get; set; }

        [Required]
        public string Region { get; set; }

        [Required]
        public string DisplayName { get; set; }

        public int HoyolabAccountId { get; set; }

        public string RecordData { get; set; }

        public virtual HoyolabAccount HoyolabAccount { get; set; }
    }
}
