﻿using Microsoft.EntityFrameworkCore;

namespace HoyolabPlugin.Models
{
    [PrimaryKey(nameof(Key), nameof(Game))]
    public class HoyolabKey
    {
        public string Key { get; set; }
        public bool IsActivated { get; set; }
        public string Game { get; set; }
    }
}
