﻿namespace HoyolabPlugin.Models
{
    public class HoyolabGameConfig
    {
        public string GameName { get; set; }
        public string GameBiz { get; set; }
        public string ActId { get; set; }

        public string Event { get; set; }

        public string HoyolabOrigin { get; set; }
        public string HoyoverseOrigin { get; set; }

        public string SignApi { get; set; }
    }
}
