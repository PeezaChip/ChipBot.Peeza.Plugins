﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace HoyolabPlugin.Models
{
    [PrimaryKey(nameof(AbyssId), nameof(UserId), nameof(Server))]
    public class HoyolabAbyss
    {
        [Required]
        public int AbyssId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Server { get; set; }

        [Required]
        public string AbyssData { get; set; }

        public string CharacterData { get; set; }
    }
}
