﻿namespace HoyolabPlugin.Models
{
    public class HoyolabAccountSettings
    {
        public bool DailySignIn { get; set; } = true;
        public bool ActivateKeys { get; set; } = true;
        public bool StoreAbyssData { get; set; } = false;
    }
}
