﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace HoyolabPlugin.Utility
{
    public static class DsSecretGenerator
    {
        private const string secret = "6cqshh5dhw73bzxn20oexa9k516chk7s";

        public static string GenerateDsSecret()
        {
            var time = DateTimeOffset.Now.ToUnixTimeSeconds();
            var rstr = Guid.NewGuid().ToString()[..6];

            var stringToHash = $"salt={secret}&t={time}&r={rstr}";
            var hash = MD5.HashData(Encoding.UTF8.GetBytes(stringToHash));
            return $"{time},{rstr},{BitConverter.ToString(hash).Replace("-", "").ToLower()}";
        }
    }
}
