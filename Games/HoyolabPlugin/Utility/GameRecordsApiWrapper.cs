﻿using Discord;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Models;
using System.Threading.Tasks;

namespace HoyolabPlugin.Utility
{
    public abstract class GameRecordsApiWrapper<TApi, TAbyss, TDailyNote, TRecordsIndex>
        where TRecordsIndex : class
        where TDailyNote : class
        where TAbyss : class
    {
        protected TApi Api { get; }
        protected HoyolabGameAccount Account { get; }
        protected HoyolabConfig Config { get; }

        protected GameRecordsApiWrapper(HoyolabConfig config, HoyolabGameAccount hoyolabGameAccount)
        {
            Account = hoyolabGameAccount;
            Config = config;

            Api = GetApi();
        }

        public abstract Task<HoyoLabResponse<TAbyss>> GetAbyss(bool current);

        public abstract Task<HoyoLabResponse<TDailyNote>> GetDailyNote();

        public abstract Task<Embed> GetDailyEmbed();

        public abstract Task<HoyoLabResponse<TRecordsIndex>> GetRecordsIndex();

        protected abstract TApi GetApi();
    }
}
