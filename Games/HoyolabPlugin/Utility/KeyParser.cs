﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using HoyolabPlugin.Data;
using HoyolabPlugin.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace HoyolabPlugin.Utility
{
    [Chipject]
    public partial class KeyParser
    {
        private readonly ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory;
        private readonly ILogger<KeyParser> log;

        public KeyParser(ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory, ILogger<KeyParser> log)
        {
            this.dbContextFactory = dbContextFactory;
            this.log = log;
        }

        [GeneratedRegex(@"[A-Z0-9]{12}")]
        private static partial Regex KeyRegex();

        public List<string> ParseKeys(string content, string game)
        {
            var matches = KeyRegex().Matches(content);
            if (!matches.Any()) return new();

            var keys = new List<string>();
            foreach (Match match in matches)
            {
                if (keys.Contains(match.Value)) continue;

                var isNumber = long.TryParse(match.Value, out var _);
                if (isNumber)
                {
                    log.LogInformation($"Key is a number, probably is not a correct key ({match.Value}), skipping.");
                    continue;
                }

                log.LogInformation($"Parsed key {match.Value}. Adding to database.");
                try
                {
                    using var dbContext = dbContextFactory.CreateDbContext();
                    if (dbContext.HoyolabKeys.FirstOrDefault(k => k.Key == match.Value && k.Game == game) != null)
                    {
                        log.LogInformation($"Key {match.Value} already exists in database, skipping.");
                        continue;
                    }

                    dbContext.HoyolabKeys.Add(new HoyolabKey() { Key = match.Value, Game = game });
                    keys.Add(match.Value);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    log.LogError(ex, $"Parsed key {match.Value} failed to add to database.");
                }
            }

            return keys;
        }
    }
}
