﻿using Discord;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Api.Extensions;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Humanizer;
using Humanizer.Localisation;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyolabPlugin.Utility
{
    public class GenshinGameRecordsWrapper : GameRecordsApiWrapper<IHoyolabGenshinRecordApi, SpiralAbyssData, DailyNoteData, RecordsIndexData>
    {
        public GenshinGameRecordsWrapper(HoyolabConfig config, HoyolabGameAccount hoyolabGameAccount)
            : base(config, hoyolabGameAccount) { }

        public override Task<HoyoLabResponse<SpiralAbyssData>> GetAbyss(bool current) => Api.GetSpiralAbyssData(Account.Region, Account.Uid.ToString(), current ? "1" : "2");
        public override Task<HoyoLabResponse<DailyNoteData>> GetDailyNote() => Api.GetDailyNoteData(Account.Region, Account.Uid.ToString());
        public override Task<HoyoLabResponse<RecordsIndexData>> GetRecordsIndex() => Api.GetRecordsIndexData(Account.Region, Account.Uid.ToString());
        public Task<HoyoLabResponse<ImaginariumTheaterDataRoot>> GetTheatre() => Api.GetImaginariumTheaterData(Account.Region, Account.Uid.ToString(), true);

        public Task<HoyoLabResponse<RecordsCharacterList>> GetCharacterList() => Api.GetCharacterList(new RecordsCharacterListRequestPayload() { Server = Account.Region, RoleId = Account.Uid.ToString() });
        public Task<HoyoLabResponse<RecordsCharacterDetail>> GetCharacterDetails(IEnumerable<long> characterIds) => Api.GetCharacterDetails(new RecordsCharacterDetailRequestPayload() { Server = Account.Region, RoleId = Account.Uid.ToString(), CharacterIds = characterIds.ToArray() });

        public async override Task<Embed> GetDailyEmbed()
        {
            var data = await GetDailyNote();
            var eb = new EmbedBuilder()
            .WithCurrentTimestamp()
                    .WithFooter($"{Account.DisplayName} - {Account.Uid}")
                    .WithColor(121512)

                    .WithAuthor("HoyoLab Notes", "https://act.hoyolab.com/ys/event/signin-sea-v3/images/paimon.792472e0.png");

            if (data.Retcode != 0)
            {
                eb.Description = "Paimon is sorry but something went wrong.";
                return eb.Build();
            }

            var daily = data.Data;

            var resinIsNotFull = daily.CurrentResin < daily.MaxResin;
            eb.AddField(Config.ResinEmoji, $"`{daily.CurrentResin}`/`{daily.MaxResin}`{(resinIsNotFull ? $"\nFull in {TimeSpan.FromSeconds(daily.ResinRecoveryTime).Humanize(maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute)}" : "")}", true);

            eb.AddField(Config.CommissionsEmoji, $"`{daily.FinishedTaskNum}`/`{daily.TotalTaskNum}`\n{(daily.IsExtraTaskRewardReceived ? "Rewards received." : "Please receive rewards.")}", true);

            var lastFinished = 0L;
            var finished = 0;
            foreach (var expedition in daily.Expeditions)
                if (expedition.RemainedTime <= 0)
                    finished++;
                else if (expedition.RemainedTime > lastFinished)
                    lastFinished = expedition.RemainedTime;
            var expeditionsDone = finished == daily.CurrentExpeditionNum;
            eb.AddField(Config.MoraEmoji, $"`{finished}`/`{daily.MaxExpeditionNum}`\n{(expeditionsDone ? "Expeditions done." : $"Done in {TimeSpan.FromSeconds(lastFinished).Humanize(maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute)}")}", true);

            var homeCoinIsNotFull = daily.CurrentHomeCoin < daily.MaxHomeCoin;
            eb.AddField(Config.RealmEmoji, $"`{daily.CurrentHomeCoin}`/`{daily.MaxHomeCoin}`\n{(homeCoinIsNotFull ? $"Full in {TimeSpan.FromSeconds(daily.HomeCoinRecoveryTime).Humanize(maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute)}" : "")}", true);

            if (daily.Transformer != null && daily.Transformer.Obtained)
            {
                var readyIn = TimeSpan.FromDays(daily.Transformer.RecoveryTime.Day)
                    + TimeSpan.FromHours(daily.Transformer.RecoveryTime.Hour)
                    + TimeSpan.FromMinutes(daily.Transformer.RecoveryTime.Minute)
                    + TimeSpan.FromSeconds(daily.Transformer.RecoveryTime.Second);

                eb.AddField(Config.TransformerEmoji, daily.Transformer.RecoveryTime.Reached ? "Ready." : $"Ready in {readyIn.Humanize(maxUnit: TimeUnit.Day, minUnit: TimeUnit.Minute)}", true);
            }

            return eb.Build();
        }

        protected override IHoyolabGenshinRecordApi GetApi()
        {
            var api = new RestClient(IHoyolabGenshinRecordApi.BaseAddress).For<IHoyolabGenshinRecordApi>();

            api.AppVersion = Config.RpcAppVersion;
            api.ClientType = Config.RpcClientType;
            api.Lang = Config.Lang;

            api.Cookie = Account.HoyolabAccount.Cookies;

            return api;
        }
    }
}