﻿using Discord;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Api.Extensions;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Humanizer;
using Humanizer.Localisation;
using RestEase;
using System;
using System.Threading.Tasks;

namespace HoyolabPlugin.Utility
{
    public class HsrGameRecordsWrapper : GameRecordsApiWrapper<IHoyolabHsrRecordApi, ForgottenHallData, DailyNoteHsrData, RecordsIndexHsrData>
    {
        public HsrGameRecordsWrapper(HoyolabConfig config, HoyolabGameAccount hoyolabGameAccount)
            : base(config, hoyolabGameAccount) { }

        public override Task<HoyoLabResponse<ForgottenHallData>> GetAbyss(bool current) => Api.GetSpiralAbyssDataHsr(Account.Region, Account.Uid.ToString(), current ? "1" : "2");
        public override Task<HoyoLabResponse<DailyNoteHsrData>> GetDailyNote() => Api.GetDailyNoteDataHsr(Account.Region, Account.Uid.ToString());
        public override Task<HoyoLabResponse<RecordsIndexHsrData>> GetRecordsIndex() => Api.GetRecordsIndexDataHsr(Account.Region, Account.Uid.ToString());

        public async override Task<Embed> GetDailyEmbed()
        {
            var data = await GetDailyNote();

            var eb = new EmbedBuilder()
                    .WithCurrentTimestamp()
                    .WithFooter($"{Account.DisplayName} - {Account.Uid}")
                    .WithColor(121512)

                    .WithAuthor("HoyoLab Notes", "https://act.hoyoverse.com/darkmatter/hkrpg/prod_gf_cn/item_icon_79bf3p/f753cd6e6274057e995591d2018bbc75.png");

            if (data.Retcode != 0)
            {
                eb.Description = "Silver Wolf is sorry but something went wrong.";
                return eb.Build();
            }

            var daily = data.Data;

            var resinIsNotFull = daily.CurrentStamina < daily.MaxStamina;
            eb.AddField(Config.ResinEmoji, $"`{daily.CurrentStamina}`/`{daily.MaxStamina}`{(resinIsNotFull ? $"\nFull in {TimeSpan.FromSeconds(daily.StaminaRecoverTime).Humanize(maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute)}" : "")}", true);

            var lastFinished = 0L;
            var finished = 0;
            foreach (var expedition in daily.Expeditions)
                if (expedition.RemainedTime <= 0)
                    finished++;
                else if (expedition.RemainedTime > lastFinished)
                    lastFinished = expedition.RemainedTime;
            var expeditionsDone = finished == daily.AcceptedEpeditionNum;
            eb.AddField(Config.MoraEmoji, $"`{finished}`/`{daily.TotalExpeditionNum}`\n{(expeditionsDone ? "Expeditions done." : $"Done in {TimeSpan.FromSeconds(lastFinished).Humanize(maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute)}")}", true);

            return eb.Build();
        }

        protected override IHoyolabHsrRecordApi GetApi()
        {
            var api = new RestClient(IHoyolabHsrRecordApi.BaseAddress).For<IHoyolabHsrRecordApi>();

            api.AppVersion = Config.RpcAppVersion;
            api.ClientType = Config.RpcClientType;
            api.Lang = Config.Lang;

            api.Cookie = Account.HoyolabAccount.Cookies;

            return api;
        }
    }
}