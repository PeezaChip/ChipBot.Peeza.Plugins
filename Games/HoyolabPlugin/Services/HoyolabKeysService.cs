﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Data;
using HoyolabPlugin.Models;
using Microsoft.Extensions.Logging;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class HoyolabKeysService : BackgroundService<HoyolabKeysService>
    {
        private readonly SettingsService<HoyolabConfig> config;
        private readonly HoyolabAccountService accountService;
        private readonly DiscordSocketClient discord;
        private readonly ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory;

        private CancellationTokenSource cancellationToken;
        private Task registerKeysTask;

        public override bool IsWorking => registerKeysTask != null && !registerKeysTask.IsCompleted;

        public HoyolabKeysService(SettingsService<HoyolabConfig> config, HoyolabAccountService accountService, DiscordSocketClient discord, ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory, ILogger<HoyolabKeysService> log) : base(log)
        {
            this.config = config;
            this.accountService = accountService;
            this.discord = discord;
            this.dbContextFactory = dbContextFactory;
        }

        public override Task StartAsync()
        {
            cancellationToken = new CancellationTokenSource();
            registerKeysTask = RegisterKeys(cancellationToken.Token);
            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            cancellationToken.Cancel();
            cancellationToken = null;
            registerKeysTask = null;
            return Task.CompletedTask;
        }

        private async Task RegisterKeys(CancellationToken token)
        {
            logger.LogInformation("Starting register task.");
            while (!token.IsCancellationRequested)
            {
                try
                {
                    if (token.IsCancellationRequested) break;

                    using var dbContext = dbContextFactory.CreateDbContext();
                    var keysToRegister = dbContext.HoyolabKeys.Where(k => !k.IsActivated);
                    if (!keysToRegister.Any())
                    {
                        logger.LogDebug($"Don't have any keys to register.");
                        await Task.Delay(TimeSpan.FromMinutes(5), token);
                        continue;
                    }

                    var key = keysToRegister.First();
                    logger.LogDebug($"Processing key {key.Key}");

                    var accounts = accountService.GetAllAccounts();
                    foreach (var group in accounts.SelectMany(a => a.GameAccounts).GroupBy(a => a.HoyolabAccount.UserId))
                    {
                        var embeds = new List<EmbedBuilder>();
                        foreach (var account in group)
                        {
                            if (token.IsCancellationRequested) break;
                            if (!account.ActivateKeys) continue;
                            if (account.Game != key.Game) continue;

                            try
                            {
                                var response = await ActivateKeyOnAccount(key, account);
                                embeds.Add(BuildUserResult(key.Key, account, response));
                            }
                            catch (Exception ex)
                            {
                                logger.LogError(ex, $"Couldn't activate user's ({account.HoyolabAccount.UserId}) account {account.Uid} key {key}");
                            }
                        }
                        await SendUserResult(group.Key, embeds);

                        await Task.Delay(TimeSpan.FromSeconds(10), token);
                    }

                    logger.LogDebug($"Marking key {key.Key} as activated");
                    key.IsActivated = true;
                    dbContext.HoyolabKeys.Update(key);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Register exception");
                }
            }

            logger.LogInformation($"Register clean up");
            cancellationToken = null;
            registerKeysTask = null;
        }

        private async Task<HoyoLabResponse<CdKeyData>> ActivateKeyOnAccount(HoyolabKey key, HoyolabGameAccount account)
        {
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) account {account.Uid}");
            var api = GetCdKeyApi(account);
            return await api.ActivateCdKey(account.Uid.ToString(), key.Key, account.Region);
        }

        private IHoyoverseCdKeyApi GetCdKeyApi(HoyolabGameAccount account)
        {
            var api = new RestClient(IHoyoverseCdKeyApi.BaseAddress).For<IHoyoverseCdKeyApi>();
            var cfg = config.Settings;
            var gameConfig = cfg.GameConfigs[account.Game];

            api.Lang = cfg.Lang;
            api.Origin = gameConfig.HoyoverseOrigin;
            api.Referer = gameConfig.HoyoverseOrigin;
            api.GameBiz = gameConfig.GameBiz;

            api.Cookie = account.HoyolabAccount.Cookies;

            return api;
        }

        private async Task SendUserResult(ulong userId, List<EmbedBuilder> embeds)
        {
            if (embeds == null || !embeds.Any()) return;
            var ch = await discord.GetUser(userId).CreateDMChannelAsync();
            await ch.SendMessageAsync(embeds: embeds.Select(e => e.Build()).ToArray());
        }

        private EmbedBuilder BuildUserResult(string key, HoyolabGameAccount account, HoyoLabResponse<CdKeyData> result)
        {
            logger.LogInformation($"Creating user ({account.HoyolabAccount.UserId}) cd key result for account {account.Uid}");
            var embed = result.Retcode == 0 ? GetEmbedFromSuccess(key, account, result.Data.Message) : GetEmbedFromError(key, account, result.Message);
            return embed;
        }

        private static EmbedBuilder GetEmbedFromError(string key, HoyolabGameAccount account, string errorMessage)
        {
            return GetEmbedBase(account)
                .WithTitle($"There was an error activating key {key}")
                .WithDescription(errorMessage);
        }

        private static EmbedBuilder GetEmbedFromSuccess(string key, HoyolabGameAccount account, string message)
        {
            return GetEmbedBase(account)
                .WithTitle($"Activated key {key}")
                .WithDescription(message);
        }

        private static EmbedBuilder GetEmbedBase(HoyolabGameAccount account)
        {
            return new EmbedBuilder()
                .WithCurrentTimestamp()
                .WithFooter($"{account.DisplayName} - {account.Uid}")
                .WithColor(121512)

                .WithThumbnailUrl("https://webstatic.hoyoverse.com/upload/event/2021/12/29/eaa64d81ae6a7318a6afb91cbffd83a4_118924438105936743.png")
                .WithAuthor("HoyoLab Redeem Code", "https://act.hoyolab.com/ys/event/signin-sea-v3/images/paimon.792472e0.png");
        }
    }
}
