﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using HoyolabPlugin.Api;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using HoyolabPlugin.Utility;
using Microsoft.Extensions.Logging;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class HsrGameRecordsService : HoyolabGameRecordsService<IHoyolabHsrRecordApi, ForgottenHallData, DailyNoteHsrData, RecordsIndexHsrData>
    {
        public HsrGameRecordsService(ILogger<HsrGameRecordsService> log, SettingsService<HoyolabConfig> config)
            : base(log, config) { }

        protected override HsrGameRecordsWrapper GetApi(HoyolabGameAccount account)
            => new(config.Settings, account);
    }
}