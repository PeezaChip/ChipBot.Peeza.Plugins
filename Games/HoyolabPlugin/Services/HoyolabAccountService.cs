﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using Discord;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Data;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class HoyolabAccountService : ChipService<HoyolabAccountService>
    {
        private readonly ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory;

        public HoyolabAccountService(ILogger<HoyolabAccountService> log, ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory) : base(log)
            => this.dbContextFactory = dbContextFactory;

        public void AddAccount(IUser user, string cookies, string game, GameRoles gameRoles, HoyoLabUserInfoRoot userInfo)
        {
            AddAccount(new HoyolabAccount()
            {
                UserId = user.Id,
                AvatarUrl = userInfo.UserInfo.AvatarUrl,
                DisplayName = userInfo.UserInfo.Nickname,
                Uid = (int)userInfo.UserInfo.Uid,
                Cookies = cookies,
            });

            AddGameAccount(new HoyolabGameAccount()
            {
                Game = game,
                Region = gameRoles.Region,
                DisplayName = gameRoles.Nickname,
                Uid = int.Parse(gameRoles.GameUid),
                HoyolabAccountId = (int)userInfo.UserInfo.Uid
            });
        }

        public void AddAccount(HoyolabAccount hoyolabAccount)
        {
            logger.LogInformation($"Adding account for {hoyolabAccount.UserId}");
            using var dbContext = dbContextFactory.CreateDbContext();
            var current = dbContext.HoyolabAccounts.Find(hoyolabAccount.Uid);
            current.AvatarUrl = hoyolabAccount.AvatarUrl;
            current.DisplayName = hoyolabAccount.DisplayName;
            current.Cookies = hoyolabAccount.Cookies;
            dbContext.Update(current);
            dbContext.SaveChanges();
        }

        public void AddGameAccount(HoyolabGameAccount account)
        {
            logger.LogInformation($"Adding account for {account.Uid} {account.Game}");
            using var dbContext = dbContextFactory.CreateDbContext();
            var current = dbContext.HoyolabGameAccounts.Add(account);
            dbContext.SaveChanges();
        }

        public void UpdateGameAccount(HoyolabGameAccount account)
        {
            logger.LogInformation($"Updating account for {account.Uid} {account.Game}");
            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.HoyolabGameAccounts.Update(account);
            dbContext.SaveChanges();
        }

        public void RemoveGameAccount(IUser user, GameAccountKey key) => RemoveGameAccount(user.Id, key.Game, int.Parse(key.UId));
        public void RemoveGameAccount(IUser user, string game, int uid) => RemoveGameAccount(user.Id, game, uid);
        public void RemoveGameAccount(ulong user, string game, int uid)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var account = dbContext.HoyolabGameAccounts.Where(g => g.Game == game && g.Uid == uid).FirstOrDefault();
            if (account == null) return;
            if (account.HoyolabAccount.UserId == user)
            {
                logger.LogInformation($"Removing account with uid {uid} {game}");
                dbContext.HoyolabGameAccounts.Remove(account);
            }
        }

        public IEnumerable<HoyolabAccount> GetAccounts(IUser user) => GetAccounts(user.Id);
        public IEnumerable<HoyolabAccount> GetAccounts(ulong user)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabAccounts.Where(a => a.UserId == user).ToList();
        }

        public Dictionary<ulong, List<HoyolabAccount>> GetAllAccountsDictionary()
            => GetAllAccounts().GroupBy(a => a.UserId).ToDictionary(g => g.Key, g => g.ToList());

        public List<HoyolabAccount> GetAllAccounts()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabAccounts.ToList();
        }

        public HoyolabGameAccount GetGameAccount(IUser user, GameAccountKey key) => GetGameAccount(user.Id, key);
        public HoyolabGameAccount GetGameAccount(IUser user, string game, int uid) => GetGameAccount(user.Id, game, uid);
        public HoyolabGameAccount GetGameAccount(ulong user, GameAccountKey key) => GetGameAccount(user, key.Game, int.Parse(key.UId));
        public HoyolabGameAccount GetGameAccount(ulong user, string game, int uid)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var account = dbContext.HoyolabGameAccounts.Where(g => g.Game == game && g.Uid == uid).FirstOrDefault();
            return account == null || account.HoyolabAccount.UserId != user ? null : account;
        }
    }
}
