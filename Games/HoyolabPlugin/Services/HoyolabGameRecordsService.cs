﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Services.Utils;
using Discord;
using HoyolabPlugin.Models;
using HoyolabPlugin.Utility;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    public abstract class HoyolabGameRecordsService<TApi, TAbyss, TDailyNote, TRecordsIndex> : ChipService<HoyolabGameRecordsService<TApi, TAbyss, TDailyNote, TRecordsIndex>>
        where TRecordsIndex : class
        where TDailyNote : class
        where TAbyss : class
    {
        private readonly GameRecordsApiWrapper<TApi, TAbyss, TDailyNote, TRecordsIndex> api;

        protected readonly SettingsService<HoyolabConfig> config;

        public HoyolabGameRecordsService(ILogger<HoyolabGameRecordsService<TApi, TAbyss, TDailyNote, TRecordsIndex>> log, SettingsService<HoyolabConfig> config)
            : base(log) => this.config = config;

        public async Task<TAbyss> GetAbyss(HoyolabGameAccount account, bool current) => (await GetApi(account).GetAbyss(current)).Data;

        public async Task<TDailyNote> GetDailyNote(HoyolabGameAccount account) => (await GetApi(account).GetDailyNote()).Data;

        public Task<Embed> GetDailyEmbed(HoyolabGameAccount account) => GetApi(account).GetDailyEmbed();

        public async Task<TRecordsIndex> GetRecordsIndex(HoyolabGameAccount account) => (await GetApi(account).GetRecordsIndex()).Data;

        protected abstract GameRecordsApiWrapper<TApi, TAbyss, TDailyNote, TRecordsIndex> GetApi(HoyolabGameAccount account);
    }
}