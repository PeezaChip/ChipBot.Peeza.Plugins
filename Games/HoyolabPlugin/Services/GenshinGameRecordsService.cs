﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using HoyolabPlugin.Api;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using HoyolabPlugin.Utility;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class GenshinGameRecordsService : HoyolabGameRecordsService<IHoyolabGenshinRecordApi, SpiralAbyssData, DailyNoteData, RecordsIndexData>
    {
        public GenshinGameRecordsService(ILogger<GenshinGameRecordsService> log, SettingsService<HoyolabConfig> config)
            : base(log, config) { }

        public async Task<ImaginariumTheaterDataRoot> GetTheatre(HoyolabGameAccount account) => (await GetApi(account).GetTheatre()).Data;
        public async Task<RecordsCharacterList> GetCharacterList(HoyolabGameAccount account) => (await GetApi(account).GetCharacterList()).Data;
        public async Task<RecordsCharacterDetail> GetCharacterDetail(HoyolabGameAccount account, IEnumerable<long> characterIds) => (await GetApi(account).GetCharacterDetails(characterIds)).Data;

        protected override GenshinGameRecordsWrapper GetApi(HoyolabGameAccount account)
            => new(config.Settings, account);
    }
}