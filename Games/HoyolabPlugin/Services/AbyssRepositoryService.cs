﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using HoyolabPlugin.Data;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class AbyssRepositoryService : ChipService<AbyssRepositoryService>
    {
        private readonly ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory;

        public AbyssRepositoryService(ILogger<AbyssRepositoryService> log, ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory) : base(log)
            => this.dbContextFactory = dbContextFactory;

        public HoyolabAbyss GetById(int abyssId, int uid, string server)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabAbyss.Find(abyssId, uid, server);
        }

        public List<HoyolabAbyss> GetAll(int uid, string server)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabAbyss.Where(a => a.UserId == uid && a.Server == server).ToList();
        }

        public void SaveAbyssData(Dictionary<long, object> data, RecordsCharacterDetail characterData, HoyolabGameAccount account)
        {
            if (data == null || !data.Any()) return;

            using var dbContext = dbContextFactory.CreateDbContext();
            foreach (var abyss in data)
            {
                var json = new HoyolabAbyss()
                {
                    AbyssId = (int)abyss.Key,
                    UserId = account.Uid,
                    Server = account.Region,
                    AbyssData = JsonConvert.SerializeObject(abyss.Value),
                    CharacterData = JsonConvert.SerializeObject(characterData)
                };

                if (dbContext.HoyolabAbyss.Where(a => a.AbyssId == json.AbyssId).Any())
                {
                    dbContext.HoyolabAbyss.Update(json);
                }
                else
                {
                    dbContext.HoyolabAbyss.Add(json);
                }
            }
            dbContext.SaveChanges();
        }
    }
}
