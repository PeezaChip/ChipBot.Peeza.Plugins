﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Interfaces;
using HoyolabPlugin.Models;
using HoyolabPlugin.Utility;
using Microsoft.Extensions.Logging;
using RestEase;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services.RedemptionCodeSources
{
    [Chipject(ChipjectType.Singleton, typeof(IRedemptionCodeSource))]
    public class GenshinRedemptionCodeSource : IRedemptionCodeSource
    {
        private const string LastPostIdKey = "hoyolab_key_last_post_id";

        private readonly ILogger<GenshinRedemptionCodeSource> _log;
        private readonly DiscordSocketClient _discord;
        private readonly SettingsService<HoyolabConfig> _config;
        private readonly ChipBotDbContextFactory _chipDbContextFactory;
        private readonly KeyParser _keyParser;
        private readonly IHoyolabApi _api;

        public GenshinRedemptionCodeSource(ILogger<GenshinRedemptionCodeSource> log, SettingsService<HoyolabConfig> config, DiscordSocketClient discord, ChipBotDbContextFactory chipDbContextFactory, KeyParser keyParser)
        {
            _log = log;

            _discord = discord;
            _config = config;
            _chipDbContextFactory = chipDbContextFactory;
            _keyParser = keyParser;

            _api = new RestClient(IHoyolabApi.BaseAddress).For<IHoyolabApi>();
        }

        public async Task GetCodes()
            => await GetUserPosts();

        private async Task GetUserPosts()
        {
            var config = _config.Settings;

            _log.LogInformation("Getting user posts to parse keys");
            var response = (await _api.GetUserPosts(config.UserToParsePosts));
            if (response.Retcode != 0)
            {
                _log.LogWarning($"Couldn't load user post to parse keys from: {response.Message} ({response.Retcode})");
                return;
            }
            var posts = response.Data.List;

            using var dbContext = _chipDbContextFactory.CreateDbContext();

            var lastReadPostId = dbContext.Dictionary.Find(LastPostIdKey) ?? new() { Key = LastPostIdKey, Value = "0" };

            foreach (var post in posts)
            {
                if (post.Post.PostId == lastReadPostId.Value) break;
                _log.LogInformation($"Parsing post {post.Post.PostId}");

                try
                {
                    if (!post.Post.TopicIds.Contains(config.KeyTopic)) continue;

                    _log.LogInformation($"Loading full post {post.Post.PostId}");
                    var fullPost = await _api.GetPostFull(post.Post.PostId);
                    if (fullPost.Retcode != 0)
                    {
                        _log.LogWarning($"Couldn't load post with keys topic: {fullPost.Message} ({fullPost.Retcode})");
                        continue;
                    }

                    _log.LogInformation($"Parsing keys");
                    var keys = _keyParser.ParseKeys(fullPost.Data.Post.Post.Content, "hk4e_global");

                    if (!keys.Any()) continue;
                    if (config.HoyolabChannel <= 0) continue;

                    _log.LogInformation($"Sending keys embed");
                    var eb = GetEmbedBase(fullPost.Data.Post.User);
                    eb.WithDescription($"Found new keys for {config.GameConfigs["hk4e_global"].GameName}:\n{string.Join("\n", keys.Select(k => $"[{k}](https://genshin.hoyoverse.com/en/gift?code={k})"))}");
                    var embed = eb.Build();

                    try
                    {
                        var ch = (ITextChannel)_discord.GetChannel(config.HoyolabChannel);
                        await ch.SendMessageAsync(embed: embed);
                    }
                    catch (Exception ex)
                    {
                        _log.LogError(ex, $"Couldn't send keys embed");
                    }

                    await Task.Delay(2500);
                }
                catch (Exception ex)
                {
                    _log.LogError(ex, $"Couldn't parse post {post.Post.PostId}");
                }
            }

            _log.LogInformation($"Updating last post");
            lastReadPostId.Value = posts.First(p => !p.Tags.IsUserTop).Post.PostId;
            dbContext.Dictionary.AddOrUpdate(lastReadPostId, LastPostIdKey);
        }

        private static EmbedBuilder GetEmbedBase(User user)
        {
            return new EmbedBuilder()
                .WithCurrentTimestamp()
                .WithColor(121512)

                .WithTitle("New Keys for Genshin Impact")

                .WithThumbnailUrl($"https://www.hoyolab.com/accountCenter?id={user.Uid}")
                .WithAuthor(user.Nickname, user.AvatarUrl.ToString(), $"https://www.hoyolab.com/accountCenter?id={user.Uid}");
        }
    }
}
