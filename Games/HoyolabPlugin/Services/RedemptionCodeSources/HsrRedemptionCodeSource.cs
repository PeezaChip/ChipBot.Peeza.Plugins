﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using HoyolabPlugin.Interfaces;
using HoyolabPlugin.Models;
using HoyolabPlugin.Utility;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services.RedemptionCodeSources
{
    [Chipject(ChipjectType.Singleton, typeof(IRedemptionCodeSource))]
    public class HsrRedemptionCodeSource : IRedemptionCodeSource
    {
        private readonly ILogger<HsrRedemptionCodeSource> _log;
        private readonly DiscordSocketClient _discord;
        private readonly SettingsService<HoyolabConfig> _config;
        private readonly KeyParser _keyParser;

        private readonly HttpClient httpClient;

        public HsrRedemptionCodeSource(ILogger<HsrRedemptionCodeSource> log, SettingsService<HoyolabConfig> config, DiscordSocketClient discord, KeyParser keyParser)
        {
            _log = log;

            _discord = discord;
            _config = config;
            _keyParser = keyParser;

            httpClient = new HttpClient();
        }

        public async Task GetCodes()
            => await GetCodesFromPrydwen();

        private async Task GetCodesFromPrydwen()
        {
            var config = _config.Settings;

            _log.LogInformation("Loading page");
            var pageSource = await httpClient.GetStringAsync("https://www.prydwen.gg/star-rail/");

            var doc = new HtmlDocument();
            doc.LoadHtml(pageSource);

            _log.LogInformation("Finding div");
            var rootNode = doc.DocumentNode.SelectSingleNode("//body//div[@class=\"codes\"]");

            _log.LogInformation("Parsing keys");
            var keys = _keyParser.ParseKeys(rootNode.InnerText, "hkrpg_global");

            if (!keys.Any()) return;
            if (config.HoyolabChannel <= 0) return;

            _log.LogInformation("Sending keys embed");
            var eb = GetEmbedBase();
            eb.WithDescription($"Found new keys for {config.GameConfigs["hkrpg_global"].GameName}:\n{string.Join("\n", keys.Select(k => $"[{k}](https://hsr.hoyoverse.com/gift?code={k})"))}");
            var embed = eb.Build();

            try
            {
                var ch = (ITextChannel)_discord.GetChannel(config.HoyolabChannel);
                await ch.SendMessageAsync(embed: embed);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Couldn't send keys embed");
            }
        }

        private static EmbedBuilder GetEmbedBase()
        {
            return new EmbedBuilder()
                .WithCurrentTimestamp()
                .WithColor(121512)

                .WithTitle("New Keys")

                .WithThumbnailUrl("https://www.prydwen.gg/static/e5cca805ee22a6a5327c633bbab70f48/c5628/prydwen_logo_small.webp")
                .WithAuthor("PRYDWEN.GG", "https://www.prydwen.gg/static/e5cca805ee22a6a5327c633bbab70f48/c5628/prydwen_logo_small.webp", $"https://www.prydwen.gg/star-rail/");
        }
    }
}
