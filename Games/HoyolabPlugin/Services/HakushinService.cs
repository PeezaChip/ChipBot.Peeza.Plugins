﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using ChipBot.Core.Services.Utils;
using HoyolabPlugin.Api;
using HoyolabPlugin.Shared.Api.Models.Hakushin;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestEase;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Chipject]
    [Notify(NoEarlierThan = 1, RepeatIntervalSeconds = 60 * 60 * 24 * 7)] // 1 week
    public class HakushinService : ChipService<HakushinService>, INotifyService
    {
        private readonly IHakushinApi hakushinApi;
        private readonly ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory;

        private HakushinData data;

        public HakushinService(ILogger<HakushinService> log, SettingsService<NetworkConfig> networkConfig, ChipBotDbContextFactory<ChipBotDbContext> chipBotDbContextFactory) : base(log)
        {
            hakushinApi = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri("https://api.hakush.in")
                })
            {
                JsonSerializerSettings = HakushinConverter.Settings
            }
            .For<IHakushinApi>();

            hakushinApi.UserAgent = networkConfig.Settings.UserAgent;

            this.chipBotDbContextFactory = chipBotDbContextFactory;

            LoadData();
        }

        private bool isDownloading = false;
        public async Task Notify()
        {
            if (isDownloading) return;
            isDownloading = true;

            try
            {
                logger.LogInformation("Downloading hakusin data");
                var newData = new HakushinData();
                var startTime = DateTime.Now;

                newData.Characters = await Download("characters", hakushinApi.GetCharacters);
                newData.CharactersFull = await DownloadCollection(newData.Characters, "character", (c) => c.En, hakushinApi.GetCharacter);

                newData.Weapons = await Download("weapons", hakushinApi.GetWeapons);
                newData.WeaponsFull = await DownloadCollection(newData.Weapons, "weapon", (c) => c.En, hakushinApi.GetWeapon);

                newData.Items = await Download("items", hakushinApi.GetItems);
                // we probably won't need them
                // data.ItemsFull = await DownloadCollection(data.Items, "item", (c) => c.Name, hakushinApi.GetItem);

                logger.LogInformation($"Download done in {DateTime.Now - startTime}");
                data = newData;
                SaveData();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't download hakushin data");
            }
            finally
            {
                isDownloading = false;
            }
        }

        public HakushinData GetData() => data;

        private async Task<Dictionary<string, TFull>> DownloadCollection<T, TFull>(Dictionary<string, T> collection, string name, Func<T, string> nameExpression, Func<string, Task<TFull>> download)
        {
            var result = new Dictionary<string, TFull>();
            var i = 1;
            var length = collection.Count;
            foreach (var (id, item) in collection)
            {
                logger.LogDebug($"Downloading {name} ({i++}/{length}) {id} - {nameExpression(item)}");
                result.Add(id, await Download($"{name}-{id}", () => download(id)));
            }
            return result;
        }

        private async Task<T> Download<T>(string name, Func<Task<T>> download)
        {
            logger.LogDebug($"Downloading {name}");
            try
            {
                return await download();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error whlie downloading {name}", ex);
            }
        }

        private void SaveData()
        {
            try
            {
                if (data == null) return;

                using var dbContext = chipBotDbContextFactory.CreateDbContext();
                var row = dbContext.Dictionary.Find(db_dictionary_key);
                var value = JsonConvert.SerializeObject(data, HakushinConverter.Settings);

                if (row != null)
                {
                    row.Value = value;
                    dbContext.Dictionary.Update(row);
                }
                else
                {
                    dbContext.Dictionary.Add(new ChipKeyValue() { Key = db_dictionary_key, Value = value });
                }

                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't save hakushin data");
            }
        }

        private void LoadData()
        {
            try
            {
                if (data != null) return;

                using var dbContext = chipBotDbContextFactory.CreateDbContext();
                var row = dbContext.Dictionary.Find(db_dictionary_key);

                if (row == null)
                {
                    _ = Notify();
                }
                else
                {
                    data = JsonConvert.DeserializeObject<HakushinData>(row.Value, HakushinConverter.Settings);
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Couldn't load hakushin data");
            }
        }

        private readonly string db_dictionary_key = "hakushin";
    }
}
