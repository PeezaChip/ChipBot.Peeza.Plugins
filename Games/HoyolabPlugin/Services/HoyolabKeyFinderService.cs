﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using HoyolabPlugin.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Chipject(ChipjectType.Transient)]
    [Notify(NoEarlierThan = 0, RepeatIntervalSeconds = 60 * 60)] // 1hr
    public class HoyolabKeyFinderService : ChipService<HoyolabKeyFinderService>, INotifyService
    {
        private readonly IEnumerable<IRedemptionCodeSource> redemptionCodeSources;

        public HoyolabKeyFinderService(ILogger<HoyolabKeyFinderService> log, IEnumerable<IRedemptionCodeSource> redemptionCodeSources) : base(log)
            => this.redemptionCodeSources = redemptionCodeSources;

        public async Task Notify()
        {
            logger.LogInformation("Starting to find keys");
            foreach (var source in redemptionCodeSources)
            {
                logger.LogInformation($"Invoking {source.GetType().Name}");
                try
                {
                    await source.GetCodes();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Failed to find keys using {source.GetType().Name}");
                }
            }
        }
    }
}
