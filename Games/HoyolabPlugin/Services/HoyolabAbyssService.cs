﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Chipject(ChipjectType.Transient)]
    [Notify(NoEarlierThan = 1, RepeatIntervalSeconds = 60 * 60 * 24)] // 1 day
    public class HoyolabAbyssService : ChipService<HoyolabAbyssService>, INotifyService
    {
        private readonly GenshinGameRecordsService genshinGameRecords;
        private readonly HsrGameRecordsService hsrGameRecords;
        private readonly HoyolabAccountService accountService;
        private readonly AbyssRepositoryService abyssRepository;
        private readonly TheatreRepositoryService theatreRepository;

        public HoyolabAbyssService(ILogger<HoyolabAbyssService> log, GenshinGameRecordsService genshinGameRecords, HsrGameRecordsService hsrGameRecords, HoyolabAccountService accountService, AbyssRepositoryService abyssRepository, TheatreRepositoryService theatreRepository) : base(log)
        {
            this.genshinGameRecords = genshinGameRecords;
            this.hsrGameRecords = hsrGameRecords;
            this.accountService = accountService;
            this.abyssRepository = abyssRepository;
            this.theatreRepository = theatreRepository;
        }

        public async Task Notify()
        {
            logger.LogInformation("Starting to update abyss data");
            var accounts = accountService.GetAllAccounts();

            foreach (var account in accounts.SelectMany(a => a.GameAccounts))
            {
                if (!account.StoreAbyssData) continue;
                Dictionary<long, object> abyssData = null;
                Dictionary<long, ImaginariumTheaterData> theatreData = null;
                RecordsCharacterDetail charactedData = null;
                switch (account.Game)
                {
                    case "hk4e_global":
                        abyssData = await ProccessAbyssAccountGenshin(account);
                        theatreData = await ProccessTheatreAccountGenshin(account);
                        charactedData = await ProccessCharactersAccountGenshin(account, theatreData, abyssData?.ToDictionary(kv => kv.Key, kv => kv.Value as SpiralAbyssData));
                        break;
                    case "hkrpg_global":
                        abyssData = await ProccessAccountHsr(account);
                        break;
                }

                try
                {
                    if (abyssData != null && abyssData.Any())
                    {
                        logger.LogInformation($"Saving user ({account.HoyolabAccount.UserId}) abyss data");
                        abyssRepository.SaveAbyssData(abyssData, charactedData, account);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't save user ({account.HoyolabAccount.UserId}) abyss data");
                }

                try
                {
                    if (theatreData != null && theatreData.Any())
                    {
                        logger.LogInformation($"Saving user ({account.HoyolabAccount.UserId}) theatre data");
                        theatreRepository.SaveTheatreData(theatreData, charactedData, account);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't save user ({account.HoyolabAccount.UserId}) theatre data");
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private async Task<Dictionary<long, object>> ProccessAbyssAccountGenshin(HoyolabGameAccount account)
        {
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) abyss account {account.DisplayName}");
            var result = new List<SpiralAbyssData>();
            try
            {
                var tasks = new List<Task<SpiralAbyssData>> { genshinGameRecords.GetAbyss(account, true), genshinGameRecords.GetAbyss(account, false) };

                await Task.WhenAll(tasks);
                foreach (var task in tasks)
                {
                    if (!task.IsCompletedSuccessfully) continue;
                    result.Add(task.Result);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Couldn't get abyss data for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
            }
            return result.ToDictionary(a => a.ScheduleId, a => a as object);
        }

        private async Task<Dictionary<long, ImaginariumTheaterData>> ProccessTheatreAccountGenshin(HoyolabGameAccount account)
        {
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) theatre account {account.DisplayName}");
            try
            {
                var result = await genshinGameRecords.GetTheatre(account);
                return result.Data.Where(a => a.Schedule?.ScheduleId >= 1).ToDictionary(a => a.Schedule.ScheduleId, a => a);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Couldn't get theatre data for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
                return [];
            }
        }

        private async Task<RecordsCharacterDetail> ProccessCharactersAccountGenshin(HoyolabGameAccount account, Dictionary<long, ImaginariumTheaterData> theatreData, Dictionary<long, SpiralAbyssData> abyssData)
        {
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) characters account {account.DisplayName}");
            try
            {
                var characterIds = new List<long>();

                try
                {
                    if (theatreData == null) throw new Exception("No theatre data");
                    foreach (var theatre in theatreData)
                    {
                        if (!theatre.Value.HasData || !theatre.Value.HasDetailData) continue;
                        characterIds.AddRange(theatre.Value.Detail.BackupAvatars.Select(avatar => avatar.AvatarId));
                        characterIds.AddRange(theatre.Value.Detail.RoundsData.SelectMany(round => round.Avatars.Select(avatar => avatar.AvatarId)));
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't get characters ids from theatre for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
                }

                try
                {
                    if (abyssData == null) throw new Exception("No abyss data");
                    foreach (var abyss in abyssData)
                    {
                        characterIds.AddRange(abyss.Value.Floors.SelectMany(floor => floor.Levels.SelectMany(level => level.Battles.SelectMany(battle => battle.Avatars.Select(avatar => avatar.Id)))));
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't get characters ids from theatre for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
                }

                var validCharacterIds = (await genshinGameRecords.GetCharacterList(account)).List.Select(character => character.Id);

                characterIds = characterIds.Distinct().Where(id => validCharacterIds.Contains(id)).ToList();

                var characterData = await genshinGameRecords.GetCharacterDetail(account, characterIds);

                return characterData;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Couldn't get characters data for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
                return null;
            }
        }

        private async Task<Dictionary<long, object>> ProccessAccountHsr(HoyolabGameAccount account)
        {
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) account {account.DisplayName}");
            var result = new List<ForgottenHallData>();
            try
            {
                var tasks = new List<Task<ForgottenHallData>> { hsrGameRecords.GetAbyss(account, true), hsrGameRecords.GetAbyss(account, false) };

                await Task.WhenAll(tasks);
                foreach (var task in tasks)
                {
                    if (!task.IsCompletedSuccessfully) continue;
                    result.Add(task.Result);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Couldn't get abyss data for user ({account.HoyolabAccount.UserId}) {account.DisplayName}");
            }
            return result.ToDictionary(a => a.ScheduleId, a => a as object);
        }
    }
}
