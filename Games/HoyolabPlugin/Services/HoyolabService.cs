﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyolabPlugin.Services
{
    [Notify(NoEarlierThan = 8)]
    [Chipject(ChipjectType.Transient)]
    public class HoyolabService : ChipService<HoyolabService>, INotifyService
    {
        private readonly SettingsService<HoyolabConfig> config;
        private readonly HoyolabAccountService accountService;
        private readonly DiscordSocketClient discord;
        private readonly GenshinGameRecordsService genshinGameRecords;
        private readonly HsrGameRecordsService hsrGameRecords;

        private Dictionary<string, HomeData> homeData;

        public HoyolabService(ILogger<HoyolabService> log, SettingsService<HoyolabConfig> config, HoyolabAccountService accountService, DiscordSocketClient discord, GenshinGameRecordsService genshinGameRecords, HsrGameRecordsService hsrGameRecords) : base(log)
        {
            this.config = config;
            this.accountService = accountService;
            this.discord = discord;

            this.genshinGameRecords = genshinGameRecords;
            this.hsrGameRecords = hsrGameRecords;
        }

        public async Task Notify()
        {
            logger.LogInformation("Starting to proccess daily check-ins");
            var accounts = accountService.GetAllAccounts();

            foreach (var group in accounts.SelectMany(a => a.GameAccounts).GroupBy(a => a.HoyolabAccount.UserId))
            {
                var embeds = new List<Embed>();

                foreach (var account in group)
                {
                    try
                    {
                        await RefreshAccountIndex(account);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Couldn't refresh user ({account.HoyolabAccount.UserId}) records data");
                    }

                    if (!account.DailySignIn) continue;
                    var result = await ProccessAccountDailySignIn(account);
                    try
                    {
                        embeds.Add(BuildUserResult(account, result));
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Couldn't send user ({account.HoyolabAccount.UserId}) daily check-in result");
                    }

                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                await SendUserResult(group.Key, embeds);
            }
        }

        private async Task SendUserResult(ulong userId, List<Embed> embeds)
        {
            if (embeds == null || !embeds.Any()) return;
            var ch = await discord.GetUser(userId).CreateDMChannelAsync();
            await ch.SendMessageAsync(embeds: embeds.ToArray());
        }

        private Embed BuildUserResult(HoyolabGameAccount account, SignInResult result)
        {
            logger.LogInformation($"Sending user ({account.HoyolabAccount.UserId}) sign-in result for account {account.Uid} {account.Game}");
            EmbedBuilder embed;
            if (result.Success)
            {
                var fluffyAward = homeData != null && homeData.TryGetValue(account.Game, out var value)
                    ? value.Awards[result.SignInCount]
                    : new FluffyAward() { Name = "Paimon", Cnt = 1, Icon = "https://act.hoyolab.com/ys/event/signin-sea-v3/images/paimon.792472e0.png" };
                embed = GetEmbedFromReward(account, fluffyAward, result.SignInCount);
            }
            else
            {
                embed = GetEmbedFromError(account, result.ErrorMessage);
            }

            return embed.Build();
        }

        private static EmbedBuilder GetEmbedFromError(HoyolabGameAccount account, string errorMessage)
        {
            var embed = GetEmbedBase(account);
            return embed
                .WithTitle("There was an error")
                .WithDescription($"Couldn't automatically sign you in today. Please sign-in manually.\n\n{errorMessage}")
                .WithThumbnailUrl(embed.Author.IconUrl);
        }

        private static EmbedBuilder GetEmbedFromReward(HoyolabGameAccount account, FluffyAward fluffyAward, int signInCount)
        {
            return GetEmbedBase(account)
                .WithTitle("Here's your daily reward")
                .WithDescription($"{fluffyAward.Name} `x{fluffyAward.Cnt}`\n\nSign-ins this month: `{signInCount}`")
                .WithThumbnailUrl(fluffyAward.Icon);
        }

        private static EmbedBuilder GetEmbedBase(HoyolabGameAccount account)
        {
            return new EmbedBuilder()
                .WithCurrentTimestamp()
                .WithFooter($"{account.DisplayName} - {account.Uid}")
                .WithColor(121512)

                .WithAuthor("HoyoLab Daily Check-in", account.Game == "hk4e_global" ? "https://webstatic.hoyoverse.com/upload/static-resource/2022/11/01/37f91f2c509e94c98203a240b12975e9_7738727125880189847.png" : "https://webstatic-sea.hoyolab.com/communityweb/business/starrail_hoyoverse.png");
        }

        private async Task RefreshAccountIndex(HoyolabGameAccount account)
        {
            object recordsIndex = null;
            switch (account.Game)
            {
                case "hk4e_global":
                    recordsIndex = await genshinGameRecords.GetRecordsIndex(account);
                    break;
                case "hkrpg_global":
                    recordsIndex = await hsrGameRecords.GetRecordsIndex(account);
                    break;
            }

            if (recordsIndex != null)
            {
                account.RecordData = JsonConvert.SerializeObject(recordsIndex);
                accountService.UpdateGameAccount(account);
            }
        }

        private async Task<SignInResult> ProccessAccountDailySignIn(HoyolabGameAccount account)
        {
            var result = new SignInResult();
            logger.LogInformation($"Processing user's ({account.HoyolabAccount.UserId}) account {account.DisplayName}");
            try
            {
                var api = GetAccountApi(account);

                await GetHomeData(account.Game, api);

                var infoResponse = await api.GetInfo();
                if (infoResponse.Retcode == 0)
                {
                    result.SignInCount = infoResponse.Data.TotalSignDay;
                    if (infoResponse.Data.IsSign)
                    {
                        logger.LogInformation($"User's ({account.HoyolabAccount.UserId}) account {account.Uid} {account.Game} already signed-in today, skipping");
                        result.ErrorMessage = "Already signed-in today.";
                    }
                    else
                    {
                        logger.LogInformation($"Signing in User's ({account.HoyolabAccount.UserId}) account {account.Uid} {account.Game}");
                        var signResponse = await api.PostSign(new SignPayload() { ActId = config.Settings.GameConfigs[account.Game].ActId });
                        if (signResponse.Retcode == 0 && signResponse.Message.StartsWith("ok", StringComparison.InvariantCultureIgnoreCase)) // check if captcha
                        {
                            logger.LogInformation($"User's ({account.HoyolabAccount.UserId}) account {account.Uid} {account.Game} successful sign-in");
                            result.Success = true;
                        }
                        else
                        {
                            logger.LogWarning($"Error while trying to sign-in: {signResponse.Message}; code {signResponse.Data.Code}");
                            result.ErrorMessage = $"{signResponse.Message}; {signResponse.Data.Code}";
                        }
                    }
                }
                else
                {
                    logger.LogWarning($"Error while trying to get info: {infoResponse.Message}");
                    result.ErrorMessage = infoResponse.Message;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error while trying to proccess user's ({account.HoyolabAccount.UserId}) account {account.Uid} {account.Game}");
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        private async Task GetHomeData(string game, IHoyolabSignApi api)
        {
            if (homeData != null && homeData.ContainsKey(game)) return;

            logger.LogInformation("HomeData is null, trying to get");
            try
            {
                var homeResponse = await api.GetHome();
                if (homeResponse.Retcode == 0)
                {
                    logger.LogInformation("Got HomeData");
                    homeData ??= new Dictionary<string, HomeData>();
                    homeData.Add(game, homeResponse.Data);
                }
                else
                    logger.LogWarning($"Error while trying to get HomeData: {homeResponse.Message}");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Got exception while trying to get HomeData");
            }
        }

        private IHoyolabSignApi GetAccountApi(HoyolabGameAccount account)
        {
            var cfg = config.Settings;
            var gameCfg = config.Settings.GameConfigs[account.Game];

            var api = new RestClient(gameCfg.SignApi).For<IHoyolabSignApi>();

            api.ActId = gameCfg.ActId;
            api.Lang = cfg.Lang;
            api.Origin = gameCfg.HoyolabOrigin;
            api.Referer = gameCfg.HoyolabOrigin;
            api.Event = gameCfg.Event;

            api.Cookie = account.HoyolabAccount.Cookies;

            return api;
        }

        private class SignInResult
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int SignInCount { get; set; }
        }
    }
}
