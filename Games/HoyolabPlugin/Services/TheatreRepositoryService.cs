﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using HoyolabPlugin.Data;
using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace HoyolabPlugin.Services
{
    [Chipject]
    public class TheatreRepositoryService : ChipService<TheatreRepositoryService>
    {
        private readonly ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory;

        public TheatreRepositoryService(ILogger<TheatreRepositoryService> log, ChipBotDbContextFactory<HoyolabDbContext> dbContextFactory) : base(log)
            => this.dbContextFactory = dbContextFactory;

        public HoyolabTheatre GetById(int theatreId, int uid, string server)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabTheatre.Find(theatreId, uid, server);
        }

        public List<HoyolabTheatre> GetAll(int uid, string server)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.HoyolabTheatre.Where(a => a.UserId == uid && a.Server == server).ToList();
        }

        public void SaveTheatreData(Dictionary<long, ImaginariumTheaterData> data, RecordsCharacterDetail characterData, HoyolabGameAccount account)
        {
            if (data == null || !data.Any()) return;

            using var dbContext = dbContextFactory.CreateDbContext();
            foreach (var theatre in data)
            {
                var json = new HoyolabTheatre()
                {
                    TheatreId = (int)theatre.Key,
                    UserId = account.Uid,
                    Server = account.Region,
                    TheatreData = JsonConvert.SerializeObject(theatre.Value),
                    CharacterData = JsonConvert.SerializeObject(characterData)
                };

                if (dbContext.HoyolabTheatre.Where(a => a.TheatreId == json.TheatreId).Any())
                {
                    dbContext.HoyolabTheatre.Update(json);
                }
                else
                {
                    dbContext.HoyolabTheatre.Add(json);
                }
            }
            dbContext.SaveChanges();
        }
    }
}
