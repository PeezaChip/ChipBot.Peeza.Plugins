﻿using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Implementations;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using HoyolabPlugin.Api;
using HoyolabPlugin.Api.ApiModels;
using HoyolabPlugin.Models;
using HoyolabPlugin.Services;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace HoyolabPlugin
{
    [Group("hoyolab", "Hoyolab commands")]
    [ChipInteractionPrecondition(typeof(RequireChannel))]
    public class HoyolabModule : ChipInteractionModuleBase
    {
        public HoyolabAccountService Account { get; set; }
        public SettingsService<HoyolabConfig> HoyolabConfig { get; set; }
        public IMemoryCache Cache { get; set; }
        public DiscordSocketClient Discord { get; set; }
        public InteractionService InteractionService { get; set; }
        public GenshinGameRecordsService GenshinRecordsService { get; set; }
        public HsrGameRecordsService HsrRecordsService { get; set; }

        [SlashCommand("add-account", "Adds an account")]
        public async Task AddAccount()
        {
            var openModalButton = new ComponentBuilder().WithButton("Step 6", "hoyolab-add-button");

            await RespondAsync("1. Please open https://www.hoyolab.com/home\n" +
                "2. Log In\n" +
                "3. Press F12\n" +
                "4. Paste the code below into console and press enter\n" +
                "```js\n" +
                "document.clear();document.body.innerHTML=document.cookie + '<button onclick=\"navigator.clipboard.writeText(document.cookie)\">Click here to copy this</button>';\n" +
                "```\n" +
                "5. Click the button that showed up on your screen in your browser\n" +
                "6. Click the button in Discord\n", ephemeral: true, components: openModalButton.Build());
        }

        [ComponentInteraction("hoyolab-add-button", true)]
        public async Task AddAccountShowModal()
            => await RespondWithModalAsync<HoyolabAddModal>("hoyolab-add-modal");

        [ModalInteraction("hoyolab-add-modal", true)]
        public async Task AddAccountOnModal(HoyolabAddModal modal)
        {
            var selfDestruct = () =>
            {
                _ = Task.Delay(1000 * 60).ContinueWith(async (t) => await ModifyOriginalResponseAsync(RemoveMessageAndComponents));
            };

            await RespondAsync("Checking if you did everything correct..\nPlease wait.", ephemeral: true);

            var hoyoApi = new RestClient(IHoyolabApi.BaseAddress).For<IHoyolabApi>();
            hoyoApi.Cookie = modal.Cookies;
            var userInfoResponse = await hoyoApi.GetUserInfo();
            if (userInfoResponse.Retcode != 0)
            {
                await ModifyOriginalResponseAsync(m => m.Content = $"Something went wrong, please try again later.\nReason: {userInfoResponse.Message}\nError code: {userInfoResponse.Retcode}");
                selfDestruct();
                return;
            }
            var userInfo = userInfoResponse.Data;

            await ModifyOriginalResponseAsync(m =>
            {
                m.Embed = new EmbedBuilder()
                    .WithTitle(userInfo.UserInfo.Nickname)
                    .WithDescription("HoyoLab Account")
                    .WithFooter(userInfo.UserInfo.Uid.ToString())
                    .WithThumbnailUrl(userInfo.UserInfo.AvatarUrl)
                    .Build();
            });

            var gameroles = new Dictionary<string, List<GameRoles>>();
            var cfg = HoyolabConfig.Settings;
            foreach (var game in cfg.GameConfigs.Values)
            {
                var api = new RestClient(IHoyoverseAccountApi.BaseAddress).For<IHoyoverseAccountApi>();
                api.GameBiz = game.GameBiz;
                api.Lang = cfg.Lang;
                api.Origin = game.HoyolabOrigin;
                api.Referer = game.HoyolabOrigin;
                api.Cookie = modal.Cookies;

                var response = await api.GetUserGameRoles();
                if (response.Retcode != 0)
                {
                    await ModifyOriginalResponseAsync(m => m.Content = $"Something went wrong, please try again later.\nReason: {response.Message}\nError code: {response.Retcode}");
                    selfDestruct();
                    return;
                }
                else
                {
                    gameroles.Add(game.GameBiz, response.Data.List);
                }
            }

            if (gameroles.Count <= 0)
            {
                await ModifyOriginalResponseAsync(m => m.Content = "Haven't found a single account.");
                selfDestruct();
                return;
            }

            Cache.Set($"hoyolab-add-account-state-{Context.User.Id}", new AddAccountState() { Cookies = modal.Cookies, GameRoles = gameroles, UserInfo = userInfo }, TimeSpan.FromMinutes(3));
            var builder = new ComponentBuilder()
                .WithSelectMenu(new SelectMenuBuilder()
                .WithCustomId("hoyolab-add-select")
                .WithPlaceholder("Please select an account")
                .WithOptions(gameroles
                    .SelectMany(game => game.Value
                        .Select(role => new SelectMenuOptionBuilder()
                            .WithLabel($"{cfg.GameConfigs[game.Key].GameName} {role.GameUid}")
                            .WithDescription($"{role.Level}AR, {role.Nickname}, {role.RegionName}")
                            .WithValue(GameAccountKey.Build(game.Key, role.GameUid)))).ToList()));

            await ModifyOriginalResponseAsync(m => { m.Content = "Found multiple accounts. Please select the one you want to use."; m.Components = builder.Build(); });

            selfDestruct();
        }

        [ComponentInteraction("hoyolab-add-select", true)]
        public async Task AddAccountOnSelect(string key)
        {
            if (Context.Data is SocketMessageComponent data)
            {
                if (Cache.TryGetValue($"hoyolab-add-account-state-{Context.User.Id}", out AddAccountState state))
                {
                    var split = key.Split('|');
                    var role = state.GameRoles[split[0]].FirstOrDefault(g => g.GameUid == split[1]);
                    if (role != null)
                    {
                        Account.AddAccount(Context.User, state.Cookies, split[0], role, state.UserInfo);
                        await data.UpdateAsync((p) => ChangeMessageAndRemoveComponents(p, "Success."));
                        return;
                    }
                }

                await data.UpdateAsync((p) => ChangeMessageAndRemoveComponents(p, "Something terrible happened. Please try again later."));
            }
        }

        [SlashCommand("remove-account", "Removes an account")]
        public async Task RemoveAccount([Autocomplete(typeof(AccountNameAutoCompleter))] string key)
        {
            Account.RemoveGameAccount(Context.User, GameAccountKey.Parse(key));
            await ReplySuccessAsync(true);
        }

        [SlashCommand("setting", "Changes account settings")]
        public async Task ChangeAccount([Autocomplete(typeof(AccountNameAutoCompleter))] string key, [Autocomplete(typeof(AccountSettingAutoCompleter))][Summary("setting", "Setting to change")] string propname, [Summary("enable")] IsOn ison)
        {
            var account = Account.GetGameAccount(Context.User, GameAccountKey.Parse(key));
            if (account == null)
            {
                await ReplyFailAsync(true);
                return;
            }

            var prop = account.GetType().GetProperty(propname);
            prop.SetValue(account, ison == IsOn.enable);

            Account.UpdateGameAccount(account);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("daily", "Display your daily information")]
        public async Task DisplayDaily([Autocomplete(typeof(AccountNameAutoCompleter))] string key)
        {
            var account = Account.GetGameAccount(Context.User, GameAccountKey.Parse(key));
            if (account == null)
            {
                await ReplyFailAsync(true);
                return;
            }

            switch (account.Game)
            {
                case "hk4e_global":
                    await ReplyEmbedAsync(await GenshinRecordsService.GetDailyEmbed(account));
                    break;
                case "hkrpg_global":
                    await ReplyEmbedAsync(await HsrRecordsService.GetDailyEmbed(account));
                    break;
            }
        }

        private class AccountNameAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var accountService = services.GetRequiredService<HoyolabAccountService>();

                var accounts = accountService.GetAccounts(context.User);

                return Task.FromResult(AutocompletionResult.FromSuccess(accounts.Take(25).SelectMany(a => a.GameAccounts.Select(g => new AutocompleteResult($"{g.DisplayName} - {g.Uid} - {g.Region}", GameAccountKey.Build(g.Game, g.Uid))))));
            }
        }

        private class AccountSettingAutoCompleter : AutocompleteHandler
        {
            private static readonly PropertyInfo[] props = typeof(HoyolabAccountSettings).GetProperties();

            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
                => Task.FromResult(AutocompletionResult.FromSuccess(props.Take(25).Select(a => new AutocompleteResult($"{a.Name}", a.Name))));
        }

        public enum IsOn
        {
            enable,
            disable
        }

        public class HoyolabAddModal : IModal
        {
            public string Title => "Add account";

            [InputLabel("Cookies")]
            [ModalTextInput("hoyolab_acc_cookies", TextInputStyle.Paragraph, "Now paste here what you got in your browser", maxLength: 4000)]
            public string Cookies { get; set; }
        }

        private class AddAccountState
        {
            public string Cookies { get; set; }
            public Dictionary<string, List<GameRoles>> GameRoles { get; set; }
            public HoyoLabUserInfoRoot UserInfo { get; set; }
        }
    }
}
