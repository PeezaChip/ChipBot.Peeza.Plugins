﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using ChipBot.Core.Extensions;
using HoyolabPlugin.Extensions;
using HoyolabPlugin.Models;
using HoyolabPlugin.Services;
using HoyolabPlugin.Shared.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyolabPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "hoyolab", Version = "v1", Title = "ChipBot Hoyolab V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class HoyolabController : ControllerBase
    {
        private readonly HoyolabAccountService accountService;
        private readonly GenshinGameRecordsService genshinGameRecordsService;
        private readonly HsrGameRecordsService hsrGameRecordsService;
        private readonly AbyssRepositoryService abyssRepositoryService;
        private readonly TheatreRepositoryService theatreRepositoryService;

        public HoyolabController(HoyolabAccountService accountService,
            GenshinGameRecordsService genshinGameRecordsService, HsrGameRecordsService hsrGameRecordsService,
            AbyssRepositoryService abyssRepositoryService,
            TheatreRepositoryService theatreRepositoryService)
        {
            this.accountService = accountService;
            this.genshinGameRecordsService = genshinGameRecordsService;
            this.hsrGameRecordsService = hsrGameRecordsService;
            this.abyssRepositoryService = abyssRepositoryService;
            this.theatreRepositoryService = theatreRepositoryService;
        }

        [HttpGet("accounts")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<HoyolabGameAccount>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.MinuteUserCache)]
        public IActionResult GetAccounts()
        {
            var hoyolabAccounts = accountService.GetAccounts(HttpContext.GetCurrentDiscordUserId());

            return Ok(hoyolabAccounts.SelectMany(a => a.GameAccounts).Select(a => a.ToApi()));
        }

        [HttpGet("daily")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DailyNoteData))]
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DailyNoteHsrData))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.MinuteUserCache)]
        public async Task<IActionResult> GetDaily(string key)
        {
            var account = accountService.GetGameAccount(HttpContext.GetCurrentDiscordUserId(), GameAccountKey.Parse(key));
            return account == null
                ? NotFound()
                : account.Game switch
                {
                    "hk4e_global" => Ok(await genshinGameRecordsService.GetDailyNote(account)),
                    "hkrpg_global" => Ok(await hsrGameRecordsService.GetDailyNote(account)),
                    _ => BadRequest(),
                };
        }

        [HttpGet("abyss")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SpiralAbyssData))]
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ForgottenHallData))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.MinuteUserCache)]
        public async Task<IActionResult> GetAbyss(string key, bool current)
        {
            var account = accountService.GetGameAccount(HttpContext.GetCurrentDiscordUserId(), GameAccountKey.Parse(key));
            return account == null
                ? NotFound()
                : account.Game switch
                {
                    "hk4e_global" => Ok(await genshinGameRecordsService.GetAbyss(account, current)),
                    "hkrpg_global" => Ok(await hsrGameRecordsService.GetAbyss(account, current)),
                    _ => BadRequest(),
                };
        }

        [HttpGet("abyss/history")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SpiralAbyssHistoryWrapper>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.HourUserCache)]
        public IActionResult GetAbyssHistory(string key)
        {
            var account = accountService.GetGameAccount(HttpContext.GetCurrentDiscordUserId(), GameAccountKey.Parse(key));
            return account == null
                ? NotFound()
                : account.Game switch
                {
                    "hk4e_global" => Ok(abyssRepositoryService.GetAll(account.Uid, account.Region).Select(a => new SpiralAbyssHistoryWrapper()
                    {
                        SpiralAbyss = JsonConvert.DeserializeObject<SpiralAbyssData>(a.AbyssData),
                        CharacterDetails = string.IsNullOrEmpty(a.CharacterData) ? null : JsonConvert.DeserializeObject<RecordsCharacterDetail>(a.CharacterData),
                    })),
                    "hkrpg_global" => BadRequest(),
                    _ => BadRequest(),
                };
        }

        [HttpGet("theatre/history")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ImaginariumTheatreHistoryWrapper>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.HourUserCache)]
        public IActionResult GetTheatreHistory(string key)
        {
            var account = accountService.GetGameAccount(HttpContext.GetCurrentDiscordUserId(), GameAccountKey.Parse(key));
            return account == null
                ? NotFound()
                : account.Game switch
                {
                    "hk4e_global" => Ok(theatreRepositoryService.GetAll(account.Uid, account.Region).Select(a => new ImaginariumTheatreHistoryWrapper()
                    {
                        ImaginariumTheater = JsonConvert.DeserializeObject<ImaginariumTheaterData>(a.TheatreData),
                        CharacterDetails = string.IsNullOrEmpty(a.CharacterData) ? null : JsonConvert.DeserializeObject<RecordsCharacterDetail>(a.CharacterData)
                    })),
                    "hkrpg_global" => BadRequest(),
                    _ => BadRequest(),
                };
        }
    }
}
