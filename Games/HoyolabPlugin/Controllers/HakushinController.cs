﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using HoyolabPlugin.Services;
using HoyolabPlugin.Shared.Api.Models.Hakushin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HoyolabPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "hoyolab", Version = "v1", Title = "ChipBot Hoyolab V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class HakushinController : ControllerBase
    {
        private readonly HakushinService hakushinService;

        public HakushinController(HakushinService hakushinService)
            => this.hakushinService = hakushinService;

        [HttpGet("characters")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<string, Character>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetCharacters() => Ok(hakushinService.GetData()?.Characters);

        [HttpGet("character")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CharacterFull))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetCharacter(string id) => Ok(hakushinService.GetData()?.CharactersFull.TryGetValue(id, out var character) ?? false ? character : null);

        [HttpGet("weapons")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<string, Weapon>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetWeapons() => Ok(hakushinService.GetData()?.Weapons);

        [HttpGet("weapon")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(WeaponFull))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetWeapon(string id) => Ok(hakushinService.GetData()?.WeaponsFull.TryGetValue(id, out var weapon) ?? false ? weapon : null);

        [HttpGet("items")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<string, Item>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetItems() => Ok(hakushinService.GetData()?.Items);

        [HttpGet("data")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(HakushinData))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public IActionResult GetData(bool full) => Ok(full ? hakushinService.GetData() : hakushinService.GetData()?.ToHalf() );
    }
}
