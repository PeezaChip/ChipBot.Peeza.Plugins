﻿using HoyolabPlugin.Models;
using HoyolabPlugin.Shared.Api.Models;
using Newtonsoft.Json;

namespace HoyolabPlugin.Extensions
{
    public static class ApiExtensions
    {
        public static HoyolabGameAccountApi ToApi(this HoyolabGameAccount account)
        {
            var api = new HoyolabGameAccountApi()
            {
                DisplayName = account.DisplayName,
                Game = account.Game,
                HoyolabAccountId = account.HoyolabAccountId,
                Region = account.Region,
                Uid = account.Uid,
            };

            if (account.RecordData != null)
            {
                switch (account.Game)
                {
                    case "hk4e_global":
                        api.GenshinData = JsonConvert.DeserializeObject<RecordsIndexData>(account.RecordData);
                        break;

                    case "hkrpg_global":
                        api.HsrData = JsonConvert.DeserializeObject<RecordsIndexHsrData>(account.RecordData);
                        break;
                }
            }

            return api;
        }
    }
}
