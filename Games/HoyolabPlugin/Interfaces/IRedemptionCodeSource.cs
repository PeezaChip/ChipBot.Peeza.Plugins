﻿using System.Threading.Tasks;

namespace HoyolabPlugin.Interfaces
{
    public interface IRedemptionCodeSource
    {
        Task GetCodes();
    }
}
