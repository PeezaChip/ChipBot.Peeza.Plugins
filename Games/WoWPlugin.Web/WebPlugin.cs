﻿using ChipBot.Core.Web.Abstract;

namespace WoWPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "WoWPlugin";
    }
}
