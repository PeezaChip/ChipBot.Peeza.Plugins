﻿using AnimalPlugin.Services;
using ChipBot.Core.Implementations;
using ChipBot.Core.Interceptors.Cooldown;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using PointsPlugin.Interceptor;
using System.Threading.Tasks;

namespace AnimalPlugin.Modules
{
    public class AbstractAnimalModule<TService, TUserConfig> : ChipInteractionModuleBase
        where TService : AnimalService<TService, TUserConfig>
        where TUserConfig : AnimaBaselUserConfig, new()
    {
        public TService Service { get; set; }
        public DiscordSocketClient Discord { get; set; }

        [SlashCommand("random", "Displays random image")]
        public async Task RandomImage()
            => await ReplyEmbedAsync((await Service.GetImage()).GetEmbed());

        [SlashCommand("gift", "Displays random image to a specified user")]
        [Cooldown(1000 * 60 * 60, Global = false)] // 1hr
        [Price(100, RefundOnError = true)]
        public async Task Gift(IUser user)
        {
            var cat = await Service.GetImage();
            var embed = cat.GetEmbed().WithDescription($"Here's a gift from {Context.User.Username}");
            var ch = await user.CreateDMChannelAsync();
            await ch.SendMessageAsync(embed: embed.Build());
            await ReplySuccessAsync(true);
        }

        [SlashCommand("subscribe", "Subscribes on daily image")]
        public async Task Subscribe()
        {
            Service.Subscribe(Context.User);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("unsubscribe", "Unsubscribes from daily image")]
        public async Task Unsubscribe()
        {
            Service.Unsubscribe(Context.User);
            await ReplySuccessAsync(true);
        }
    }
}
