﻿using AnimalPlugin.Services;
using ChipBot.Core.Implementations;
using Discord.Interactions;

namespace AnimalPlugin.Modules
{
    [Group("animal", "animal commands")]
    public class AnimalModule : ChipInteractionModuleBase
    {
        [Group("cat", "cat commands")]
        public class CatModule : AbstractAnimalModule<CatService, CatUserConfig> { }

        [Group("dog", "dog commands")]
        public class DogModule : AbstractAnimalModule<DogService, DogUserConfig> { }
    }
}
