﻿namespace AnimalPlugin
{
    public class AnimalConfig
    {
        public string CatApiKey { get; set; }
        public string DogApiKey { get; set; }
    }
}
