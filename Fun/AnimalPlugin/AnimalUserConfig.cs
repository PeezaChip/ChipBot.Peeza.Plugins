﻿namespace AnimalPlugin
{
    public abstract class AnimaBaselUserConfig
    {
        public bool IsSubscribed { get; set; }
    }

    public class CatUserConfig : AnimaBaselUserConfig { }
    public class DogUserConfig : AnimaBaselUserConfig { }
}
