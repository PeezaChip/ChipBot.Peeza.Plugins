﻿using AnimalPlugin.Services;
using ChipBot.Core.Attributes;
using ChipBot.Core.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AnimalPlugin.Controllers
{
    [ApiController]
    [Route("animal/[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "animal", Version = "v1", Title = "ChipBot Animal V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public abstract class AnimalController<TService, TUserConfig> : ControllerBase
        where TService : AnimalService<TService, TUserConfig>
        where TUserConfig : AnimaBaselUserConfig, new()
    {
        private readonly TService animalService;

        public AnimalController(TService animalService)
            => this.animalService = animalService;

        [HttpGet("current")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public IActionResult IsSubscribed()
            => Ok(animalService.IsSubscribed(HttpContext.GetCurrentDiscordUserId()));

        [HttpPost("subscribe")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Subscribe()
        {
            animalService.Subscribe(HttpContext.GetCurrentDiscordUserId());
            return Ok();
        }

        [HttpPost("unsubscribe")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Unsubscribe()
        {
            animalService.Unsubscribe(HttpContext.GetCurrentDiscordUserId());
            return Ok();
        }
    }
}
