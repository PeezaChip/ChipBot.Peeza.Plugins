﻿using AnimalPlugin.Services;

namespace AnimalPlugin.Controllers
{
    public class DogController : AnimalController<DogService, DogUserConfig>
    {
        public DogController(DogService animalService) : base(animalService) { }
    }
}
