﻿using AnimalPlugin.Services;

namespace AnimalPlugin.Controllers
{
    public class CatController : AnimalController<CatService, CatUserConfig>
    {
        public CatController(CatService animalService) : base(animalService) { }
    }
}
