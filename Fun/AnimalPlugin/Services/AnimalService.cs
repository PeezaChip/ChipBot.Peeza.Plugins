﻿using AnimalPlugin.Extensions;
using AnimalPlugin.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AnimalPlugin.Services
{
    [Notify(NoEarlierThan = 10)]
    public abstract class AnimalService<TService, TUserConfig> : ChipService<TService>, INotifyService
        where TService : class
        where TUserConfig : AnimaBaselUserConfig, new()
    {
        private readonly IAnimalApi api;
        private readonly UserSettingsService<TUserConfig> userConfig;
        private readonly DiscordSocketClient discord;

        public AnimalService(ILogger<TService> log, IAnimalApi api, UserSettingsService<TUserConfig> userConfig, DiscordSocketClient discord) : base(log)
        {
            this.api = api;
            this.userConfig = userConfig;
            this.discord = discord;
        }

        public async Task Notify()
        {
            logger.LogInformation($"Sending daily {GetType().Name}");

            foreach (var (userId, userSettings) in userConfig.UserSettings)
            {
                try
                {
                    if (!userSettings.IsSubscribed) continue;

                    logger.LogInformation($"Sending user ({userId}) daily {GetType().Name}");
                    var cat = await GetImage();
                    var embed = cat.GetEmbed().WithDescription("Here's your daily animal");
                    var ch = await discord.GetUser(userId).CreateDMChannelAsync();
                    await ch.SendMessageAsync(embed: embed.Build());

                    await Task.Delay(1000);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't send user ({userId}) daily {GetType().Name}");
                }
            }
        }

        public Task<AnimalImage> GetImage()
            => api.GetImage();

        public bool IsSubscribed(IUser user) => IsSubscribed(user.Id);
        public bool IsSubscribed(ulong userId)
        {
            var settings = userConfig.Get(userId);
            return settings.IsSubscribed;
        }

        public void Subscribe(IUser user) => Subscribe(user.Id);
        public void Subscribe(ulong userId)
        {
            var settings = userConfig.Get(userId);

            settings.IsSubscribed = true;
            userConfig.SaveConfiguration(settings, userId);
        }

        public void Unsubscribe(IUser user) => Unsubscribe(user.Id);
        public void Unsubscribe(ulong userId)
        {
            var settings = userConfig.Get(userId);

            settings.IsSubscribed = false;
            userConfig.SaveConfiguration(settings, userId);
        }
    }
}
