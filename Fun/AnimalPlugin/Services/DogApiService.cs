﻿using AnimalPlugin.Models;
using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RestEase;
using System;
using System.Net.Http;

namespace AnimalPlugin.Services
{
    [Chipject]
    public class DogApiService : ApiService<IAnimalApi>
    {
        private const string ApiUrl = "https://api.thedogapi.com/v1/";

        public DogApiService(ILogger<DogApiService> log, SettingsService<AnimalConfig> config, SettingsService<NetworkConfig> networkConfig) : base(log, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(ApiUrl)
                }).For<IAnimalApi>();

            ApiClient.UserAgent = UserAgent;
            ApiClient.ApiKey = config.Settings.DogApiKey;
        }
    }
}
