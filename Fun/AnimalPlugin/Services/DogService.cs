﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;

namespace AnimalPlugin.Services
{
    [Chipject]
    public class DogService : AnimalService<DogService, DogUserConfig>
    {
        public DogService(ILogger<DogService> log, DogApiService api, UserSettingsService<DogUserConfig> userConfig, DiscordSocketClient discord)
            : base(log, api.ApiClient, userConfig, discord) { }
    }
}
