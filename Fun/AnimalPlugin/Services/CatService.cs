﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;

namespace AnimalPlugin.Services
{
    [Chipject]
    public class CatService : AnimalService<CatService, CatUserConfig>
    {
        public CatService(ILogger<CatService> log, CatApiService api, UserSettingsService<CatUserConfig> userConfig, DiscordSocketClient discord)
            : base(log, api.ApiClient, userConfig, discord) { }
    }
}
