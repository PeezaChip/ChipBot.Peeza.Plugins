﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;

namespace AnimalPlugin.Models
{
    public class AnimalImage
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; } = 0;

        [JsonProperty("height")]
        public int Height { get; set; } = 0;

        public EmbedBuilder GetEmbed()
        {
            return new EmbedBuilder()
                .WithFooter($"ID {Id}")
                .WithCurrentTimestamp()
                .WithColor(GetColor())
                .WithImageUrl(Url.ToString());
        }

        private Color GetColor()
        {
            var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(Id));
            return new Color(BitConverter.ToUInt32(bytes) % (maxColorValue + 1));
        }

        private static readonly MD5 md5 = MD5.Create();
        private static readonly uint maxColorValue = 0xFFFFFF;
    }
}
