﻿using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnimalPlugin.Models
{
    public interface IAnimalApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Header("x-api-key")]
        string ApiKey { get; set; }

        [Get("images/search")]
        Task<List<AnimalImage>> GetImages(int limit = 1);
    }
}
