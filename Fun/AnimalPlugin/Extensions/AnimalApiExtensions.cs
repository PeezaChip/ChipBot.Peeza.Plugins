﻿using AnimalPlugin.Models;
using System.Linq;
using System.Threading.Tasks;

namespace AnimalPlugin.Extensions
{
    public static class AnimalApiExtensions
    {
        public static async Task<AnimalImage> GetImage(this IAnimalApi animalApi) => (await animalApi.GetImages(1)).First();
    }
}
