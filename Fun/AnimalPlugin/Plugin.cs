﻿using ChipBot.Core.Abstract;

namespace AnimalPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "AnimalPlugin";
    }
}
