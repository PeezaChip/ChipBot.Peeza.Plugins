﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using NineGagPlugin.Models;
using RestEase;
using System;
using System.Net;
using System.Net.Http;

namespace NineGagPlugin.Services
{
    [Chipject]
    public class NineGagApi : ApiService<INineGagApi>
    {
        private const string ApiUrl = "https://9gag.com/v1/";

        public NineGagApi(ILogger<NineGagApi> log, SettingsService<NetworkConfig> network) : base(log, network)
        {
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip
            };

            ApiClient = new RestClient(new HttpClient(handler)
            {
                BaseAddress = new Uri(ApiUrl)
            }).For<INineGagApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
