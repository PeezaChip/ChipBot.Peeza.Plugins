﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using NineGagPlugin.Extensions;
using NineGagPlugin.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NineGagPlugin.Modules
{
    [Group("9gag", "9gag commands")]
    public class NineGagModule : ChipInteractionModuleBase
    {
        public NineGagApi Api { get; set; }
        public Random Rng { get; set; }

        [SlashCommand("meme", "Get meme with specified tag")]
        public async Task NineGag([Summary("tags", "tags e.g. wednesday-addams, cats")] string tags)
        {
            var split = tags.Split(" ", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            var combinedPath = string.Join("/tag/", split);

            try
            {
                var response = await Api.ApiClient.GetPostsWithTag(combinedPath);
                if (response.Data == null || !response.Data.Posts.Any())
                {
                    await ReplyError("No images was found with your tags.");
                    return;
                }

                var post = response.Data.Posts.ElementAt(Rng.Next(response.Data.Posts.Count - 1));
                await post.SendToChannel((ITextChannel)Context.Channel);
            }
            catch
            {
                await ReplyError("Something went wrong.");
            }
        }

        [SlashCommand("meme-by-id", "Get meme with specified id")]
        [RegisterToDevGuildOnly]
        public async Task NineGagId([Summary("id", "post id")] string id)
        {
            try
            {
                var response = await Api.ApiClient.GetPostById(id);
                if (response.Data == null || response.Data.Post == null)
                {
                    await ReplyError("No images was found with specified id.");
                    return;
                }

                await response.Data.Post.SendToChannel((ITextChannel)Context.Channel);
            }
            catch
            {
                await ReplyError("Something went wrong.");
            }
        }
    }
}
