﻿using Discord;
using NineGagPlugin.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NineGagPlugin.Extensions
{
    public static class NineGagExtensions
    {
        public static Embed ToEmbed(this Post post, out string additionalText)
        {
            additionalText = null;
            var eb = new EmbedBuilder()
                .WithAuthor("9GAG", "https://9gag.com/favicon.ico", post.PostSection.Url.ToString())
                .WithThumbnailUrl(post.PostSection.ImageUrl.ToString())
                .WithTitle(post.Title)
                .WithTimestamp(DateTimeOffset.FromUnixTimeSeconds(post.CreationTs))
                .WithFooter(string.Join(", ", post.Tags.Take(3).Select(t => t.Key)));

            eb.AddField("ℹ️", $"👍 {post.UpVoteCount} 👎 {post.DownVoteCount}\n💬 {post.CommentsCount}");

            if (!string.IsNullOrWhiteSpace(post.Description))
                eb.WithDescription(post.Description);

            switch (post.Type.ToLower())
            {
                case "photo":
                    eb.WithImageUrl(post.Images.Image700.Url.ToString());
                    break;

                case "animated":
                    additionalText = $"{(post.Images.Image460Sv.HasAudio > 0 ? "🔊" : "")}{post.Images.Image460Sv.Url}";
                    break;

                default:
                    additionalText = $"\n Hey, listen here you little shit <@199924382724915201>, I've got {post.Type} and I don't know what to do.";
                    break;
            }

            return eb.Build();
        }

        public static async Task SendToChannel(this Post post, ITextChannel channel)
        {
            var embed = post.ToEmbed(out var text);
            await channel.SendMessageAsync(embed: embed);
            if (!string.IsNullOrWhiteSpace(text))
                await channel.SendMessageAsync(text);
        }
    }
}
