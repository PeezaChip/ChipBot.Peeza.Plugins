﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using Discord;
using Microsoft.Extensions.Logging;
using NineGagPlugin.Extensions;
using NineGagPlugin.Services;
using System.Linq;
using System.Threading.Tasks;
using WednesdayPlugin.Core;

namespace NineGagPlugin
{
    [Chipject(ChipjectType.Transient, typeof(IWednesdayProvider))]
    public class WednesNineProvider : ChipService<WednesNineProvider>, IWednesdayProvider
    {
        private readonly NineGagApi api;

        public WednesNineProvider(ILogger<WednesNineProvider> logger, NineGagApi api) : base(logger)
            => this.api = api;

        public async Task SendMeme(ITextChannel channel)
        {
            logger.LogInformation("Getting wednesday posts");
            var posts = await api.ApiClient.GetPostsWithTag("wednesday", "fresh");
            var post = posts.Data.Posts.OrderByDescending(p => p.UpVoteCount).First();

            logger.LogInformation("Sending wednesday meme");
            await post.SendToChannel(channel);
        }
    }
}
