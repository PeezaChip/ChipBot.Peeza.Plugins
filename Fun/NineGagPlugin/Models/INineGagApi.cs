﻿using RestEase;
using System.Threading.Tasks;

namespace NineGagPlugin.Models
{
    public interface INineGagApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("tag-posts/tag/{tag}/type/{type}")]
        Task<NineResponse<MultiplePostsData>> GetPostsWithTag([Path(UrlEncode = false)] string tag, [Path] string type = "hot");


        [Get("post/id/{id}")]
        Task<NineResponse<SinglePostData>> GetPostById([Path] string id);
    }
}
