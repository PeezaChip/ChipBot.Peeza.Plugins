﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace NineGagPlugin.Models
{
    public class NineResponse<T>
    {
        [JsonProperty("meta")]
        public Meta Meta { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }

    public abstract class NineData
    {
        [JsonProperty("nextCursor")]
        public string NextCursor { get; set; }
    }

    public class SinglePostData : NineData
    {
        [JsonProperty("post")]
        public Post Post { get; set; }

        [JsonProperty("nextPosts")]
        public List<Post> NextPosts { get; set; }

        [JsonProperty("prevPosts")]
        public List<object> PrevPosts { get; set; }
    }

    public class MultiplePostsData
    {
        [JsonProperty("posts")]
        public List<Post> Posts { get; set; }

        [JsonProperty("tag")]
        public Tag Tag { get; set; }

        [JsonProperty("tags")]
        public List<Tag> Tags { get; set; }
    }

    public class Post
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("nsfw")]
        public long Nsfw { get; set; }

        [JsonProperty("upVoteCount")]
        public long UpVoteCount { get; set; }

        [JsonProperty("downVoteCount")]
        public long DownVoteCount { get; set; }

        [JsonProperty("creationTs")]
        public long CreationTs { get; set; }

        [JsonProperty("promoted")]
        public long Promoted { get; set; }

        [JsonProperty("isVoteMasked")]
        public long IsVoteMasked { get; set; }

        [JsonProperty("hasLongPostCover")]
        public long HasLongPostCover { get; set; }

        [JsonProperty("images")]
        public Images Images { get; set; }

        [JsonProperty("sourceDomain")]
        public string SourceDomain { get; set; }

        [JsonProperty("sourceUrl")]
        public string SourceUrl { get; set; }

        [JsonProperty("commentsCount")]
        public long CommentsCount { get; set; }

        [JsonProperty("postSection")]
        public PostSection PostSection { get; set; }

        [JsonProperty("tags")]
        public List<Tag> Tags { get; set; }
    }

    public class Images
    {
        [JsonProperty("image700")]
        public Image Image700 { get; set; }

        [JsonProperty("image460")]
        public Image Image460 { get; set; }

        [JsonProperty("imageFbThumbnail")]
        public Image ImageFbThumbnail { get; set; }

        [JsonProperty("image460sv", NullValueHandling = NullValueHandling.Ignore)]
        public Image460Sv Image460Sv { get; set; }
    }

    public class Image
    {
        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("webpUrl", NullValueHandling = NullValueHandling.Ignore)]
        public Uri WebpUrl { get; set; }
    }

    public class Image460Sv
    {
        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("hasAudio")]
        public long HasAudio { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("vp8Url")]
        public Uri Vp8Url { get; set; }

        [JsonProperty("h265Url")]
        public Uri H265Url { get; set; }

        [JsonProperty("vp9Url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Vp9Url { get; set; }
    }

    public class PostSection
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("webpUrl")]
        public Uri WebpUrl { get; set; }
    }

    public class Tag
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public class Meta
    {
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("sid")]
        public string Sid { get; set; }
    }
}
