﻿using ChipBot.Core.Abstract;
using Lavalink4NET.Extensions;
using Lavalink4NET.InactivityTracking.Extensions;
using Lavalink4NET.InactivityTracking.Trackers.Users;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace XinyanPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "XinyanPlugin";

        public override void ModifyServiceCollection(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            serviceCollection.AddLavalink();
            serviceCollection.AddInactivityTracking();
            serviceCollection.AddInactivityTracker<UsersInactivityTracker>();

            serviceCollection.ConfigureLavalink(o =>
            {
                o.BaseAddress = configuration.GetValue<Uri>("LavalinkConfig:BaseAddress");
                o.Label = configuration.GetValue<string>("LavalinkConfig:Label");
                o.Passphrase = configuration.GetValue<string>("LavalinkConfig:Passphrase");
            });

            serviceCollection.ConfigureInactivityTracking(o =>
            {
                o.UseDefaultTrackers = false;
            });
        }
    }
}
