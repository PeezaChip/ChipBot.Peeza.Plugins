﻿using ChipBot.Core.Extensions;
using Lavalink4NET.Players;
using Lavalink4NET.Players.Queued;
using XinyanPlugin.Core;
using XinyanPlugin.Shared.Api;

namespace XinyanPlugin.Extensions
{
    public static class ModelExtensions
    {
        public static ApiTrack ToApi(this XinyanQueueItem item)
        {
            var track = item.Reference.Track;

            return new ApiTrack()
            {
                Author = track.Author,
                Duration = track.Duration,
                Image = track.ArtworkUri?.ToString(),
                Source = track.SourceName,
                Title = track.Title,
                Url = track.Uri?.ToString(),
                UserId = item.Requester.Id
            };
        }

        public static ApiQueue ToApiQueue(this XinyanPlayer player)
        {
            return new ApiQueue()
            {
                CurrentTrack = player.CurrentItem?.ToApi(),
                Queue = player.Queue.Select(i => i.ToApi()).ToList(),
                History = player.History.Reverse().Take(20).Select(i => i.ToApi()).ToList()
            };
        }

        public static ApiPlayerState ToApiState(this XinyanPlayer player)
        {
            var volume = (int)(player.Volume * 100);

            if (volume < 0)
            {
                volume = 0;
            }
            else if (volume > 1000)
            {
                volume = 1000;
            }

            return new ApiPlayerState()
            {
                IsPlaying = !player.IsPaused,
                IsRepeatEnabled = player.RepeatMode == TrackRepeatMode.Track,
                IsShuffleEnabled = player.Shuffle,
                Volume = volume,
                Position = player.Position?.RelativePosition ?? TimeSpan.Zero,
                Guild = player.Guild.ToApiDiscordGuild()
            };
        }
    }
}
