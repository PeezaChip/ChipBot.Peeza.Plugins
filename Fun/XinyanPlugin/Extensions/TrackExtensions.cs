﻿using Discord;
using Lavalink4NET.Tracks;
using System.Security.Cryptography;
using System.Text;
using XinyanPlugin.Core;

namespace XinyanPlugin.Extensions
{
    public static class TrackExtensions
    {
        public static EmbedBuilder ToEmbed(this XinyanQueueItem item)
        {
            var eb = item.Reference.Track.ToEmbed();

            if (item.Requester != null)
            {
                eb.WithAuthor(item.Requester);
            }

            return eb;
        }

        public static EmbedBuilder ToEmbed(this LavalinkTrack track)
        {
            var uri = track.Uri.ToString();
            var eb = new EmbedBuilder()
                .WithUrl(uri)
                .WithDescription(track.Title)
                .WithColor(GetColor(uri))
                .WithFooter($"⏱️ {track.Duration}");

            if (track.ArtworkUri != null)
            {
                eb.WithThumbnailUrl(track.ArtworkUri.ToString());
            }

            return eb;
        }

        private static Color GetColor(string uri)
        {
            var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(uri));
            return new Color(BitConverter.ToUInt32(bytes) % (maxColorValue + 1));
        }

        private static readonly MD5 md5 = MD5.Create();
        private static readonly uint maxColorValue = 0xFFFFFF;
    }
}
