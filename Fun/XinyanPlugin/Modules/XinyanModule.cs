﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using Humanizer;
using Lavalink4NET.Rest.Entities.Tracks;
using Microsoft.Extensions.DependencyInjection;
using XinyanPlugin.Core;
using XinyanPlugin.Models;
using XinyanPlugin.Services;

namespace XinyanPlugin.Modules
{
    [RequireContext(ContextType.Guild)]
    public class XinyanModule : ChipInteractionModuleBase
    {
        public XinyanService XinyanService { get; set; }

        [SlashCommand("player", "player actions")]
        public async Task Player(PlayerAction action)
        {
            var player = await GetPlayer();
            if (player == null)
            {
                await ReplyError("Nothing is playing");
                return;
            }

            await player.DoPlayerAction(action);
            await RespondAsync($"Player `{action}`");
        }

        [SlashCommand("queue", "queue actions")]
        public async Task Queue(QueueAction action)
        {
            var player = await GetPlayer();
            if (player == null)
            {
                await RespondAsync("Nothing is playing");
                return;
            }

            bool result;
            switch (action)
            {
                case QueueAction.repeat:
                    result = await player.ToggleRepeat();
                    break;

                case QueueAction.shuffle:
                    result = await player.ToggleShuffle();
                    break;

                default:
                    await ReplyFailAsync();
                    return;
            }

            await RespondAsync($"{action.ToString().ApplyCase(LetterCasing.Title)} is `{(result ? "On" : "Off")}`");
            return;
        }

        [SlashCommand("play", "plays music")]
        public async Task Play([Summary("query", "search query")][Autocomplete(typeof(TrackSearchAutoCompleter))] string link)
        {
            await DeferAsync(ephemeral: true);

            var isUri = Uri.IsWellFormedUriString(link, UriKind.Absolute);

            if (!isUri)
            {
                await FollowupAsync("Something went wrong", ephemeral: true);
                return;
            }

            await XinyanService.LoadTracks(Context, link);
            await FollowupAsync("Done", ephemeral: true);
        }

        [SlashCommand("position", "shows the track position")]
        public async Task Position()
        {
            var player = await GetPlayer();
            if (player == null || player.CurrentItem == null)
            {
                await RespondAsync("Nothing is playing");
                return;
            }

            await RespondAsync($"Position: {player.Position?.Position} / {player.CurrentTrack.Duration}.");
        }

        [SlashCommand("volume", "sets the player volume (0 - 1000%)")]
        public async Task Volume(int volume = 100)
        {
            if (volume is > 1000 or < 0)
            {
                await RespondAsync("Volume out of range: `0%` - `1000%`");
                return;
            }

            var player = await GetPlayer();
            if (player == null)
            {
                await RespondAsync("Nothing is playing");
                return;
            }

            await player.SetVolumeAsync(volume / 100f);
            await RespondAsync($"Volume `{volume}%`");
        }

        private class TrackSearchAutoCompleter : AutocompleteHandler
        {
            private readonly AutocompleteResult[] emptySearch;

            private string lastSearch;
            private DateTime lastSearchTime;
            private AutocompletionResult lastSearchResult;

            public TrackSearchAutoCompleter()
                => emptySearch = Array.Empty<AutocompleteResult>();

            public async override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var query = autocompleteInteraction.Data.Current.Value.ToString();
                if (query.Length < 3) return AutocompletionResult.FromSuccess(emptySearch);
                if (lastSearch != null && lastSearch == query && DateTime.Now - lastSearchTime < SearchCache) return lastSearchResult;
                if (DateTime.Now - lastSearchTime < SearchCooldown) return lastSearchResult;

                var xinyan = services.GetRequiredService<XinyanService>();
                var search = await xinyan.SearchTracks(query);

                if (!search.IsSuccess)
                {
                    return AutocompletionResult.FromSuccess(emptySearch);
                }

                var completionResult = ToResult(search, query);

                lastSearch = query;
                lastSearchTime = DateTime.Now;
                lastSearchResult = completionResult;

                return completionResult;
            }

            private static AutocompletionResult ToResult(TrackLoadResult search, string query)
            {
                var isUri = Uri.IsWellFormedUriString(query, UriKind.Absolute);

                return search.IsPlaylist && isUri
                    ? AutocompletionResult.FromSuccess(new[] { new AutocompleteResult(search.Playlist.Name, query) })
                    : AutocompletionResult.FromSuccess(search.Tracks.Take(10).Select(t =>
                    {
                        var trackTitle = $"{t.Author} - {t.Title} [{t.Duration}]";
                        return new AutocompleteResult(trackTitle.Length > 80 ? trackTitle[..78] + ".." : trackTitle, t.Uri.ToString());
                    }));
            }

            private static readonly TimeSpan SearchCache = TimeSpan.FromSeconds(50);
            private static readonly TimeSpan SearchCooldown = TimeSpan.FromSeconds(2);
        }

        private Task<XinyanPlayer> GetPlayer(bool connect = false) => XinyanService.GetPlayerAsync(Context, connect);
    }
}
