﻿using Microsoft.AspNetCore.SignalR;
using XinyanPlugin.Extensions;

namespace XinyanPlugin.Core
{
    public partial class XinyanPlayer
    {
        protected async Task SendQueue()
            => await Hub.Clients.Group(GuildId.ToString()).SendAsync("ReceiveQueue", this.ToApiQueue());

        protected async Task SendState()
            => await Hub.Clients.Group(GuildId.ToString()).SendAsync("ReceivePlayerState", this.ToApiState());
    }
}
