﻿using Discord;
using Discord.WebSocket;
using Lavalink4NET.Players.Queued;
using Microsoft.AspNetCore.SignalR;
using XinyanPlugin.Hubs;

namespace XinyanPlugin.Core
{
    public record XinyanPlayerOptions : QueuedLavalinkPlayerOptions
    {
        public ITextChannel TextChannel { get; }
        public SocketGuild Guild { get; }
        public IHubContext<XinyanHub> Hub { get; }

        public XinyanPlayerOptions(ITextChannel textChannel, SocketGuild guild, IHubContext<XinyanHub> hub)
        {
            TextChannel = textChannel;
            Guild = guild;
            Hub = hub;
        }
    }
}
