﻿using Discord;
using Lavalink4NET.Players;
using Lavalink4NET.Players.Queued;

namespace XinyanPlugin.Core
{
    public record class XinyanQueueItem(TrackReference Reference, IUser Requester) : ITrackQueueItem;
}
