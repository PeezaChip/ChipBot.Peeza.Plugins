﻿using Discord;
using Lavalink4NET.Players;
using Lavalink4NET.Players.Queued;
using Lavalink4NET.Tracks;
using XinyanPlugin.Extensions;
using XinyanPlugin.Models;

namespace XinyanPlugin.Core
{
    public partial class XinyanPlayer : QueuedLavalinkPlayer
    {
        public async Task Enqueue(IEnumerable<LavalinkTrack> tracks, IUser user)
        {
            var queueItems = tracks.Select(t => new XinyanQueueItem(new TrackReference(t), user)).ToList();

            if (queueItems.Count > 1)
            {
                await TextChannel.SendMessageAsync($"Enqueued {queueItems.Count} tracks.\nTotal Duration: `{new TimeSpan(tracks.Select(t => t.Duration).Sum(d => d.Ticks))}`");
            }
            else
            {
                if (State == PlayerState.Playing || Queue.Any())
                {
                    await TextChannel.SendMessageAsync(embed: queueItems.First().ToEmbed().WithTitle("Enqueued").Build());
                }
            }

            foreach (var item in queueItems)
            {
                await PlayAsync(item);
            }
        }

        public async Task DoPlayerAction(PlayerAction action)
        {
            if (action == PlayerAction.playpause)
            {
                switch (State)
                {
                    case PlayerState.Playing:
                        action = PlayerAction.pause;
                        break;
                    case PlayerState.Paused:
                        action = PlayerAction.play;
                        break;
                }
            }

            switch (action)
            {
                case PlayerAction.play:
                    if (State == PlayerState.Playing) return;
                    if (State == PlayerState.Paused)
                    {
                        await ResumeAsync();
                    }
                    break;
                case PlayerAction.pause:
                    if (State == PlayerState.Paused || State == PlayerState.NotPlaying) return;
                    await PauseAsync();
                    break;
                case PlayerAction.stop:
                    if (State == PlayerState.NotPlaying) return;
                    await StopAsync();
                    break;
                case PlayerAction.skip:
                    await SkipAsync();
                    break;
                case PlayerAction.disconnect:
                    await DisconnectAsync();
                    break;
            }

            await SendState();
        }

        public async Task<bool> ToggleShuffle()
        {
            Shuffle = !Shuffle;
            await SendState();
            return Shuffle;

        }

        public async Task<bool> ToggleRepeat()
        {
            RepeatMode = RepeatMode == TrackRepeatMode.None ? TrackRepeatMode.Track : TrackRepeatMode.None;
            await SendState();
            return RepeatMode == TrackRepeatMode.Track;
        }
    }
}
