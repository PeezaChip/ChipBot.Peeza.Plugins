﻿using Lavalink4NET.Players;
using Lavalink4NET.Players.Queued;
using XinyanPlugin.Extensions;

namespace XinyanPlugin.Core
{
    public partial class XinyanPlayer : QueuedLavalinkPlayer
    {
        protected async override ValueTask NotifyTrackStartedAsync(ITrackQueueItem queueItem, CancellationToken cancellationToken = default)
        {
            await base.NotifyTrackStartedAsync(queueItem, cancellationToken);

            await TextChannel.SendMessageAsync(embed: ((XinyanQueueItem)queueItem).ToEmbed().WithTitle("Now playing").Build());

            await SendQueue();
        }

        protected async override ValueTask NotifyTrackEnqueuedAsync(ITrackQueueItem queueItem, int position, CancellationToken cancellationToken = default)
        {
            await base.NotifyTrackEnqueuedAsync(queueItem, position, cancellationToken);

            await SendQueue();
        }
    }
}
