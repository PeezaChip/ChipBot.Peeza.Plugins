﻿using Discord;
using Discord.WebSocket;
using Lavalink4NET.Players;
using Lavalink4NET.Players.Queued;
using Microsoft.AspNetCore.SignalR;
using XinyanPlugin.Hubs;

namespace XinyanPlugin.Core
{
    public partial class XinyanPlayer : QueuedLavalinkPlayer
    {
        public ITextChannel TextChannel { get; }
        public SocketGuild Guild { get; }
        public IHubContext<XinyanHub> Hub { get; }

        public new XinyanQueueItem CurrentItem => (XinyanQueueItem)base.CurrentItem;
        public new IEnumerable<XinyanQueueItem> Queue => base.Queue.Cast<XinyanQueueItem>();
        public IEnumerable<XinyanQueueItem> History => base.Queue.History.Cast<XinyanQueueItem>();

        public XinyanPlayer(IPlayerProperties<XinyanPlayer, XinyanPlayerOptions> properties) : base(properties)
        {
            TextChannel = properties.Options.Value.TextChannel;
            Guild = properties.Options.Value.Guild;
            Hub = properties.Options.Value.Hub;
        }

        public static ValueTask<XinyanPlayer> CreatePlayerAsync(IPlayerProperties<XinyanPlayer, XinyanPlayerOptions> properties, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ArgumentNullException.ThrowIfNull(properties);

            return ValueTask.FromResult(new XinyanPlayer(properties));
        }
    }
}
