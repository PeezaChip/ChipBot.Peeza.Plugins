﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Extensions;
using Discord;
using Discord.WebSocket;
using Lavalink4NET;
using Lavalink4NET.DiscordNet;
using Lavalink4NET.Players;
using Lavalink4NET.Rest.Entities.Tracks;
using Lavalink4NET.Tracks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using XinyanPlugin.Core;
using XinyanPlugin.Hubs;

namespace XinyanPlugin.Services
{
    [Chipject]
    public class XinyanService : ChipService<XinyanService>
    {
        private readonly IAudioService audioService;
        private readonly DiscordSocketClient discord;
        private readonly IHubContext<XinyanHub> hub;

        public XinyanService(ILogger<XinyanService> log, IAudioService audioService, DiscordSocketClient discord, IHubContext<XinyanHub> hub) : base(log)
        {
            this.audioService = audioService;
            this.discord = discord;
            this.hub = hub;
        }

        public async Task<TrackLoadResult> SearchTracks(string query)
        {
            var result = await audioService.Tracks
                .LoadTracksAsync(query, TrackSearchMode.YouTube);

            return result;
        }

        public async Task LoadTracks(IInteractionContext context, string link)
        {
            var tracks = await LoadTracksInner(link);

            var player = await GetPlayerAsync(context) ?? throw new Exception("Couldn't connect to to voice channel");

            await player.Enqueue(tracks, context.User);
        }

        public async Task LoadTracks(HubCallerContext context, string link)
        {
            var tracks = await LoadTracksInner(link);

            var player = await GetPlayerAsync(context) ?? throw new Exception("Couldn't connect to to voice channel");

            await player.Enqueue(tracks, context.GetCurrentDiscordUser());
        }

        public async Task LoadTracks(HttpContext context, string link)
        {
            var tracks = await LoadTracksInner(link);

            var player = await GetPlayerAsync(context) ?? throw new Exception("Couldn't connect to to voice channel");

            await player.Enqueue(tracks, context.GetCurrentDiscordUser());
        }

        public async Task<XinyanPlayer> GetPlayerAsync(IInteractionContext context, bool connectToVoiceChannel = true)
        {
            var retrieveOptions = new PlayerRetrieveOptions(connectToVoiceChannel ? PlayerChannelBehavior.Join : PlayerChannelBehavior.None);

            var options = new XinyanPlayerOptions(context.Channel as ITextChannel, context.Guild as SocketGuild, hub);

            var result = await audioService.Players.RetrieveAsync<XinyanPlayer, XinyanPlayerOptions>(context, XinyanPlayer.CreatePlayerAsync, options, retrieveOptions);

            if (!result.IsSuccess)
            {
                var errorMessage = result.Status switch
                {
                    PlayerRetrieveStatus.UserNotInVoiceChannel => "You are not connected to a voice channel.",
                    PlayerRetrieveStatus.BotNotConnected => "The bot is currently not connected.",
                    _ => "Unknown error.",
                };

                throw new Exception(errorMessage);
            }

            return result.Player;
        }

        public async Task<XinyanPlayer> GetPlayerAsync(HubCallerContext context)
        {
            var guild = GetGuildFromContext(context) ?? throw new Exception("You are not connected to a voice channel.");
            var player = await audioService.Players.GetPlayerAsync<XinyanPlayer>(guild) ?? throw new Exception("Unknown error.");

            return player;
        }

        public async Task<XinyanPlayer> GetPlayerAsync(HttpContext context)
        {
            var guild = GetGuildFromContext(context) ?? throw new Exception("You are not connected to a voice channel.");
            var player = await audioService.Players.GetPlayerAsync<XinyanPlayer>(guild) ?? throw new Exception("Unknown error.");

            return player;
        }

        private async Task<List<LavalinkTrack>> LoadTracksInner(string link)
        {
            var tracks = new List<LavalinkTrack>();

            var result = await audioService.Tracks.LoadTracksAsync(link, new TrackLoadOptions(TrackSearchMode.YouTube));

            if (result.IsPlaylist)
            {
                tracks.AddRange(result.Tracks);
            }
            else if (result.Track != null)
            {
                tracks.Add(result.Track);
            }

            return !tracks.Any() ? throw new Exception("Error while loading tracks") : tracks;
        }

        private SocketGuild GetGuildFromContext(HubCallerContext context)
        {
            var userId = context.GetCurrentDiscordUserId();
            var voiceChannel = discord.GetUser(userId).MutualGuilds.Select(g => g.GetUser(userId).VoiceChannel).Where(v => v != null).FirstOrDefault();
            return voiceChannel?.Guild;
        }

        private SocketGuild GetGuildFromContext(HttpContext context)
        {
            var userId = context.GetCurrentDiscordUserId();
            var voiceChannel = discord.GetUser(userId).MutualGuilds.Select(g => g.GetUser(userId).VoiceChannel).Where(v => v != null).FirstOrDefault();
            return voiceChannel?.Guild;
        }
    }
}
