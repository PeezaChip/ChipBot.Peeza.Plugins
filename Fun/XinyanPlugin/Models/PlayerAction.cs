﻿namespace XinyanPlugin.Models
{
    public enum PlayerAction
    {
        play, pause, stop, skip, disconnect, playpause
    }
}