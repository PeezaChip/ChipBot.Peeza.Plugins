﻿using ChipBot.Core.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Text.Json;
using XinyanPlugin.Core;
using XinyanPlugin.Extensions;
using XinyanPlugin.Models;
using XinyanPlugin.Services;

namespace XinyanPlugin.Hubs
{
    [Authorize]
    [ChipHub("/xinyanr")]
    public class XinyanHub : Hub
    {
        private readonly XinyanService xinyan;
        private readonly ILogger<XinyanHub> logger;

        public XinyanHub(XinyanService xinyan, ILogger<XinyanHub> logger)
        {
            this.xinyan = xinyan;
            this.logger = logger;
        }

        public async override Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
            _ = await GetPlayer();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
            await ClearGroup();
        }

        public async Task Play()
        {
            var player = await GetPlayer();
            player?.DoPlayerAction(PlayerAction.playpause);
        }

        public async Task Skip()
        {
            var player = await GetPlayer();
            player?.DoPlayerAction(PlayerAction.skip);
        }

        public async Task Stop()
        {
            var player = await GetPlayer();
            player?.DoPlayerAction(PlayerAction.stop);
        }

        public async Task Volume(int volume)
        {
            if (volume is > 1000 or < 0) return;

            var player = await GetPlayer();
            player?.SetVolumeAsync(volume / 100f);
        }

        public async Task ToggleShuffle()
        {
            var player = await GetPlayer();
            player?.ToggleShuffle();
        }

        public async Task ToggleRepeat()
        {
            var player = await GetPlayer();
            player?.ToggleRepeat();
        }

        public async Task GetQueue()
        {
            var player = await GetPlayer();
            var queue = player?.ToApiQueue();

            logger.LogTrace($"GetQueue()\n {JsonSerializer.Serialize(queue)}");
            await Clients.Caller.SendAsync("ReceiveQueue", queue);
        }

        public async Task GetPlayerState()
        {
            var player = await GetPlayer();
            var result = player?.ToApiState();

            logger.LogTrace($"GetPlayerState()\n {JsonSerializer.Serialize(result)}");
            await Clients.Caller.SendAsync("ReceivePlayerState", result);
        }

        private static readonly ConcurrentDictionary<string, string> GroupMap = new();

        private async Task AddToGroup(string newGroup)
        {
            if (GroupMap.TryGetValue(Context.ConnectionId, out var currentGroup))
            {
                if (newGroup == currentGroup) return;

                GroupMap[Context.ConnectionId] = newGroup;

                await Groups.RemoveFromGroupAsync(Context.ConnectionId, currentGroup);
            }
            else
            {
                GroupMap.TryAdd(Context.ConnectionId, newGroup);
            }
            await Groups.AddToGroupAsync(Context.ConnectionId, newGroup);
        }

        private async Task ClearGroup()
        {
            if (GroupMap.Remove(Context.ConnectionId, out var currentGroup))
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, currentGroup);
            }
        }

        private async Task<XinyanPlayer> GetPlayer()
        {
            try
            {
                var player = await xinyan.GetPlayerAsync(Context);
                if (player != null)
                {
                    await AddToGroup(player.GuildId.ToString());
                    return player;
                }
            }
            catch { }
            return null;
        }
    }
}
