﻿using ChipBot.Core.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using XinyanPlugin.Services;

namespace XinyanPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [Authorize]
    [ChipApi(GroupName = "xinyan", Version = "v1", Title = "ChipBot Xinyan V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class XinyanController : ControllerBase
    {
        private readonly XinyanService xinyanService;

        public XinyanController(XinyanService xinyanService)
            => this.xinyanService = xinyanService;

        [HttpPost("load")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUsers(string link)
        {
            await xinyanService.LoadTracks(HttpContext, link);
            return Ok();
        }
    }
}
