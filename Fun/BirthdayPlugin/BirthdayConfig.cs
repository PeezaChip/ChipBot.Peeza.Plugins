﻿using Newtonsoft.Json;

namespace BirthdayPlugin
{
    public class BirthdayConfig
    {
        [JsonProperty("channel")]
        public ulong NotifyChannel { get; set; } = 0;

        [JsonProperty("message")]
        public string Message { get; set; } = "Happy Birthday {0}!! :birthday: :balloon: :tada:";
    }
}
