﻿using BirthdayPlugin.Data;
using BirthdayPlugin.Data.Models;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BirthdayPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "birthday", Version = "v1", Title = "ChipBot Birthday V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class BirthdayController : ControllerBase
    {
        private readonly ChipBotDbContextFactory<BirthdayDbContext> dbContextFactory;

        public BirthdayController(ChipBotDbContextFactory<BirthdayDbContext> dbContextFactory)
            => this.dbContextFactory = dbContextFactory;

        [HttpGet("closest")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dictionary<ulong, DateTime>))]
        public IActionResult GetClosestBirthdays()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var birthdays = dbContext.Birthdays.ToList();
            var date = DateTime.Now;

            var closestDates = birthdays
                .Select(bd => new { id = bd.UserId, date = new DateTime(bd.Month < date.Month || (bd.Month == date.Month && bd.Day < date.Day) ? date.Year + 1 : date.Year, bd.Month, bd.Day) })
                .OrderBy(d => d.date)
                .Take(3)
                .ToDictionary(obj => obj.id, obj => obj.date);

            return Ok(closestDates);
        }

        [HttpGet("my")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<byte, byte>))]
        public IActionResult GetMy()
        {
            using var dbContext = dbContextFactory.CreateDbContext();

            var userId = HttpContext.GetCurrentDiscordUserId();

            var model = dbContext.Birthdays.Find(userId);

            return Ok(new Tuple<byte, byte>(model?.Month ?? 1, model?.Day ?? 1));
        }

        [HttpPost("my")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PostMy(byte month, byte day)
        {
            using var dbContext = dbContextFactory.CreateDbContext();

            var userId = HttpContext.GetCurrentDiscordUserId();

            var model = dbContext.Birthdays.Find(userId);

            if (model == null)
            {
                model = new BirthdayModel()
                {
                    UserId = userId,
                    Day = day,
                    Month = month
                };
                dbContext.Birthdays.Add(model);
            }
            else
            {
                model.Day = day;
                model.Month = month;
                dbContext.Birthdays.Update(model);
            }

            dbContext.SaveChanges();

            return Ok();
        }
    }
}
