﻿using BirthdayPlugin.Data;
using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BirthdayPlugin.Services
{
    [Notify]
    [Chipject(ChipjectType.Transient)]
    public class BirthdayService : ChipService<BirthdayService>, INotifyService
    {
        private readonly DiscordSocketClient discord;
        private readonly SettingsService<BirthdayConfig> config;
        private readonly SettingsService<GlobalConfig> globalConfig;
        private readonly ChipBotDbContextFactory<BirthdayDbContext> dbContextFactory;

        private SocketGuild MainGuild => discord.GetGuild(globalConfig.Settings.MainGuildId);

        public BirthdayService(ILogger<BirthdayService> log, SettingsService<GlobalConfig> globalConfig, SettingsService<BirthdayConfig> config,
            DiscordSocketClient discord, ChipBotDbContextFactory<BirthdayDbContext> dbContextFactory)
            : base(log)
        {
            this.discord = discord;
            this.config = config;
            this.globalConfig = globalConfig;
            this.dbContextFactory = dbContextFactory;
        }

        public async Task Notify()
        {
            logger.LogInformation("Checking birthdays");

            using var dbContext = dbContextFactory.CreateDbContext();
            var date = DateTime.Now;
            var birthdays = dbContext.Birthdays
                .Where(b => b.Month == date.Month && b.Day == date.Day)
                .Select(b => b.UserId);

            foreach (var id in birthdays)
            {
                var user = MainGuild.GetUser(id);
                if (user == null) continue;

                logger.LogInformation($"Notifying {user.Nickname} ({id})");

                try
                {
                    var channel = (config.Settings.NotifyChannel == 0
                        ? await user.CreateDMChannelAsync()
                        : discord.GetChannel(config.Settings.NotifyChannel) as IMessageChannel)
                        ?? throw new Exception("Channel is null");

                    await channel.SendMessageAsync(string.Format(config.Settings.Message, user.Mention));
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Couldn't send birthday notice");
                }

                await Task.Delay(1000);
            }

            logger.LogInformation("Checked birthdays");
        }
    }
}
