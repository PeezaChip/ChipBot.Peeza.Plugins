﻿using ChipBot.Core.Data;

namespace BirthdayPlugin.Data
{
    public class BirthdayDbContextFactory : ChipBotDbContextFactory<BirthdayDbContext> { }
}
