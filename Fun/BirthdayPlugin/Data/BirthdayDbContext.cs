﻿using BirthdayPlugin.Data.Models;
using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;

namespace BirthdayPlugin.Data
{
    [ChipDbContext("BirthdayPlugin")]
    public class BirthdayDbContext : DbContext
    {
        public DbSet<BirthdayModel> Birthdays { get; set; }

        public BirthdayDbContext(DbContextOptions options) : base(options) { }
    }
}
