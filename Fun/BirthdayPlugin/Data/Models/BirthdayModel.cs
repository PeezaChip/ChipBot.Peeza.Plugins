﻿using System.ComponentModel.DataAnnotations;

namespace BirthdayPlugin.Data.Models
{
    public class BirthdayModel
    {
        [Key]
        public ulong UserId { get; set; }

        public byte Month { get; set; }
        public byte Day { get; set; }
    }
}
