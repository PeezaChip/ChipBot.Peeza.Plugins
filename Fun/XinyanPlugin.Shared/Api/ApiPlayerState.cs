﻿using ChipBot.Core.Shared.Api;

namespace XinyanPlugin.Shared.Api
{
    public class ApiPlayerState
    {
        public bool IsShuffleEnabled { get; set; }
        public bool IsRepeatEnabled { get; set; }
        public bool IsPlaying { get; set; }
        public int Volume { get; set; }
        public TimeSpan Position { get; set; }

        public ApiDiscordGuild Guild { get; set; }
    }
}
