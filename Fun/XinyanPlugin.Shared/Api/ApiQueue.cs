﻿namespace XinyanPlugin.Shared.Api
{
    public class ApiQueue
    {
        public ApiTrack CurrentTrack { get; set; }

        public List<ApiTrack> Queue { get; set; } = new();
        public List<ApiTrack> History { get; set; } = new();
    }
}
