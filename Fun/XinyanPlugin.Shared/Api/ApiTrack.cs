﻿namespace XinyanPlugin.Shared.Api
{
    public class ApiTrack
    {
        public ulong UserId { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        public TimeSpan Duration { get; set; }

        public string Url { get; set; }

        public string Source { get; set; }

        public string Image { get; set; }
    }
}
