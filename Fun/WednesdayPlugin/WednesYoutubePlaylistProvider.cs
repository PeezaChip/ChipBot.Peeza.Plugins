﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using Discord;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using WednesdayPlugin.Core;
using WednesdayPlugin.Services;

namespace WednesdayPlugin
{
    [Chipject(ChipjectType.Transient, typeof(IWednesdayProvider))]
    public class WednesYoutubePlaylistProvider : ChipService<WednesYoutubePlaylistProvider>, IWednesdayProvider
    {
        private readonly Random random;
        private readonly WednessApiService api;

        public WednesYoutubePlaylistProvider(ILogger<WednesYoutubePlaylistProvider> logger, Random random, WednessApiService api) : base(logger)
        {
            this.random = random;
            this.api = api;
        }

        public async Task SendMeme(ITextChannel channel)
        {
            logger.LogInformation("Getting videos");
            var playlist = await api.ApiClient.GetPlaylist();
            var id = playlist.Items[random.Next(0, playlist.Items.Count)].ContentDetails.VideoId;

            logger.LogInformation("Sending meme");
            await channel.SendMessageAsync($"It's wednesday my dudes!\nhttps://youtu.be/{id}");
        }
    }
}
