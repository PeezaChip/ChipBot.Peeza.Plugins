﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WednesdayPlugin.Core;

namespace WednesdayPlugin.Services
{
    [Chipject(ChipjectType.Transient)]
    [Notify(NoEarlierThan = 18)]
    public class WednesdayService : ChipService<WednesdayService>, INotifyService
    {
        private readonly Random random;
        private readonly DiscordSocketClient discord;
        private readonly SettingsService<WednesdayConfig> config;
        private readonly IEnumerable<IWednesdayProvider> providers;

        public WednesdayService(ILogger<WednesdayService> logger, Random random, DiscordSocketClient discord, SettingsService<WednesdayConfig> config, IEnumerable<IWednesdayProvider> providers) : base(logger)
        {
            this.random = random;
            this.discord = discord;
            this.config = config;
            this.providers = providers;
        }

        public async Task Notify()
        {
            var now = DateTime.Now;
            if (now.DayOfWeek != DayOfWeek.Wednesday) return;

            logger.LogInformation($"Getting channel");
            var channel = (ITextChannel)discord.GetChannel(config.Settings.ChannelId);

            foreach (var provider in providers.OrderRandomly(random))
            {
                logger.LogInformation($"Trying to use {provider.GetType().Name}");
                try
                {
                    logger.LogInformation("Sending meme");
                    await provider.SendMeme(channel);
                    break;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Couldn't use {provider.GetType().Name}");
                }
            }
        }
    }
}
