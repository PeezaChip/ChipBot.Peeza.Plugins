﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;
using RestEase;
using System;
using System.Net.Http;
using WednesdayPlugin.Models;

namespace WednesdayPlugin.Services
{
    [Chipject]
    public class WednessApiService : ApiService<IWednesdayApi>
    {
        private const string ApiUrl = "https://www.googleapis.com/youtube/v3/";

        public WednessApiService(ILogger<WednessApiService> logger, SettingsService<WednesdayConfig> config, SettingsService<NetworkConfig> networkConfig) : base(logger, networkConfig)
        {
            ApiClient = new RestClient(
                new HttpClient()
                {
                    BaseAddress = new Uri(ApiUrl)
                }).For<IWednesdayApi>();

            ApiClient.UserAgent = UserAgent;
            ApiClient.ApiKey = config.Settings.ApiKey;
        }
    }
}
