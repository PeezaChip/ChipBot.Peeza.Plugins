﻿using RestEase;
using System.Threading.Tasks;

namespace WednesdayPlugin.Models
{
    public interface IWednesdayApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Query("key")]
        string ApiKey { get; set; }

        [Get("playlistItems")]
        Task<Playlist> GetPlaylist(string part = "contentDetails", string playlistId = "PLy3-VH7qrUZ5IVq_lISnoccVIYZCMvi-8", string maxResults = "50");
    }
}
