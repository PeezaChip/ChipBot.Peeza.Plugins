﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WednesdayPlugin.Models
{
    public class Playlist
    {
        [JsonProperty("items")]
        public List<Item> Items { get; set; }
    }

    public class Item
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("contentDetails")]
        public ContentDetails ContentDetails { get; set; }
    }

    public class ContentDetails
    {
        [JsonProperty("videoId")]
        public string VideoId { get; set; }

        [JsonProperty("videoPublishedAt")]
        public DateTimeOffset VideoPublishedAt { get; set; }
    }
}
