﻿using ChipBot.Core.Web.Abstract;

namespace BirthdayPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "BirthdayWebPlugin";
    }
}
