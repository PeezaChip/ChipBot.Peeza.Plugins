﻿using ChipBot.Core.Web.Abstract;

namespace AnimalPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "AnimalWebPlugin";
    }
}
