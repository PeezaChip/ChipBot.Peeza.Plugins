﻿using ChipBot.Core.Abstract;

namespace DebugPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "DebugPlugin";
    }
}