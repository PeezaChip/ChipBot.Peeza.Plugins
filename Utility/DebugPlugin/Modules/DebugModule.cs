﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace DebugPlugin.Modules
{
    [Group("debug", "debug commands")]
    [ChipInteractionPrecondition(typeof(RequireOwners))]
    [RegisterToDevGuildOnly]
    public class DebugModule : ChipInteractionModuleBase
    {
        private static IEnumerable<Type> notifyTypes;

        public static IEnumerable<Type> GetNotifyTypes(IServiceProvider serviceProvider)
        {
            if (notifyTypes != null) return notifyTypes;

            var types = serviceProvider.GetRequiredService<IEnumerable<Type>>();
            var typesToInject = types.Where(t => !t.IsAbstract && t.GetCustomAttribute<ChipjectAttribute>() != null);
            var notifyService = typeof(INotifyService);

            notifyTypes = typesToInject.Where(notifyService.IsAssignableFrom);
            return notifyTypes;
        }

        private readonly IServiceProvider serviceProvider;

        public DebugModule(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            _ = GetNotifyTypes(serviceProvider);
        }

        [SlashCommand("force-notify", "forces a service to invoke notify")]
        public async Task NotifyService([Summary("service", "service to invoke notify"), Autocomplete(typeof(NotifyAutoCompleter))] string serviceName)
        {
            try
            {
                var type = notifyTypes.FirstOrDefault(t => t.Name == serviceName) ?? throw new Exception($"Can't find type for {serviceName}");
                var service = (INotifyService)serviceProvider.GetRequiredService(type);
                await service.Notify();
                await ReplySuccessAsync();
            }
            catch (Exception ex)
            {
                await ReplyError(ex.Message);
            }
        }

        private class NotifyAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var notifyTypes = GetNotifyTypes(services);

                var options = notifyTypes.Select(t => new AutocompleteResult(t.Name, t.Name));

                var query = autocompleteInteraction.Data.Current.Value.ToString().Trim();

                if (!string.IsNullOrWhiteSpace(query))
                {
                    options = options.Where(o => o.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
                }

                return Task.FromResult(AutocompletionResult.FromSuccess(options.Take(10)));
            }
        }
    }
}
