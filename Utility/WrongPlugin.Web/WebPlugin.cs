﻿using ChipBot.Core.Web.Abstract;

namespace WrongPlugin.Web
{
    public class WebPlugin : AbstractWebPlugin
    {
        public override string Name => "WrongWebPlugin";
    }
}
