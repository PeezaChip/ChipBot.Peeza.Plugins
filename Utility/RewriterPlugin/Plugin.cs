﻿using ChipBot.Core.Abstract;

namespace RewriterPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "RewriterPlugin";
    }
}
