﻿using RewriterPlugin.Models;
using System.Collections.Generic;

namespace RewriterPlugin
{
    public class RewriterConfig
    {
        public List<RewriterRule> Rules { get; set; } = new();
    }
}
