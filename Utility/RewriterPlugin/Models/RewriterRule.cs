﻿namespace RewriterPlugin.Models
{
    public class RewriterRule
    {
        public string Regex { get; set; }
        public int Groups { get; set; }
        public string Format { get; set; }
        public bool SendAsThread { get; set; }
    }
}
