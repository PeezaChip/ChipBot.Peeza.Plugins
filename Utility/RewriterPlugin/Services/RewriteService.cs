﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Services.Utils;
using Discord.WebSocket;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using RewriterPlugin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RewriterPlugin.Services
{
    [Chipject]
    public class RewriteService : ChipService<RewriteService>
    {
        private const string cacheKey = "rewrite_service_rules";

        private readonly SettingsService<RewriterConfig> settings;
        private readonly IMemoryCache cache;

        public RewriteService(ILogger<RewriteService> logger, DiscordSocketClient discord, SettingsService<RewriterConfig> settings, IMemoryCache cache) : base(logger)
        {
            this.settings = settings;
            this.cache = cache;

            discord.MessageReceived += MessageReceived;
        }

        private async Task MessageReceived(SocketMessage msg)
        {
            var rules = GetRewriteRules();
            foreach (var (rule, regex) in rules)
            {
                var matches = regex.Matches(msg.Content);
                if (!matches.Any()) continue;

                var results = new List<string>();

                foreach (Match match in matches)
                {
                    if (!match.Success) continue;
                    if (match.Groups.Count != rule.Groups + 1) continue;

                    results.Add(string.Format(rule.Format, match.Groups.Values.Skip(1).Take(rule.Groups).ToArray()));
                }

                if (!results.Any()) continue;

                if (msg.Channel is SocketTextChannel socketChannel && rule.SendAsThread)
                {
                    var thread = await socketChannel.CreateThreadAsync("ChipRewrite", message: msg);
                    await Task.Delay(100);
                    await thread.SendMessageAsync(string.Join("\n", results));
                }
                else
                {
                    await msg.Channel.SendMessageAsync(string.Join("\n", results) + $"\nAuthor: {msg.Author.Mention}");
                    await Task.Delay(100);
                    await msg.DeleteAsync();
                }

                break;
            }
        }

        private IDictionary<RewriterRule, Regex> GetRewriteRules()
        {
            var cached = cache.Get(cacheKey);
            if (cached != null) return (Dictionary<RewriterRule, Regex>)cached;

            logger.LogDebug("Loading rules");
            var rules = settings.Settings.Rules.ToDictionary(rule => rule, rule => new Regex(rule.Regex));

            if (rules != null)
            {
                cache.Set(cacheKey, rules, DateTimeOffset.Now.AddHours(1));
                return rules;
            }

            return new Dictionary<RewriterRule, Regex>();
        }
    }
}
