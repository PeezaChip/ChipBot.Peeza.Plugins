﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;
using System.Text;
using WrongPlugin.Services;

namespace WrongPlugin.Modules
{
    [Group("wrong", "wrong channel commands")]
    public class WrongChannelModule : ChipInteractionModuleBase
    {
        public WrongChannelService WrongChannel { get; set; }

        [SlashCommand("call-cops", "puts a cop picture")]
        public async Task CallCops(ITextChannel textChannel = null)
        {
            var sb = new StringBuilder();
            if (textChannel != null)
            {
                sb.AppendLine($"Please go to {textChannel.Mention}");
            }
            sb.AppendLine(WrongChannel.GetRandomPicture());
            await RespondAsync(sb.ToString());
        }

        [SlashCommand("add-cop", "adds a picture")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task AddPicture([Summary("url", "picture url")] string uri)
        {
            WrongChannel.AddPicture(uri);
            await ReplySuccessAsync();
        }

        [SlashCommand("remove-cop", "removes a picture")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task RemovePicture([Summary("url", "picture url")] string uri)
        {
            WrongChannel.RemovePicture(uri);
            await ReplySuccessAsync();
        }
    }
}
