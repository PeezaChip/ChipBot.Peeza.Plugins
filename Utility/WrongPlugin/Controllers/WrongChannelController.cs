﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WrongPlugin.Services;

namespace WrongPlugin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Roles = RoleDefaults.Owner)]
    [Produces("application/json")]
    [ChipApi(GroupName = "wrongchannel", Version = "v1", Title = "ChipBot Wrong Channel V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class WrongChannelController : ControllerBase
    {
        private readonly WrongChannelService wrongChannelService;

        public WrongChannelController(WrongChannelService wrongChannelService)
            => this.wrongChannelService = wrongChannelService;

        [HttpGet("list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<string>))]
        public IActionResult GetList()
            => Ok(wrongChannelService.GetPictures());

        [HttpPost("add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult PostImage(string uri)
        {
            wrongChannelService.AddPicture(uri);
            return Ok();
        }

        [HttpDelete("remove")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult DeleteImage(string uri)
        {
            wrongChannelService.RemovePicture(uri);
            return Ok();
        }
    }
}
