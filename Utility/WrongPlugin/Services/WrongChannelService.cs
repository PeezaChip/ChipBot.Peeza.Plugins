﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using Microsoft.Extensions.Logging;
using WrongPlugin.Data;
using WrongPlugin.Data.Models;

namespace WrongPlugin.Services
{
    [Chipject]
    public class WrongChannelService : ChipService<WrongChannelService>
    {
        private readonly Random random;
        private readonly ChipBotDbContextFactory<WrongChannelDbContext> dbContextFactory;

        public WrongChannelService(ILogger<WrongChannelService> logger, Random random, ChipBotDbContextFactory<WrongChannelDbContext> dbContextFactory) : base(logger)
        {
            this.random = random;
            this.dbContextFactory = dbContextFactory;
        }

        public void AddPicture(string uri)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var exists = dbContext.WrongPictures.Find(uri) != null;
            if (exists) return;

            dbContext.WrongPictures.Add(new WrongChannelModel() { Url = uri });
            dbContext.SaveChanges();
        }

        public void RemovePicture(string uri)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var model = dbContext.WrongPictures.Find(uri);
            if (model == null) return;

            dbContext.WrongPictures.Remove(model);
            dbContext.SaveChanges();
        }

        public List<string> GetPictures()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return dbContext.WrongPictures.Select(p => p.Url).ToList();
        }

        public string GetRandomPicture()
        {
            var pictures = GetPictures();
            return pictures.Any() ? pictures[random.Next(0, pictures.Count)] : "<place for child cop image>";
        }
    }
}
