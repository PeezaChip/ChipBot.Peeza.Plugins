﻿using System.ComponentModel.DataAnnotations;

namespace WrongPlugin.Data.Models
{
    public class WrongChannelModel
    {
        [Key]
        public string Url { get; set; }
    }
}
