﻿using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using WrongPlugin.Data.Models;

namespace WrongPlugin.Data
{
    [ChipDbContext("WrongPlugin")]
    public class WrongChannelDbContext : DbContext
    {
        public DbSet<WrongChannelModel> WrongPictures { get; set; }

        public WrongChannelDbContext(DbContextOptions options) : base(options) { }
    }
}
