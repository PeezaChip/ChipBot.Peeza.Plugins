﻿using ChipBot.Core.Data;
using WrongPlugin.Data;

namespace PointsPlugin.Data
{
    public class WrongChannelDbContextFactory : ChipBotDbContextFactory<WrongChannelDbContext> { }
}
