﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WrongPlugin.Migrations
{
    /// <inheritdoc />
    public partial class WrongPictures : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WrongPictures",
                columns: table => new
                {
                    Url = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WrongPictures", x => x.Url);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WrongPictures");
        }
    }
}
