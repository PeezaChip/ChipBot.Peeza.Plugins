﻿using ChipBot.Core.Abstract;

namespace ImgurApiPlugin
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "ImgurApiPlugin";
    }
}
