﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Imgur.API.Authentication;
using Imgur.API.Endpoints;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace ImgurApiPlugin.Services
{
    [Chipject]
    public class ImgurApiService : ApiService<ApiClient>
    {
        public ImageEndpoint ImageEndpoint { get; set; }

        public ImgurApiService(ILogger<ApiService<ApiClient>> logger, SettingsService<NetworkConfig> networkConfig, SettingsService<ImgurConfig> config)
            : base(logger, networkConfig)
        {
            var httpClient = new HttpClient();

            ApiClient = new ApiClient(config.Settings.ClientId, config.Settings.ClientSecret);

            ImageEndpoint = new ImageEndpoint(ApiClient, httpClient);
        }
    }
}
