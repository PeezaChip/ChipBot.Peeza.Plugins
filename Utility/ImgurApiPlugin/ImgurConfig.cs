﻿namespace ImgurApiPlugin
{
    public class ImgurConfig
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
